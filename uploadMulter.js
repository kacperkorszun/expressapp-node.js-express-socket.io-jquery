module.exports = { dest: './uploads/',
 limits: {
  fileSize: 100000000,
  files : 1
},
onFileUploadStart: function (file,req, res) {


  var imType = file.mimetype;
  if (req.user) {
    if(!(imType == 'image/png' || imType == 'image/gif' || imType == 'image/jpg' || imType == 'image/jpeg')) {
      file.mdErr = 'fileType'
      fs.unlink('./' + file.path) 
    } 
  } else {
    file.mdErr = 'Unlogged';
  }
},
onFileUploadComplete: function (file) {
  
},
onFileSizeLimit: function (file) {
  file.mdErr = 'sizeLimit'
  fs.unlink('./' + file.path) // delete the partially written file
},
onFilesLimit: function (file) {
  file.mdErr = 'filesLimit'
},
onError: function (error, next) {
  file.mdErr = 'err'
  console.log(error);
  next(error)
}
}