var passport = require('passport');
var passportLocal = require('passport-local');
var passportHttp = require('passport-http');
var dbModule = require('./dbModule.js');
var  Users = require('./models/user.js');
var LocalStrategy = passportLocal.Strategy;

var obj = {};

obj.local =  new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password'
  },
  function(username, password, done) {
    Users.findOne({ login: username.trim() }, function(err, user) {
      if (err) { 
        return done(null, false, { message : err.message}); }
      if (!user) {
        return done(null, false, { message : 'Incorrect username.' });
      } else {
      
      }
      if (!Users.verifyPassword(password,user.password, user.timeStamp)) {
        return done(null, false, { message : 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
)



obj.rejestracja = new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password',
    passReqToCallback : true
  },
  function(req,login, password, done) {
    
    Users.validateLogin(login, function(status, message) {
      if(status) {
         if(!Users.checkPasswordConfirmation(password, req.body.passwordConfirm)) {
          return done(null, false, { message: 'Podane hasła nie są takie same' , target : 'passwordConfirm' });
        } else {
          Users.validatePassword(password, function(status, message) {
            if(status) {
               Users.checkLoginAvailability(login, function(err, status, message) {
                if(err) {
                  done(err)
                } else if(!status) {
                  done(null, false, { message: message , target : 'login' });
                } else {
                  var newUser = new Users({});
                  newUser.login = login.replace(/(\n|\r|\r\n)/g,' ').trim();
                  newUser.timeStamp = Date.now();
                  newUser.password = password;
                  newUser.superUser = false;
                  newUser.save(function(err){
                    if(err) {
                      console.log(err.message);
                    } else {
                      return done(err, newUser);
                    }
                    
                  });
                }
              });
            } else {
              return done(null, false, { message: message , target : 'password' });
            }
          }); 
        }
      } else {
        return done(null, false, { message : message , target : 'login'});
      }
    })

   
  });

module.exports = obj;