// -- NPM MODULES ---
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');

var bodyParser = require('body-parser');
var expressSession = require('express-session');
var multer  = require('multer');
var request = require('request');
var events = require("events");
var uploadMulter = multer(require("./uploadMulter.js"));
var passport = require('passport');
var passportLocal = require('passport-local');
var passportHttp = require('passport-http');
var mongoose = require('mongoose');
var redisStore = require("connect-redis")(expressSession);
var redis   = require("redis");

// -- VARS --
global.appRoot = path.resolve(__dirname);
var usedPort = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000;
var ipAddress = process.env.OPENSHIFT_NODEJS_IP


// -- LOCAL MODULES --
var dbModule = require('./dbModule.js');
var homeController = require(appRoot+'/controllers/homeController');


// -- PRIVATE METHODS --
var loginAdmin = function(req,res,next) {
  Users.findOne({ login: 'admin' }, function(err, user) {
    req.login(user, function(err) {
        if (err) {
         return  next(err);
        } else if(user) { 
          return  next();
        } else {
          return next();
        }
    });
  })
}

//  -- MONGOOSE MODELS --
var Users = require('./models/user.js');



// -- REDIS --
var redisClient
var sessionRedisStore;
if(!process.env.OPENSHIFT_REDIS_HOST) {
  redisClient  = redis.createClient();
  sessionRedisStore = new redisStore(({ host: 'localhost', port: 6379, client: redisClient,ttl :  260}));
} else {
  redisClient  = redis.createClient(process.env.OPENSHIFT_REDIS_PORT, process.env.OPENSHIFT_REDIS_HOST);
  redisClient.auth(process.env.REDIS_PASSWORD);
  sessionRedisStore = new redisStore(({ host: process.env.OPENSHIFT_REDIS_HOST, password: process.env.REDIS_PASSWORD, port: process.env.OPENSHIFT_REDIS_PORT, client: redisClient,ttl :  260}));
}

redisClient.on("error", function (err) {
    console.log("Error " + err);
});

redisClient.on("connect", function () {
    console.log("Redis connected");
});

// -- SESSION --
var appSession = expressSession({
    store: sessionRedisStore,
    secret: 'sekretnySektret',
    saveUninitialized: false,
    resave: false
});

// -- SERVER START -- 
var app = express();
var server = app.listen(usedPort, ipAddress);

app.use(appSession);

// -- PASSPORT --
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  Users.findById(id, function(err, user) {
    done(err, user);
  });
});

var passportStrategies =require('./passportStrategies.js');

passport.use('local',passportStrategies.local);
passport.use('rejestracja', passportStrategies.rejestracja);

//  -- SOCKETS --
var io = require('socket.io').listen(server);
var homePageSocket = require('./socketModules/homePageSocket.js')(io);
var anteroomPageSocket = require('./socketModules/anteroomPageSocket.js')(io);
var userPageSocket = require('./socketModules/userPageSocket.js')(io);
var showcaseTestSocket = require('./socketModules/showcaseTestSocket.js')(io);
var updateSocket = require('./socketModules/updateSocket.js')(io);
var pixSocket = require('./socketModules/pixSocket.js')(io);
var infoSocket = require('./socketModules/infoSocket.js')(io);

io.use(function(socket, next) {
    appSession(socket.request, socket.request.res, next);
});

// DB CONNECTION START
dbModule.start(function(){ 
  homeController.initx(function() {}, homePageSocket);
});

// view engine setup
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(logger('dev'));
var jsonParser = bodyParser.json({limit: '50mb'});
var urlencodedParser = bodyParser.urlencoded({ extended: false, limit: '5mb' });
app.use(urlencodedParser);
app.use(jsonParser);

app.use(express.static(path.join(__dirname, 'public')));

//  -- ROUTES FILES --
var prestart = require('./routes/prestart');
var glowna = require('./routes/index');
var moje = require('./routes/moje');
var anteroom = require('./routes/anteroom');
var usersPages = require('./routes/users');
var logout = require('./routes/logout');
var registerService = require('./routes/registerService');
var pictures = require('./routes/pictures');
var loginRoute = require('./routes/login');
var adminRoute = require('./routes/admin');
var register = require('./routes/register');
var upload = require('./routes/upload');

app.use('*', prestart);
app.use('/', registerService);
app.use('/', glowna);
app.use('/me', moje);
app.use('/poczekalnia', anteroom);
app.use('/p', pictures);
app.use('/u', usersPages);
app.use('/logout', logout);
app.use('/login', loginRoute);
app.use('/admin', adminRoute);
app.use('/rejestracja', register);

app.use('/upload', upload);


/*app.get('/plus/:id', function(req, res) {
  if (req.user) {
    var unleashed = false;
    if(req.user.login = 'admin') {
      unleashed = true;
    }
    dbModule.plusPix(req.user.id, req.params.id, function(obj){
      res.json(obj);
    },unleashed);
  } else {
    res.json({ messages : [{ status : 'error' , text: 'Musisz być zalogowany aby głosować' }]});
  }
});
*/



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  req.toSend.title = "404 Not Found";
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    if(!req.toSend) req.toSend ={}
    res.status(err.status || 500);
    req.toSend.title = err.status || 500;
    res.render('error', {
       title : req.toSend.title,
      user: req.toSend.user,
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  if(!req.toSend) req.toSend ={}
  res.status(err.status || 500);
  req.toSend.title = err.status || 500;
  res.render('error', {
    title : req.toSend.title,
    user: req.toSend.user,
    message: err.message,
    error: {}
  });
});


module.exports = app;

