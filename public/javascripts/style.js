var animateHome = function(images, callback) {
  
  var showed = [];
  var toShow;
  var xcallback = null;
  var nextImg = function() {
    if(!(showed.length == images.length)) {
      toShow = Math.floor(Math.random()*images.length);
      while($.inArray(toShow, showed) != -1) {
        toShow = Math.floor(Math.random()*images.length);
      }
      showed.push(toShow);
      if(showed.length == images.length)  xcallback = callback;
      animateBigHomePix(images[toShow], xcallback);
      setTimeout(function() { nextImg(); }, 50);
    }
  };

  nextImg();
}


var animateBigHomePix = function(img,callback) {
  $("#"+img._id+".imgSec img").hide();
  $("#"+img._id+" .imgSec").css({height : img.dim, width: img.dim});
  $("#"+img._id).show( "scale", {duration : 500}, function(){
    $(this).find('.imgSec img').fadeIn(200, function() {
      $(this).find('.imgSec').removeAttr("style");
      if(callback)callback();
    });
    
  } );
}


var animateSmallPix = function(img,callback) {
  $("#"+img._id).show( "blind", { duration: 600 },function(){
    callback();
  });
}


var animateOther = function(images,callback) {
  var showNext = function(xix) {
    if(xix<images.length) {
      $("#"+images[xix]._id).hide();
      $("#"+images[xix]._id).show( "blind", { duration: 600 },function(){
        if(xix == images.length -1) {
          if(callback) callback();
        }
      });
      var xbufor = xix +1;
      setTimeout(function() { showNext(xbufor); }, 300);
    }
  };
  showNext(0);
};



var removeBigHomeAnimation = function (node, callback){
  node.find('img').fadeOut(200, function() {
    node.hide( "scale", {}, 500, function(){
      callback();
    });
  });
}


var removeOtherAnimation = function (node, callback){
    node.hide( "blind", { duration: 600 }, function(){
      callback();
    });
}

var updatePix = function(id, changeObj, callback) {
  
  $('#'+id+" .pixPoints").text(changeObj.points);
  if(true) {
    //onsole.log(changeObj.dim);
    var animateObjDim ={};
    var animateObjTopLeft = {};
    if(changeObj.dim !== null) {
      animateObjDim.height  = changeObj.dim;
      animateObjDim.width = changeObj.dim;
    }
    if(changeObj.top !== null) {
      animateObjTopLeft.top = changeObj.top;
    }
    if(changeObj.left !== null) {
      animateObjTopLeft.left = changeObj.left;
    }
    var z = 0;
    var cbFun = function(val, cb) {
      
      if(val == 3) {

        var setDim = true;
        if(window.pixKettle.currentScreen.width == false ) setDim = false;
        if(setDim) {
          $("#"+id+".picBox .descSec").animate({ height : 0.35 * $("#"+id+".picBox .imgSec").height()});
        }
        if(cb)cb();
      }
    }

    $("#"+id+"  .imgSec img").finish();
    $("#"+id+"  .imgSec").finish();
    $("#"+id).finish();
    $("#"+id+"  .imgSec").animate(animateObjDim, function() {
      z++;
      cbFun(z,callback);
    });

    $("#"+id+"  .imgSec img").animate(animateObjDim, function() {
      z++;
      cbFun(z,callback);
    });
    $("#"+id).animate(animateObjTopLeft, function() {
      z++;
      cbFun(z,callback);
    });
  }
  if(changeObj.dim !== null)updateFonts(id,changeObj);

};


updatePixSmall = function(id, changeObj) {
  $('#'+id+" .pixPoints").text(changeObj.points);
  if(changeObj.changePosition) {
    $('#'+id).fadeOut('slow', function(){
      if(changeObj.leftBrother){
        $("#"+changeObj.leftBrother).after($('#'+id));
      } else {
        $("#"+id).prependTo($(".picScreen"));
      }
      $("#"+id).fadeIn('slow');
    })
  }
};

var updateFonts = function(id, changeObj) {

    var pBB = changeObj.dim/90;
    $("#"+id+"  .pointUpBtt").css({'font-size' : changeObj.dim/10,  'margin' :pBB, 'border-width' : pBB, 'width' : 'calc(100% - '+ (2*pBB-0.175*20)+'px)', 'height': 'calc(50% - '+ 2*pBB +'px)'});
    $("#"+id+"  .pixPoints").css({'font-size' : changeObj.dim/10});
    $("#"+id+"  .title").css({'font-size' : parseInt(changeObj.dim/11)});
    $("#"+id+"  .avImg").css({'height' : parseInt(changeObj.dim/15), width : 'auto', 'margin-right' : parseInt(changeObj.dim/30) });
    $("#"+id+"  .author").css({'font-size' : parseInt(changeObj.dim/15)});

}


var galleryOn = false

$(window).on('popstate', function() {
      if(document.location.hash === '' && galleryOn) {
        $(".galleryContainer").click();
      } 
});



var showGallery = function(el) {

  var dim = 800;
  var dimPx =  parseInt(dim)+'px';
  var halfDimPx = parseInt(dim/2)+'px';
  var img = el.children("img").eq(0).attr("src");

  var galleryContainer = $("<div></div>",{class : 'galleryContainer'});
  var imgCloud = $("<div></div>", {class: "imgCloud"});
  imgCloud.attr("style",'left: calc(50% - '+halfDimPx+'); top: calc(50% - '+halfDimPx+');')
  $("<img></img>",{src: img, width: dimPx, height: dimPx }).appendTo(imgCloud);
  imgCloud.appendTo(galleryContainer);
  galleryContainer.click(function() {
    $(this).hide();
    galleryOn = false;
    $(this).remove();
    history.pushState("", document.title, window.location.pathname);
  });
  galleryContainer.prependTo($('body'));
  galleryOn =true;
  document.location.hash = "/p/"+el.attr('id').slice(3);
}


var showImg = function(el, setDim, callback) {
  if(setDim) animateBigHomePix(el, callback); else animateSmallPix(el,callback);
}



var imgDec = function() {
 

  $(".picBox").mouseenter(function() {
    $(this).children(".descSec").stop();
    $(this).children(".descSec").slideDown("slow");
  });


  $(".picBox").mouseleave(function() {
    $(this).children(".descSec").stop();
    $(this).children(".descSec").slideUp("slow");
  });
}



var setLoadingKettle = function() {

  var divx = $('<div></div>',{class: 'loadingPanel'});
  $('<img src="/images/loadingKettle2.gif" alt="loading ..." />').appendTo(divx);
  divx.appendTo($('.picScreen'));
  
}



$(document).ready(function(){
  var urlHash = document.location.hash;
  if (urlHash.substring(0,4)=="#/p/")window.location.replace('http://' + location.host+document.location.hash.slice(1));
  var imgTab =[];
});



