Array.prototype.delteById = function(id,callback) {
  var j =0;
  while(j < this.length  && this[j].reqId != id)j++;
  if(j<this.length) {
    callback(this[j]);
    this.splice(j, 1);
  }
}

if(!window.pixKettle) {
	window.pixKettle = {};
} 

window.pixKettle.tools = {};


window.pixKettle.tools.removeStatus = function(node, formatt) {
  var statuses = ['info', 'error', 'success'];
  var bufStas;
  for(var j=0;j<statuses.length;j++){
    
      bufStas = formatt.replace(/<status>/g, statuses[j]);
      node.removeClass(bufStas);
  }
};

window.pixKettle.tools.setStatus = function(node, status, formatt) {
  window.pixKettle.tools.removeStatus(node,formatt);
  bufStas = formatt.replace(/<status>/g, status);
  node.addClass(bufStas);
};

window.pixKettle.tools.firstLetterUp = function(string) {
	return string[0].toUpperCase() + string.slice(1);
};


window.pixKettle.tools.checkSize = function(callback) {

  var chosen = false;
  var  i=0;
  while(i < screenSizes.length && !chosen && screenSizes[i].minSize != false) {
    if (window.matchMedia('(min-width: ' + screenSizes[i].minSize + 'px)').matches) {
      chosen = true;
    } else {
      i++;
    }
  } 
  callback(screenSizes[i] || null);
};




window.pixKettle.tools.logFun = function(formName, messageFun) {
	var data =  $("#"+formName).serialize();
	$.ajax({
          method: "POST",
          url: "/login",
          data: data,
          error: function(){
          	var errorMessage = { status : 'error', text : 'Błąd : '};
          	var messages = [errorMessage];
            //showFlashMessages(messages);
          	messageFun(messages);
          },
          success: function(data){
              if(data.status == 'ok') {
               	window.location.replace('http://' + location.host+'/');
              }
              messageFun(data.messages);
          },
          timeout: 3000 
      });
}

window.pixKettle.tools.clearCss = function() {
  $(".picBox img").removeAttr( 'style' );
      $(".picBox .title").removeAttr( 'style' );
      $(".picBox .avImg").removeAttr( 'style' );
      $(".picBox .author").removeAttr( 'style' );
      $(".picBox .pointUpBtt").removeAttr( 'style' );
      $(".picBox .pixPoints").removeAttr( 'style' );
}

window.pixKettle.tools.setCss = function(node, xdim) {
      node.find("img").css({"height" : xdim, "width" : xdim});
      node.find(".title").css({'font-size' : parseInt(xdim/11)});
      node.find(".avImg").css({'height' : parseInt(xdim/15), width : 'auto', 'margin-right' : parseInt(xdim/30) });
      node.find(".author").css({'font-size' : parseInt(xdim/15)});
      var pBB = xdim/90;
      node.find(".pointUpBtt").css({'font-size' : xdim/10,  'margin' :pBB, 'border-width' : pBB, 'width' : 'calc(100% - '+ (2*pBB-0.175*20)+'px)', 'height': 'calc(50% - '+ 2*pBB +'px)'});
      node.find(".pixPoints").css({'font-size' : xdim/10});
}

window.pixKettle.tools.plusButtonsClick = function(){
  $( ".pointUpBtt" ).click(function() {
    
    var pixId = $(this).attr('id').slice(3);
        
        if(myAppObj.client.logged) {
          pixKettle.plusingSocketModule.plusPerSocket(plusingSocket , pixId);
        } else {
          showFlashMessages([{ status : "error", text: "Musisz być zalogowany żeby głosować"}]);
        }
         
        return false
    });
}

pixKettle.tools.pixHashFun =  function() {
  var urlHash = document.location.hash;
    if (urlHash=="") {
    $(".galleryContainer").hide();
      $(".galleryContainer").remove();
      history.pushState("", document.title, window.location.pathname);
  }
};


//  -- START --
pixKettle.tools.start = function() {
  window.pixKettle.tools.plusButtonsClick();
  //window.onhashchange = pixKettle.tools.pixHashFun;
}