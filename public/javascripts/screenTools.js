window.pixKettle.tools.screenTools = {

	screenWatcher : function(callback) {
		return function() {
		pixKettle.tools.checkSize(function(xscreen) {
	      
	      if(xscreen && xscreen.name) {
	        if(xscreen.name != window.pixKettle.currentScreen.name ) {
	        	if(galleryOn)$(".galleryContainer").click();
	          	callback(xscreen);
	        }
	      }

	    })
		}
	},

	loadNoConnectionScreen : function() {
		var statement = 'Brak połączenia WebSocket.'
		var container = $("<div id=\"noConnectionMesssageContainer\"></div");
		var mCloud = $('<div id="fm1436904876327" role="alert" class="messageCloud error"></div>');
		var textEl = $('<span></span>',{text : statement});
		var imgEl = $('<img/>',{src : errKettle.src});
		imgEl.appendTo(mCloud);
		textEl.appendTo(mCloud);
		mCloud.appendTo(container);
		$( ".picScreen" ).before(container);
		$(".picScreen").remove();
	},


	changePixScreen :  function(xscreen, page, undo, socket) {

	  if(xscreen.width == false) {
	    $(".picScreen").attr("id", "picScreen"+window.pixKettle.tools.firstLetterUp(xscreen.name));
	    if(page != 'home')window.pixKettle.tools.clearCss();
	  } else {
	    if(page == 'home') {
	    	
	      $(".picScreen").attr("id", "picScreenPuzzled");
	      
	    } else {
	      $(".picScreen").attr("id", "picScreenFloat");
	    }
	  }
	  if(undo) {
	    if(undo && (undo.width == false && xscreen.width != false) || (undo.width != false  && xscreen.width == false)) {

	      this.cleanScreen();
	      socket.emit('getPoczekalnia');
	    } else if(xscreen.width && undo.width) {
	    	if(page == 'home') {
	    		this.cleanScreen();
	    		
	    	}
	      //window.pixKettle.tools.setCss($('.picBox'), xscreen.tileSize);
	    }
	  }
	  
	},

	cleanScreen : function() {
	  $(".picScreen").empty();
	},


	setImgToForm : function(img,setDim) {
	  var picBox = $("<div></div>", {class: "picBox", "id" : img._id});
	  var imgSec = $("<div></div>", {class: "imgSec", id: 'img'+img._id});
	  var imgX = $("<img></img>",{src: img.src});
	  imgX.appendTo(imgSec);  
	  if(setDim) imgX.css({width: img.dim, height: img.dim});
	  imgSec.appendTo(picBox);
	  var descSec = $("<div></div>", {class: "descSec"});
	  var descPan = $("<div></div>", {class: "descPan"});
	  var divTitle = $("<div></div>",{class : "title"}).text(img.title).appendTo(descPan);
	  var authorAv = img.authorAvatar || "/images/av.png";
	  var avImg = $('<img class = "avImg" alt="av">').attr('src', authorAv);
	  var divAuthor = $("<div></div>",{class : "author"}).append($('<a href="../../u/'+img.author+'"></a>').append(avImg).append('<span>'+img.author+'</span>')).appendTo(descPan); 
	  if(setDim) {
	    divTitle.css({'font-size' : parseInt(img.dim/11)});
	    avImg.css({'height' : parseInt(img.dim/15), width : 'auto', 'margin-right' : parseInt(img.dim/30) });
	    divAuthor.css({'font-size' : parseInt(img.dim/15)});
	  }
	  descPan.appendTo(descSec);
	  descSec.appendTo(picBox);
	  var pBtt = $("<button></button>", {class: "pointUpBtt", id : 'btt'+img._id}).html('<span>+</span>');
	  var pixPoints = $('<div class="pixPoints">'+img.points+'</div>');
	  if(setDim) {
	    var pBB = img.dim/90;
	    pBtt.css({'font-size' : img.dim/10,  'margin' :pBB, 'border-width' : pBB, 'width' : 'calc(100% - '+ (2*pBB-0.175*20)+'px)', 'height': 'calc(50% - '+ 2*pBB +'px)'});
	    pixPoints.css({'font-size' : img.dim/10});
	  }
	  $("<div></div>",{class : "badgeSec"}).append(pixPoints).append(pBtt).appendTo(descSec);
	  if(setDim){
	    picBox.css({top:img.top, left:img.left, display : 'block'});
	  } 
	  picBox
	  
	  return picBox;
	},


	SetImgsToScreen : function(images,puzzle,screen, callback) {
	    //$('.picScreen').hide();
	    var j=0
	    for(j=0;j < images.length; j++) {
	      window.pixKettle.tools.screenTools.setImgToForm(images[j],puzzle).appendTo($(".picScreen")).hide();
	    }
	    if(screen && !puzzle){
	      window.pixKettle.tools.setCss($(".picBox"), window.pixKettle.currentScreen.tileSize);
	    }
	    
	    // CHANGE HERE !!!!!
	    if(screen)imgDec();
	    window.pixKettle.tools.plusButtonsClick();
	    $(".imgSec").click(function(){
	    	//CHANGE HERE
	    	if(window.pixKettle.currentScreen.width) {
	    		showGallery($(this));
	    	} else {

	    		window.location.href = 'http://' + location.host+'/p/' + $(this).attr('id').slice(3);
	    	}
	      
	    })

	    //$('.picScreen').show();
	    if(images.length == 0) {
	    	
	      if(callback) callback();
	    } else {
	    	//CHANGE HERE
	      if(puzzle)animateHome(images,callback); else animateOther(images,callback);
	    }
	    
	    //$('.picBox').show();


	},

	removeImgsFromScreen : function( puzzle, callback ) {  
	 
	  $(".picBox .imgSec img").finish();
	  $(".picBox  .imgSec").finish();
	  $('.picBox').finish();
	  var toRemove ={};
	  var removed = [];
	  var removeArr = $('.picBox');
	  var nextUnPuzzled = function(val) {

	    if(val < removeArr.length - 1) {
	   
	      removeOtherAnimation($(removeArr.get(val)),function() {
	        $(removeArr.get(val)).remove();
	      });
	      nextUnPuzzled(++val);
	    } else {
	      removeOtherAnimation($(removeArr.get(val)) , function() {
	        $(removeArr.get(val)).remove();
	        if(callback) callback();
	      });
	    }
	  }


	   var nextPuzzled = function() {
	   	
	    xcallback = false;
	    if(!(removed.length == removeArr.length)) {
	      toRemove = Math.floor(Math.random()*removeArr.length);
	      while($.inArray(toRemove, removed) != -1) {
	        toRemove = Math.floor(Math.random()*removeArr.length);
	      }
	      removed.push(toRemove);
	      if(removed.length == removeArr.length)  xcallback = callback;
	      removeBigHomeAnimation($(removeArr.get(toRemove)), function() {
	        $(removeArr.get(toRemove)).remove();
	        if(xcallback) xcallback();
	      });
	      setTimeout(function() { nextPuzzled(); }, 50);
	    }
	  }

	  if(!puzzle) {
	    
	    if(removeArr.length && removeArr.length > 0) {
	      nextUnPuzzled(0,  callback);
	    }  else {
	      if(callback) callback();
	    }
	  } else {
	   if(removeArr.length && removeArr.length > 0) {
	      nextPuzzled(0, removeArr, callback);
	    }  else {
	      if(callback) callback();
	    }
	    
	  } 
	  
	   //removeOtherAnimation(id, endFun); else removeBigHomeAnimation (id, endFun);

	  /*var endFun = function(){
	    $("#"+id).remove();
	    if(callback)callback();
	  }*/

	  
	}

};