$(document).ready(function(){
	var currId = $('.picBox').attr('id');
	//var usSocket;
	var usSocket
	if (!usSocket || !usSocket.connected) {
		usSocket = io('http://' + location.host + '/updateStream');
	}
	usSocket.on('connect', function () {       
		console.log('Nawiązano połączenie przez usSocket.io');
	});

	usSocket.on('connect_failed', function(){
		loadNoConnectionScreen();
	});

	usSocket.on('connect_error', function() {
		loadNoConnectionScreen();			    	
	});

	usSocket.on('disconnect', function() {
		usSocket.disconnect();
		loadNoConnectionScreen();
	});


	usSocket.on("action", function (data) {
		if(data.id == currId) {
			if(data.type == 'plus') {
				$('.pixPoints').text(data.points);
			} else if(data.type == 'delete' || data.type == 'deleteAll') {
				
			} else {
				console.log('niespodziewane');
			}
			
		}
	});

});