if(!window.pixKettle) {
	window.pixKettle = { };
} 

window.pixKettle.home = {};
pixKettle.home.tools = {};

var currentPicScreenHeight = 0;
var updateFunx, funx ;

pixKettle.home.tools.loadHomeUploadInfo = function() {
	$.get( '/homeUploadInfo.html', function( data ) {
  		$('.contentContainer #flashContainer').after(data);
  		$('#uploadInfoBtt').click(function() {
			location.href = 'http://' + location.host+'/upload';
		});

		$('.closeCloud').click(function(){
			$('.homeUploadInfo').slideUp('slow', function(){
				$(this).remove();
			})
		})
	});
}


window.pixKettle.home.tools.removePix = function(id,callback) {
	//console.log(id);

	var endFun = function(){
		$("#"+id).remove();
		if(callback)callback();
	}
	var xnode = $("#"+id);
	if(pixKettle.currentScreen.width == false ) removeOtherAnimation(xnode, endFun); else removeBigHomeAnimation (xnode, endFun);
}


window.pixKettle.home.tools.addPix = function(newPix,id,callback) {
	
	var setDim = true;
	if(pixKettle.currentScreen.width == false ) setDim = false;

	//console.log('add pix');
	if(id == null) {
		pixKettle.tools.screenTools.setImgToForm(newPix,setDim).hide().prependTo($(".picScreen"));
	} else {
		
		pixKettle.tools.screenTools.setImgToForm(newPix,setDim).hide().insertAfter($( ".picScreen > #" + id ));
	} 

	if(setDim) {
		$('#'+newPix._id+".picBox").mouseenter(function() {
		    $(this).children(".descSec").stop();
		    $(this).children(".descSec").slideDown("slow");
		  });


	  $('#'+newPix._id+".picBox").mouseleave(function() {
	    $(this).children(".descSec").stop();
	    $(this).children(".descSec").slideUp("slow");
	  });
	}
	
    $('#'+newPix._id+" .pointUpBtt" ).click(function() {
    //console.log('elo');
		 //console.log('elo');
    var pixId = $(this).attr('id').slice(3);
      
        if(myAppObj.client.logged) {
          pixKettle.plusingSocketModule.plusPerSocket(plusingSocket , pixId);
        } else {
          showFlashMessages([{ status : "error", text: "Musisz być zalogowany żeby głosować"}]);
        }
         
        return false
    });
    $('#'+newPix._id+" .imgSec").click(function(){
      showGallery($(this));
    })
	showImg(newPix, setDim, callback);
}

$(document).ready(function(){
	pixKettle.home.tools.loadHomeUploadInfo();
	pixKettle.home.tools.animationEngineObj = animationEngine();
	
});