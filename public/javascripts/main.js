var showFlashMessages

$(document).ready(function(){
	flashMessagesLib.quickStart();
	showFlashMessages = flashMessagesLib.showFlashMessages;
	
	if(myAppObj.startFlashMessages) {
		showFlashMessages(myAppObj.startFlashMessages);
	}
	
	if(myAppObj.client.logged) {
	    window.pixKettle.tools.navbarTools.logOnNavbar(myAppObj.client.user);
	 }

	  if(myAppObj.page == 'login') {
	  	$('.contentContainer').first().attr('id', 'formScreen');
	    $("#picScreenFloat").removeClass('picScreen');
	    
	  }

	  if(myAppObj.page == 'anteroom') {
	  	$('#picScreen').attr('class', 'picScreenPoczekalnia');
	    
	  }

	 

	  if(myAppObj.page == "singlePicture" || myAppObj.page ==  'upload') {
	  	$('#picScreenFloat').attr('id', 'picScreenFull');
	  }

	  



	  pixKettle.tools.start();


	
});