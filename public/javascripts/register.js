var checkObj = {login: true, password: true, confirmPassword: true};


var clearAllStatuses = function() {
	var fields = ["global", "password", "passwordConfirm", "login"];
	for(var j =0; j <  fields.length ; j++ ) {
		window.pixKettle.tools.removeStatus($('#'+fields[j]+'Input'), '<status>');
		window.pixKettle.tools.removeStatus($('#formMessage'+ window.pixKettle.tools.firstLetterUp(fields[j])),'<status>');
		$('#formMessage'+ window.pixKettle.tools.firstLetterUp(fields[j])).empty();
		$('#formMessageGlobal').slideUp();
	}
	
}




var checkPassword = function(pass) {

	if(pass.length < 8) {
		var obj ={status : 'error', text:'Podane hasło jest za krótkie', field : 'password'};
		setFormMssg(obj);
	} else if (pass.length > 30) {
		var obj ={status : 'error', text:'Podane hasło jest za długie', field : 'password'};
		setFormMssg(obj);
	} else {
		$.ajax({
	      method: "GET",
	      url: "/checkPassword/"+pass,
	      error: function(){
	      	obj ={status : 'error', text:'Wystąpił nieznany błąd', field : 'password'};
	         setFormMssg(obj)
	      },
	      success: function(data){
	      		obj = data;
	      		obj.field = 'password';

	          setFormMssg(obj);
	          if(obj.status == 'success' && $('#passwordConfirmInput').val() != "") checkPasswordConfirm($('#passwordConfirmInput').val());
	      },
	      timeout: 3000 // sets timeout to 3 seconds
	  });
	}
}



var checkPasswordConfirm = function(pass) {
	if(checkObj.password) {
		if(pass != $('#passwordInput').val()) {
			var obj ={status : 'error', text:'', field : 'passwordConfirm'};
			setFormMssg(obj);
		} else {
			setFormMssg({status:'success',text:'',field:'passwordConfirm'});
		}
	}
}


var setFormMssg = function(objekt) {
	var subName = window.pixKettle.tools.firstLetterUp(objekt.field);
	var mssgString = '';
	if(objekt.status == 'success') {
		checkObj[objekt.field] = true;
	} else {
		checkObj[objekt.field] = false;
	}
	mssgString += objekt.text;
	$('#formMessage'+ subName).text(mssgString);
	window.pixKettle.tools.setStatus($('#formMessage'+ subName), objekt.status, '<status>');
	window.pixKettle.tools.setStatus($('#'+objekt.field+'Input'), objekt.status, '<status>');
	if(subName == 'Global')$("#formMessageGlobal").slideDown('slow');
}

var checkLogin = function(login) {
	if(login.length < 3) {
		var obj ={status : 'error', text:'Podany login jest za krótki', field : 'login'};
		setFormMssg(obj);
	} else if (login.length > 30) {
		var obj ={status : 'error', text:'Podany login jest za długi', field : 'login'};
		setFormMssg(obj);
	} else {
		$.ajax({
          method: "GET",
          url: "/checkLogin/"+login,
          error: function(){
          	obj ={status : 'error', text:'Wystąpił nieznany błąd', field : 'global'};
             setFormMssg(obj)
             $("#formMessageGlobal").slideDown('slow');
          },
          success: function(data){
          		obj = {};
          		if(data.status == 'ok') {
          			if(data.result) {
          				obj.status = 'success';
          			} else {
          				obj.status = 'error';
          			}
          		} else {
          			obj.status = 'error';
          		}
          		obj.text = data.text;
          		obj.field = 'login';

              setFormMssg(obj)
          },
          timeout: 3000 // sets timeout to 3 seconds
      });
	}
} 

var sendRegisterForm = function() {
	var data =  $("#registerForm").serialize();
	$.ajax({
          method: "POST",
          url: "/rejestracja",
          data: data,
          error: function(){
          	obj ={status : 'error', text:'Wystąpił nieznany błąd', field : 'global'};
            setFormMssg(obj)
          },
          success: function(data){
              if(data.status == 'success') {
               	window.location.replace('http://' + location.host+'/');
              } else {
              	setFormMssg({status: 'error', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global'});
              	for(i=0;i< data.formMessages.length;i++) {
              		setFormMssg(data.formMessages[i]);
              	}
              }
          },
          timeout: 3000 // sets timeout to 3 seconds
      });
}


$(document).ready(function(){

	$('#registerForm').submit(false);
	$('#loginInput').focusout(function() {
		checkLogin($(this).val());
	});
	$('#passwordInput').focusout(function() {
		checkPassword($(this).val());
	});
	$('#passwordConfirmInput').focusout(function() {
		checkPasswordConfirm($(this).val());
	});

	$('.standardInput').focusin(function() {
		$(this).attr("class","standardInput");
		var bufName = window.pixKettle.tools.firstLetterUp($(this).attr('name'));
		$('#formMessage'+bufName).text('').attr("class","inputInfo");
	})

	$('#registerSubmit').click(function() {
		checkLogin($('#loginInput').val());
		checkPassword($('#passwordInput').val());
		checkPasswordConfirm($('#passwordConfirmInput').val());
		if(checkObj.login && checkObj.password && checkObj.passwordConfirm) {
			sendRegisterForm();
			clearAllStatuses();
		} else {
			setFormMssg({status: 'error', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global'});
			$("#formMessageGlobal").slideDown('slow');
		}
	})
})