
var setFormMssg = function(objekt,field) {
	var subName = window.pixKettle.tools.firstLetterUp(field);
	var mssgString = '';
	mssgString += objekt.text;
	$('#formMessage'+ subName).text(mssgString);
	window.pixKettle.tools.setStatus($('#formMessage'+ subName), objekt.status, '<status>');
	window.pixKettle.tools.setStatus($('#'+objekt.field+'Input'), objekt.status, '<status>');
}



$(document).ready(function(){
	$('#loginPageBtt').click(function() {
		$("#formMessageGlobal").slideUp('slow');
		window.pixKettle.tools.logFun('loginPageCloud', function(messages){
			for(var j=0; j< messages.length; j++) {
				setFormMssg(messages[j],'global');
				$("#formMessageGlobal").slideDown('slow');
			}
		});
  		return false;
	});
});