var hideLoginCloud = function() {
  $("#loginShadow").hide();
  $("#loginCloud").stop();
  $("#loginCloud").slideUp("slow");
}

var showLoginCloud = function() {
  $("#loginShadow").show();
  $("#loginCloud").stop();
  $("#loginCloud").slideDown("slow");
}

var animateLoginCloud = function(callback) {
  $("#loginNav").click(function() {
    return false;
  });
  

  $("#loginNav").hover(function() {
    showLoginCloud();
  }, function() {
    if(!$("#loginCloudPasswordInput").is(":focus") && !$("#loginCloudNameInput").is(":focus")) {
      hideLoginCloud();
    }
  }

  );

  $("#loginCloud").mouseenter(function() {
    showLoginCloud();
  });


  $('#loginCloud').mouseleave(function() {
    if(!$("#loginCloudPasswordInput").is(":focus") && !$("#loginCloudNameInput").is(":focus")) {
      hideLoginCloud();
    }
  });

  $("#loginCloudPasswordInput").focusout( function(){
    if(!$('#loginCloud').is(':hover') && !$("#loginNav").is(':hover'))
    hideLoginCloud();
  });

  $("#loginCloudNameInput").focusout( function(){
    if(!$('#loginCloud').is(':hover') && !$("#loginNav").is(':hover'))
    hideLoginCloud();
  });


  $("#loginCloudPasswordInput").focus( function(){
    
    showLoginCloud();
  });

  $("#loginCloudNameInput").focus( function(){
    
    showLoginCloud();
  });

  callback();
}


var animateUserCloud = function() {
  
  $("#userCloudBtt").click(function() {
    $("#userCloud").stop();
    $( "#userCloud" ).slideToggle( "slow", function() {
    // Animation complete.
    });
  });
}


var animateDropDown = function() {
  $(".mobileDropDown").click(function() {
     $( "#panels" ).slideToggle( "fast", function() {
    // Animation complete.
    });
  });
}

$(document).ready(function(){
  var small = false;
  if($( window ).width() < 700) {
    small = true;
    animateDropDown();
  } else {
    if(!myAppObj.client.logged) {
       $("<div id=\"loginCloud\"></div>").load("../../login/form", function() {
          animateLoginCloud(function() {
           $('#loginNavBtt').click(function() {
              window.pixKettle.tools.logFun('loginForm', showFlashMessages);
              return false;
            })
          });
          
      }).appendTo($("#navContent"));
    }
  }
  $( window ).resize( function() {
    if($( window ).width() < 700 ) {
      if(!small) {
        small = true;
        $("#panels").hide();
        $( "#loginNav" ).off( 'click');
        $( "#loginNav" ).off( 'mouseenter mouseleave');
        $( "#loginCloud" ).off('mouseenter mouseleave');
        $("#loginCloud").remove();
        $( "#userCloudBtt" ).off( 'click');
         $(".mobileDropDown").show();
        animateDropDown();
        $( "#userCloud" ).show();
      }
      
    } else {
      if(small) {
        small=false;
        $(".mobileDropDown").off("click");
        $(".mobileDropDown").hide();
        $("#panels").show();
        $("<div id=\"loginCloud\"></div>").load("../../login/form", function() {
              animateLoginCloud(function() {
               
              });
              
          }).appendTo($("#navContent"));
        $( "#userCloud" ).hide();
        animateUserCloud();
      }
    }
  })

	 
  
   
  //$(".mobileDropDown").click();
});