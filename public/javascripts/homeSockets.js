if(!window.pixKettle) {
	window.pixKettle = { };
	window.pixKettle.home = { };
} else if (!window.pixKettle.home) {
	window.pixKettle.home = { };
}

var socket

window.pixKettle.currentScreen = {};
window.pixKettle.currentPicScreenHeight;
pixKettle.home.postRemove = false;


window.pixKettle.home.socketTools = {};
window.pixKettle.home.socketTools.resizeFun = function(currSocket){

	return function(xscreen) {
		var undo = window.pixKettle.currentScreen;
		window.pixKettle.currentScreen = xscreen;
		pixKettle.tools.screenTools.changePixScreen(xscreen, 'home');
		currSocket.emit('resize',{current : xscreen.name, undo : undo.name});
	};
	
};

window.pixKettle.home.socketTools.handleScreenSizes = function(screenSize) {
	bufor = window.pixKettle.currentScreen;
	window.pixKettle.currentScreen = screenSize;

	if($.isEmptyObject(bufor)) {
		pixKettle.tools.screenTools.changePixScreen(screenSize, 'home');
		$( window ).resize(pixKettle.tools.screenTools.screenWatcher(window.pixKettle.home.socketTools.resizeFun(socket)));
	}

	if(screenSize) {
		//console.log(screenSize.name);
		socket.emit('getScreen', screenSize.name);
	} else {
		//jakis error
	}
};

pixKettle.home.socketTools.handleImages = function (data) {
	
	//console.log('socket Images');
	//console.log(data);
    var ximages = data.images;
    var xpuzzle = data.puzzle;
    var xvar = !(window.pixKettle.currentScreen.width == false);
    //console.log(images);
   
    if(data.puzzle) {

    	//var autoHeight = $('#first').height();
    	if(data.height > 0) {
    		
    		
    		$(".picScreen").animate({height : data.height},'slow',function(){
    		
    		});

    	} else {
    		$(".picScreen").css("height","auto");
    	}
    } else {
    	$(".picScreen").css("height","auto")
    }
   
    pixKettle.home.tools.animationEngineObj.resetAE();
    pixKettle.home.tools.animationEngineObj.main({
		action: 'opening',
		funx : function(callback) {

			pixKettle.tools.screenTools.cleanScreen();
			pixKettle.tools.screenTools.SetImgsToScreen(ximages, xpuzzle, xvar, callback); 
			
		}
	});
    
};



pixKettle.home.socketTools.disconnectFun = function(xsocket) {
	xsocket.disconnect();
	if(!pixKettle.home.tools.animationEngineObj.checkRemoveAll) {
		window.pixKettle.tools.screenTools.cleanScreen();
		window.pixKettle.tools.screenTools.loadNoConnectionScreen();
	} else {
		setTimeout(function() {
			if(!xsocket.connected) {
				window.pixKettle.tools.screenTools.cleanScreen();
				window.pixKettle.tools.screenTools.loadNoConnectionScreen();
			}
		}, 5000);
	}
};

pixKettle.home.socketTools.removeAllfun = function(xscreen) {
	var puzzled = true;
	if(xscreen.width == false) {
		puzzled = false;
	} 
	pixKettle.home.tools.animationEngineObj.main({
		action: 'removeAll',
		funx : function(callback) {
			pixKettle.tools.screenTools.removeImgsFromScreen(puzzled, function() {
				$(".picScreen").css("height","auto");
				pixKettle.home.postRemove = true;
				callback();
			});
		}
	});
};

pixKettle.home.socketTools.removePixFun = function(idx) {
 	funx = function() {
		var funxId = idx;
		return function(callback) {
			window.pixKettle.home.tools.removePix(funxId, callback);
		}
	}();
	pixKettle.home.tools.animationEngineObj.main({
		action: 'removePix',
		id: idx,
		funx : funx
	});
 };


pixKettle.home.socketTools.newPixFun =  function(xnewPix, lbrother) {
	
 	funx = function() {
		var funxNewPix = $.extend({}, xnewPix);
		//data.leftBrother
		var funxLeftBrother = lbrother;
		return function(callback) {
			//console.log(funxLeftBrother);
			pixKettle.home.tools.addPix(funxNewPix, funxLeftBrother, callback);
		}
	}();
	pixKettle.home.tools.animationEngineObj.main({
		action: 'newPix',
		id: xnewPix._id,
		nextId : lbrother,
		funx : funx
	})
 }


pixKettle.home.socketTools.updatePixFun =  function(xid, xchangeObj) {
	//console.log(++updateCounter+'....  UPDATE ....................................');
	updateFunx = function() {
		var funxId = xid;
		var funxChangeObj = jQuery.extend({},xchangeObj);
		return function(callback) {
			updatePix(funxId, funxChangeObj, callback);
		}
	}();
	pixKettle.home.tools.animationEngineObj.main({
		action: 'updatePix',
		id: xid,
		funx : updateFunx
	})
}




pixKettle.home.socketTools.newPicScreenHeightFun = function(xheight) {
	var newHeight = NaN;
	if(xheight)newHeight = parseInt(xheight);
	if(!isNaN(newHeight) && window.pixKettle.currentPicScreenHeight != newHeight) {
		window.pixKettle.currentPicScreenHeight = newHeight;

		funx = function() {
				var funHeight = xheight;
				return function(callback) {
	    			$(".picScreen").animate({ height : funHeight},function(){
	    				if(funHeight == 0) {
	    					$(".picScreen").css("height","auto");
	    				}
	    				if(callback)callback();
	    			});
				}
			}();
		pixKettle.home.tools.animationEngineObj.main({
			action: 'ScreenHeightChange',
			funx : funx
		})
		
	}
}

pixKettle.initFun = function(){

	//SOCKET CONNECTION STUFF
	if (!socket || !socket.connected) {
		socket = io('http://' + location.host + '/home',{ 'connect timeout': 3000});
	}

	socket.on('connect', function () {       
		console.log('Nawiązano połączenie przez Socket.io(/home)');
	});

	socket.on('connect_failed', function(){
		alert('home socket connection fail');
    	window.pixKettle.tools.cleanScreen();
		window.pixKettle.tools.screenTools.loadNoConnectionScreen();
	});

	socket.on('connect_error', function() {
		alert('home socket connection error');
		window.pixKettle.tools.screenTools.cleanScreen();
		
			    	
	});

	socket.on('disconnect', function() {
		alert('home socket disconnected');
    	
    	pixKettle.home.socketTools.disconnectFun(socket);
    	window.pixKettle.tools.screenTools.loadNoConnectionScreen();
    	socket.conn.close();
     });


	

	//WHEN SCREEN CHANGE
    socket.on("screenChange", function (data) {
    	//console.log(' ---- screenChange ---');
		//console.log(data);
		if(data) {
			window.pixKettle.tools.screenTools.cleanScreen();
			//style
			setLoadingKettle();
		} 
	});

    //GET IMAGES
	socket.on("images", pixKettle.home.socketTools.handleImages );

	socket.on("updateScreen", function (data) {
		//console.log(' ---- updateScreen ---');
		//console.log(data);
		switch(data.type) {

			case "updatePix":
		    
		    	if(data.screen && data.screen == window.pixKettle.currentScreen.name|| (window.pixKettle.currentScreen.width == false && data.screen == 'other'))pixKettle.home.socketTools.updatePixFun(data.id, data.changeObj);
		    	//console.log(data);
		    	break;

		    case "removeAll":
		    	
				pixKettle.home.socketTools.removeAllfun(window.pixKettle.currentScreen);	    	
		        break;

		    case "resizeAll":
		        
		        break;


		    case "removePix": //remove here

            	if(window.pixKettle.currentScreen.name == data.screen  || (window.pixKettle.currentScreen.width == false && data.screen == 'other')) {
		    		
		    		
		    		pixKettle.home.socketTools.removePixFun(data.id);
		    		
		    	} 
		        break;

		    case "newPix":

		    	//console.log("new pix");
		    	
		    	if(window.pixKettle.currentScreen.name == data.screen  || (window.pixKettle.currentScreen.width == false && data.screen == 'other')) {
		    		
		    	
		    		pixKettle.home.socketTools.newPixFun(data.newPix, data.leftBrother);
		    		
		    	} 
		        break;

			case "updatePixSmall":
			   
		    	if(window.pixKettle.currentScreen.width == false) {
		    		updatePixSmall(data.id, data.changeObj);

		    	}
		        break;

	        case "screenStarted":
	        	//console.log('nowy ekran');
		    	//console.log(currentScreen.name);
		    	socket.emit('getScreen', window.pixKettle.currentScreen.name);
		        break;


		}
	});
		
		

		socket.on("newPicScreenHeight", function (data) {

			
			if(data.toScreen && data.toScreen == window.pixKettle.currentScreen.name) {
				pixKettle.home.socketTools.newPicScreenHeightFun(data.height);
			}
		});

	 

	window.pixKettle.tools.checkSize(window.pixKettle.home.socketTools.handleScreenSizes);

};
