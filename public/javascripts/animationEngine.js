var animationEngine = function() {
	var obj={};
	var state = false;
	var openingAnimation = false;
	var animStock = [];
	var busy = false;
	var queue =[];
	var nextInArrState = false;
	var idCounter = 0;
	var blocked =false;
	

	obj.resetAE = function() {
		 obj={};
	 state = false;
	 openingAnimation = false;
	 animStock = [];
	 busy = false;
	 queue =[];
	 nextInArrState = false;
	 idCounter = 0;
	 blocked =false;
	
	}


	var nextId = function() {
		return ++idCounter;
	}

	obj.main = function(obj) {
		//console.log('--- main function ---');
		//alert(String(obj.id).slice(-2));
		//console.log(animStock);
		obj.funId = nextId();
		if(!busy && queue.length == 0  && !blocked) {
		
			busy = true;
			setAnimation(obj);
			if(queue && queue.length  == 0) {
		
				busy = false;
			} else {
				nextInQueue();
			}
		} else {
		
			queue.push(obj);
		}
	};





	var nextInQueue = function () {
		if(queue &&  queue.length > 0) {
			setAnimation(queue.shift());
			if(queue && queue.length  == 0) {
				busy = false;
			} else {
				nextInQueue();
			}
		}
	};



	var clearStock = function() {
		if(!blocked) {
			blocked = true;
			var bufArr = [];
			for(var k=0;k<animStock.length;k++) if(!animStock[k].started){ bufArr.push(animStock[k]); animStock.splice(k, 1); k--; } 
			queue = bufArr.concat(queue);
			nextInQueue();
			blocked = false;
		}
	};

	obj.checkRemoveAll = function() {
		return checkStock({ action : 'removeAll'});
	}

	var checkStock =function(actions) {
		var j = 0, done = false, k=0;
		while( j < animStock.length && !done) {
		
			k = 0;
			while( k < actions.length && !done) {
				if(actions[k].action == animStock[j].action) done = true;
				if(done && actions[k].id) if(!(actions[k].id== animStock[j].id))done = false;
				if (done && actions[k].nextId) if (!(actions[k].nextId == animStock[j].nextId))done = false;
				k++;
			}
			j++;
		}
		return done;
	};


	var findInStock = function(funId) {
		var j = 0;
		while(j < animStock.length && animStock[j].funId !== funId)j++;
		
		if(j==animStock.length) return -1; else return j;
	};

	var removeFromStock = function(funId) {
		var elIndex = findInStock(funId);
		//alert('el index'+elIndex);

		if(elIndex > -1) {
			animStock.splice(elIndex, 1);
			
		} else {
			//console.log('err');
		}
	}

	var start = function(obj) {
		//console.log('start '+obj.funId);
		obj.started = true;
		animStock.push(obj);
		obj.funx(function(){
			removeFromStock(obj.funId);
			clearStock();
		})
	}
	
	var setAnimation = function(obj) {
		//alert(obj.action);
		switch (obj.action) {

		    case 'opening':
		    	if(animStock && animStock.length > 0) {
		    		
		    		animStock.push(obj);
		    	} else {
		    		start(obj);
		    	}
		        break;
		    case 'ScreenHeightChange':
		    	if(animStock && animStock.length > 0) {
		    		animStock.push(obj);
		    	} else {
		    		start(obj);
		    	}
		        break;
		    case 'newPix':
		    	//alert('newpix + len animStock' + animStock.length);
		    	//alert('leftbrat:'+obj.nextId);
		    	if(animStock && animStock.length > 0 && checkStock([{action :'opening'}, {action :'ScreenHeightChange'} ,{ action : 'newPix', id : obj.nextId}])) {
		    		//alert('falsze');
		    		animStock.push(obj);
		    	} else {
		    		start(obj);
		    	}
		        break;
		    case 'removePix':
		    	if(animStock && animStock.length > 0 && checkStock([{action :'opening'}, {action :'ScreenHeightChange'},{ action : 'newPix', nextId : obj.id}])) {
		    		animStock.push(obj);
		    	} else {
		    		start(obj);
		    	}
		        
		        break;
		    case 'removeAll':
		   		animStock=[];
		       	start(obj);
		        break;
		    case 'updatePix':
		        if(animStock && animStock.length > 0 && checkStock([{action :'opening'},{ action : 'newPix', id : obj.id }])) {
		    		animStock.push(obj);
		    	} else {
		    		start(obj);
		    	}
		        break;
		    default: 
		       console.log('nieznana akcja');
		}
	}


	return obj;
}