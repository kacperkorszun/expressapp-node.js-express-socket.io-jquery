const myAppObj = {};

myAppObj.conf = {
	develop : true,
	noMessage : false,
	noSuccessMessage : false,
	noErrorMessage : false,
	noInfoMessage : false,
	noExtraMessages : true
}