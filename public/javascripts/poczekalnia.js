//.live( 
var socket
var undoPage  = 0;

var idsArr = [];

var pageInfo = {
	page : 0,
	isEnd : false
};


pixKettle.initFun = function(){
	console.log('wow');
	socket.emit('getPoczekalnia');
}



var resizeAll = function(dim) {
	$(".picBox .imgSec").css({height: dim, width: dim});
	$(".picBox .imgSec img").css({height: dim, width: dim});
}

var deletePix = function(id) {
	$("#"+id).remove();
}

var addPix = function(newPix , id) {
	if(id == null) {
		setImgToForm(newPix).prependTo($("#picScreen"));
	} else {
		$( "#" + id ).after( setImgToForm(newPix) );
	}
	showImg(newPix);
}

var updatePix = function(id, changeObj) {
	$('#'+id+" .pixPoints").text(changeObj.points);
	if(dim) {
		$("#"+id+" .picBox .imgSec").css({height: changeObj.dim, width: changeObj.dim});
		$("#"+id+" .picBox .imgSec img").css({height: changeObj.dim, width: changeObj.dim});
	}
}


var refreshFun = function(socket) {
	socket.emit('refresh');
}



var changePoczekalniaScreen = function(undo, xscreen)  {
	if(xscreen.width != false) {
		$(".picScreen").attr("id", "picScreenFloat");
	} else {
		$(".picScreen").attr("id", "picScreen"+pixKettle.tools.firstLetterUp(pixKettle.currentScreen.name));
		pixKettle.tools.clearCss();
	}
	if(undo && (undo.width == false && xscreen.width != false) || (undo.width != false  && xscreen == false)) {
		pixKettle.tools.screenTools.cleanScreen();
		socket.emit('getPoczekalnia');
	} else if(xscreen.width && undo.width) {
		pixKettle.tools.setCss($('.picBox'), pixKettle.currentScreen.tileSize);
	}
}


var setIdsArr = function(images) {
	 for(var j=0;j<images.length;j++) {
		idsArr.push(images[j]._id);
	}
}


var compareIdsArrs = function(first, second) {
	if(first.length == second.length) {
		var k =0;
		while(first[k] == second[k] && k < first.length) k++;
		if(k == first.length) return true; else return false;
	} else {
		return false;
	}
} 

var loadButtonFun = function  () {
	if(pageInfo.isEnd) {
		$('#loadMore').hide();
	} else {
		$('#loadMore').show();
	}
}


var loadNextPage = function() {

}


$(document).ready(function(){



	$('#loadMore').click(function() {
		$('#loadMore').hide();
		var pagex = pageInfo.page +1;
		socket.emit('getPage', pagex);
	})

	

	//var socket;
	if (!socket || !socket.connected) {
		socket = io('http://' + location.host + '/poczekalnia');
	}
	socket.on('connect', function () {       
	            console.log('Nawiązano połączenie przez Socket.io');
	        });

	socket.on('connect_failed', function(){
    	pixKettle.tools.screenTools.cleanScreen();
		loadNoConnectionScreen();
	});

	socket.on('connect_error', function() {
		pixKettle.tools.screenTools.cleanScreen();
		loadNoConnectionScreen();
			    	
	});

	socket.on('disconnect', function() {
			    	
			    	socket.disconnect();
			    	pixKettle.tools.screenTools.cleanScreen();
			    	loadNoConnectionScreen();
			     });


	

		socket.on("screenChange", function (data) {
		if(data) {
			window.pixKettle.tools.screenTools.cleanScreen();
			//style
			setLoadingKettle();
		} 
	});
	
	socket.on("images", function (data) {
				
				window.pixKettle.tools.screenTools.cleanScreen();
	            var images = data.images;
	            if(pixKettle.currentScreen.width == false) {
	            	
	            	window.pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, false);     
	            } else {
	            	window.pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, true);     
	            }
	            setIdsArr(data.images);
	            pageInfo.page = 0;
	            pageInfo.isEnd = data.isEnd;
	            loadButtonFun();
	            if(undoPage > pageInfo.page && !pageInfo.isEnd) {
	            	$('#loadMore').click();
	            }
	        });


	socket.on("nextPage", function (data) {
				
	            var images = data.images;
	            if(pixKettle.currentScreen.width == false) {
	            	window.pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, false);     
	            } else {
	            	window.pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, true);     
	            }
	            setIdsArr(data.images);
	            pageInfo.page++;
	            pageInfo.isEnd = data.isEnd;
	            loadButtonFun();
	              if(undoPage > pageInfo.page && !pageInfo.isEnd) {
	            	$('#loadMore').click();
	            }
	           
	        });


	socket.on("refreshBack", function (data) {
		//cleanScreen();

		if(!compareIdsArrs(idsArr, data.ids)) {
			pixKettle.tools.screenTools.cleanScreen();
			undoPage = pageInfo.page;
			socket.emit('getPoczekalnia');
		} else {
					
	}
    
    socket.on('screenSizes' , function(data) {
    	screenSizes = data;
    	console.log(data);
    	pixKettle.tools.checkSize(function(screenSize) {
    		bufor = pixKettle.currentScreen;
    		pixKettle.currentScreen =screenSize;
    		if($.isEmptyObject(bufor)) {
    			//changePoczekalniaScreen(false, screenSize);
    			pixKettle.tools.screenTools.changePixScreen(screenSize, 'poczekalnia', false, socket);
				$( window ).resize(pixKettle.tools.screenTools.screenWatcher(function(xscreen){
					var undo = pixKettle.currentScreen;
					pixKettle.currentScreen = xscreen;
					//changePoczekalniaScreen(undo, currentScreen);
					pixKettle.tools.screenTools.changePixScreen(xscreen, 'poczekalnia', undo, socket);
				}));
			}
    		if(screenSize) {
    			socket.emit('getPoczekalnia');
    		} else {
    			//jakis error
    		}
    		
    	});
    })

    //socket.emit('initial');

	
    var socket2 = io('http://' + location.host + '/updateStream');

    socket2.on('connect', function () {       
	            console.log('Nawiązano połączenie przez socket2.io');
	        });

	socket2.on('connect_failed', function(){
    	//cleanScreen();
		loadNoConnectionScreen();
	});

	socket2.on('connect_error', function() {
		//cleanScreen();
		loadNoConnectionScreen();
			    	
	});

	socket2.on('disconnect', function() {
			    	socket2.disconnect();
			    	//cleanScreen();
			    	loadNoConnectionScreen();
			     });


	socket2.on("action", function (data) {
		console.log('refresh');
		refreshFun(socket);
	});


});