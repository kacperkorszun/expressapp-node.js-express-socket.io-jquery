if(!window.pixKettle) {
	window.pixKettle = { };
	window.pixKettle.home = { };
} else if (!window.pixKettle.home) {
	window.pixKettle.home = { };
}

var socket

var idsArr = [];

var pageInfo = {
	page : 0,
	isEnd : false
};

var refreshFun = function() {
	socket.emit('refresh', usId);
}



var changePoczekalniaScreen = function(undo, xscreen)  {
	if(xscreen.width != false) {
		$(".picScreen").attr("id", "picScreenFloat");
	} else {
		$(".picScreen").attr("id", "picScreen"+pixKettle.tools.firstLetterUp(window.pixKettle.currentScreen.name));
		pixKettle.tools.clearCss();
		socket.emit('getUsersPixs', usId);
	}
	if(undo && (undo.width == false && xscreen.width != false) || (undo.width != false  && xscreen == false)) {
		pixKettle.tools.screenTools.cleanScreen();
		socket.emit('getUsersPixs', usId);
	} else if(xscreen.width && undo.width) {
		pixKettle.tools.setCss($('.picBox'), window.pixKettle.currentScreen.tileSize);
	}
}


var setIdsArr = function(images) {
	 for(var j=0;j<images.length;j++) {
		idsArr.push(images[j]._id);
	}
}


var compareIdsArrs = function(first, second) {
	if(first.length == second.length) {
		var k =0;
		while(first[k] == second[k] && k < first.length) k++;
		if(k == first.length) return true; else return false;
	} else {
		return false;
	}
} 

var loadButtonFun = function  () {
	if(pageInfo.isEnd) {
		$('#loadMore').hide();
	} else {
		$('#loadMore').show();
	}
}

pixKettle.initFun = function(){
	$('#loadMore').click(function() {
		var pagex = pageInfo.page +1;
		socket.emit('getPage', {page : pagex , usId : usId});
	})



	//var socket;
	if (!socket || !socket.connected) {
		socket = io('http://' + location.host + '/usersPixs');
	}
	socket.on('connect', function () {       
	            console.log('Nawiązano połączenie przez Socket.io');
	            socket.emit('getUsersPixs', usId);
	        });

	socket.on('connect_failed', function(){
    	pixKettle.tools.cleanScreen();
		loadNoConnectionScreen();
	});

	socket.on('connect_error', function() {
		pixKettle.tools.cleanScreen();
		loadNoConnectionScreen();
			    	
	});

	socket.on('disconnect', function() {
    	socket.disconnect();
    	window.pixKettle.tools.cleanScreen();
    	loadNoConnectionScreen();
     });


	

		socket.on("screenChange", function (data) {
		if(data) {
			cleanScreen();
			var divx = $('<div></div>',{class: 'loadingPanel'});
			$('<img src="/images/loadingKettle2.gif" alt="loading ..." />').appendTo(divx);
			divx.appendTo($('.picScreen'));
		} 
	});
	
	socket.on("images", function (data) {
				window.pixKettle.tools.screenTools.cleanScreen();
				console.log(data);
	            var images = data.images;
	            console.log(images);
	            if(window.pixKettle.currentScreen.width == false) {
	            	pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, false);     
	            } else {
	            	window.pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, true);     
	            }
	            setIdsArr(data.images);
	            pageInfo.page = 0;
	            pageInfo.isEnd = data.isEnd;
	            loadButtonFun();
	            
	        });


	socket.on("nextPage", function (data) {
				console.log(data);
	            var images = data.images;
	            console.log(images);
	            if(window.pixKettle.currentScreen.width == false) {
	            	pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, false);     
	            } else {
	            	pixKettle.tools.screenTools.SetImgsToScreen(data.images, false, true);     
	            }
	            setIdsArr(data.images);
	            pageInfo.page++;
	            pageInfo.isEnd = data.isEnd;
	            loadButtonFun();
	           
	        });


	socket.on("refreshBack", function (data) {
				//cleanScreen();
				console.log(data);
				if(!compareIdsArrs(idsArr, data.ids)) {
					pixKettle.tools.screenTools.cleanScreen();
					socket.emit('getUsersPixs',usId);
				} else {
					console.log('brak');
				}

	            /*var images = data.images;
	            console.log(images);
	            if(currentScreen.width == false) {
	            	SetImgsToScreen(data.images, false, false);     
	            } else {
	            	SetImgsToScreen(data.images, false, true);     
	            }*/
	            
	        });


	/*socket.on("refresh", function (data) {
				cleanScreen();
				console.log(data);
	            var images = data.images;
	            console.log(images);
	            if(currentScreen.width == false) \
	            	SetImgsToScreen(data.images, false, false);     
	            } else {
	            	SetImgsToScreen(data.images, false, true);     
	            }
	            
	        });*/

	
    


	
    var socket2 = io('http://' + location.host + '/updateStream');

    socket2.on('connect', function () {       
	            console.log('Nawiązano połączenie przez socket2.io');
	        });

	socket2.on('connect_failed', function(){
    	//cleanScreen();
		loadNoConnectionScreen();
	});

	socket2.on('connect_error', function() {
		//cleanScreen();
		loadNoConnectionScreen();
			    	
	});

	socket2.on('disconnect', function() {
			    	console.log("no elo");
			    	socket2.disconnect();
			    	//cleanScreen();
			    	loadNoConnectionScreen();
			     });


	socket2.on("action", function (data) {
		console.log(data);
		console.log('refresh');
		refreshFun();
	});


}