if( !window.flashMessagesLib) {
	window.flashMessagesLib = {};
	
	flashMessagesLib = function() {
		var fmMemory = [];
		var fmStart = Date.now();
		var busy = false;
		var fmQueue = [];
		var queuing = function() {
			//console.log(fmQueue);
			if(fmQueue[0]) {
				busy = true;
				fmQueue.shift()(function() {
					queuing();
				});
			} else {
				busy = false;
			}
		};


		var fmConf = {
			hideMssgs : true,
			animateMssgs : true,
			scrollToTop : true,
			showTime : 7000,
			fmContainer :"flashContainer"

		}

		var delAnim = function(fmId ,callback)  {
			//console.log(fmId);
			$('#'+fmId).slideUp( "slow", function() {
				$('#'+fmId).remove();
				callback();
	  		});
		};


		var addAnim = function(fmId, callback) {
			//console.log('hello' + fmId);
			$('#'+fmId).slideDown( "slow", function() {
				$("html, body").animate({ scrollTop: $('#'+fmId).scrollTop()  }, "slow");
				callback();
	  		});
		}


		var genIdFun = function() {
			var  i =0;
			var fmId = 'fm'+Date.now();
			while($.inArray(fmId, fmMemory) != -1) {
				fmId = 'fm'+(Date.now()+i);
				i++;
			}
			if(fmConf.hideMssgs)fmMemory.push(fmId);
			return fmId;
		}

		var delMssgFun = function(fmId) {
			fmMemory = jQuery.grep(fmMemory, function( a ) {
					return a !== fmId;
			});
			if(!busy) {
				busy = true;
				delAnim(fmId, function() {
					queuing();
				})
			} else {
				fmQueue.push(function(callback) {
					delAnim(fmId, callback)
				})
			}
		}

		var addMssgFun = function(message) {
			var myId = genIdFun();
			var node = $("<div></div>",{id : myId, role: 'alert', class : 'messageCloud '+message.status});
			node.text(message.text);
			node.hide();
			node.appendTo('#'+fmConf.fmContainer);
			if(!busy) {
				busy = true;
				addAnim(myId, function() {
					if(fmConf.hideMssgs)setTimeout(function() {delMssgFun(myId)}, fmConf.showTime);
					queuing();
				})
			} else {
				fmQueue.push(function(callback) {
					addAnim(myId, function() {
						if(fmConf.hideMssgs)setTimeout(function() {delMssgFun(myId)}, fmConf.showTime);
						callback();
					})
				})
			}
		};



		var setConf = function(obj) {
			var setShowTime = function(xvar) {
				fmConf.showTime = xvar
			}

			var setFmContainer = function(xvar) {
				fmConf.fmContainer = xvar;
			}

			if(obj['showTime']) {
				setShowTime(obj['showTime']);
			}

			if(obj['fmContainer']) {
				setFmContainer(obj['fmContainer']);
			}
		}


		var showFlashMessages = function(messages) {
			if(messages && messages.length) {
				for(i=0;i<messages.length;i++) {
			  	//console.log(messages[i]);
		
			    addMssgFun(messages[i]);
			  }
			}
		}; 

		var preloadedMessages = function() {
			$("#" +fmConf.fmContainer + " > div").each(function(i){
				if($(this).hasClass('messageCloud')) {
					var myId = genFmId();
					$(this).attr('id', myId);
					if(fmConf.hideMssgs)setTimeout(function() {delMssg(myId)}, fmConf.showTime);
				}
			});
		}


		var quickStart = function(xconf){
			if(xconf) {
				setConf(xconf);
			}
			preloadedMessages();
		}

		return {
			'deleteMessage' : delMssgFun,
			'addMessage' : addMssgFun,
			'setConf' : setConf,
			'showFlashMessages' : showFlashMessages,
			'preloadedMessages' : preloadedMessages,
			'quickStart' : quickStart

		};

	}();
}





