window.pixKettle.tools.navbarTools = {}

window.pixKettle.tools.navbarTools.logOnNavbar = function(user) {

        var userPanel = $("<li></li>", { "class" : "userLiPanel"});
        var userCloudBtt = $("<button></button>", { "id" : "userCloudBtt"});
        var userLink = $("<a></a>", { "id" : "user", "href" : "/u/" + user.login  });
        var xAvatar = user.avatar || "/images/av.png";
        var avImg = $("<img />",{ "class" : "avImg", src : xAvatar}); 
        var spanPart = $("<span>&nbsp;"+user.login+"</span>");
        userPanel.append(userCloudBtt);
        userPanel.append(userLink);
        userLink.append(avImg);
        userLink.append(spanPart);

        var userCloud = $("<ul></ul>",{ "id" : "userCloud"});
        var liOptions = $("<li></li>").append($("<a></a>", {"href" : "me/option"}).text("opcje"));
        var liLogout  = $("<li></li>").append($("<a></a>", {"id" : "unlogNav", "href" :"/logout"}).text("wyloguj"));

        userCloud.append(liOptions).append(liLogout);

    $("#rightSidePanel").empty().append(userPanel).after(userCloud);
    animateUserCloud();
  };