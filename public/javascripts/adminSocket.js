var getN = function(xnumber) {
    if(xnumber.toString().length < 2) {
      return '0'+xnumber
    } else {
      return xnumber;
    }
} 


var tickSecond = function(begin) {
  var hPart = parseInt(begin/(1000*60*60)); 
  var mPart = parseInt(begin/(1000*60))%60;
  var sPart = parseInt(begin/1000)%60;
  
  return function(callback) {
    if(sPart > 0) {
      sPart --;
    } else {
      sPart = 59;
      if(mPart > 0) {
        mPart--;
      } else {
        mPart = 59;
        if(hPart >0) {
          hPart --;
        } else {
          hPart = 23;
        }
      }
    }
    callback(getN(hPart)+':'+getN(mPart)+':'+getN(sPart));
  }
}


var timerObjFun = function(current , end, callback){
	var inProgres =true
  var obj = {};
  obj.state = end - current;
  var tickSecondFun = tickSecond(obj.state);
  obj.interval = setInterval(function(){
    tickSecondFun(function(dateString){
      callback(dateString);
    })
  }, 1000);
  obj.timeout = setTimeout(function() {clearInterval(obj.interval);inProgres=false;}, obj.state+1000);
  obj.cancelFun = function() {
  	if(inProgres) {
  		clearInterval(obj.interval);
    	clearTimeout(obj.timeout);
  	}
  }
  return obj;
};

var timerObj = {} ;


var dateStringGen = function(dateNum) {
	var bufDate = new Date(dateNum);
    var day = getN(bufDate.getDate());
    var month= getN(bufDate.getMonth());
    var year = getN(bufDate.getFullYear());
    var hour = getN(bufDate.getHours());
    var minute = getN(bufDate.getMinutes());
    var second = getN(bufDate.getSeconds());
	return day+'.'+month+'.'+year+'&nbsp;&nbsp;&nbsp;'+hour+':'+minute+":"+second;
}



var setTestInfoToTable = function(data) {
	var row = $('<tr></tr>').attr('id',data.tId);
  	$('<td>'+data.tId+'</td>').appendTo(row);
  	$('<td>'+data.name+'</td>').appendTo(row);
  	$('<td>'+dateStringGen(data.testEnd - data.testPeriod)+'</td>').appendTo(row);
  	$('<td>'+dateStringGen(data.testEnd)+'</td>').appendTo(row);
  	$('<td>'+'.'+'</td>').addClass('tbTimer').appendTo(row);

  	$('<td></td>').append($('<button type="button">').text('zakończ').addClass('btn btn-default cancelBtt')).appendTo(row);
  	row.appendTo($('#showcaseStartedTestTable'));
  	$('#'+data.tId+' .cancelBtt').click(function() {
  		console.log('elo');
    	$.ajax({
		
          method: "GET",
          url: "/admin/showcaseStop",
          error: function(){
          	//obj ={status : 'err', text:'Wystąpił nieznany błąd', field : 'global', form: type};
            //setFormMssg(obj)
            console.log('err byl')
          },
          success: function(data){
          	console.log(data);
              if(data.status == 'success') {
              	console.log('ok');
                
               	
              } else {
              	//setFormMssg({status: 'err', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global', form : type});
              	//for(i=0;i< data.formMessages.length;i++) {
              	//	console.log('aa');
              	//	setFormMssg(data.formMessages[i]);
              	//}
              	console.log('nieudalo sie');
              }
          },
          timeout: 3000 // sets timeout to 3 seconds
      });	
	});
}


$(document).ready(function(){
$('#showcaseStartedTestTable').hide();
if(!socket) var socket;
if (!socket || !socket.connected) {
		socket = io('http://' + location.host + '/showcaseTest',{ 'connect timeout': 3000});
	}
	socket.on('connect', function () {       
	            console.log('Nawiązano połączenie przez Socket.io');
	            socket.emit('checkTest');
	});

	socket.on("testId", function (data) {
		if(data.started == true && data.tId) {
			console.log('elo');	
			console.log(data);
			

			socket.emit('getTestInfo', data.tId);
		} 
	});

	socket.on("testInfo", function (data) {
		$('#showcaseStartBtt').prop('disabled', true);
		console.log(data);
		setTestInfoToTable(data);
		timerObj = timerObjFun(data.current, data.testEnd, function(dateString) {
			$('#'+data.tId+' .tbTimer').text(dateString);
		});
		$('#showcaseStartedTestTable').slideDown('slow');

	});


	socket.on("testEnd", function (data) {
		console.log(data);
		$('#showcaseStartedTestTable').slideUp('slow');
		timerObj.cancelFun();
		$('#'+data).remove();
		$('#showcaseStartBtt').prop('disabled', false);
	});


});