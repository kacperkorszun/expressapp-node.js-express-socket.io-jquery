var titleState = true;

var getImgCanvas = function(width, height, fillColor) {
  var xcanvas = $('.uploadScreen > img').cropper('getCroppedCanvas',{
    width: width,
    height: height,
    fillColor : fillColor
    });
    return xcanvas;
   
}

var checkTitle = function(title) {
  var newTitle = title.replace(/(\n|\r|\r\n)/g,' ').trim();
  if(newTitle.length <  3) {
    var obj ={status : 'error', text:'Podany tytuł jest za krótki'};
    showFlashMessages([obj]);
    titleState = false;
  }  else if( newTitle.length  > 40) {
    var obj ={status : 'error', text:'Podany tytuł jest za długi'};
    showFlashMessages([obj]);
    titleState = false;
  } else {
    titleState =true;
  }
}


var getScreenWidth = function() {
  var chosenOne = 700
  var xxwidth = window.innerWidth;
  if(xxwidth >= chosenOne + 40) {
    return chosenOne
  } else {
    return xxwidth - 40;
  }
}

var setPreviewScreen = function(xcanvas) {
   $('.previewScreen').empty().append(xcanvas);
} 

var loadImg = function(e,callback) {
  var filex = e.target.files[0];
  var reader = new FileReader();
  reader.onload = function(e) {
    $('.uploadScreen').empty().append($('<img alt="Picture">').attr('src', reader.result));
    callback();
  }
  
  reader.readAsDataURL(filex);
}


$(document).ready(function(){
  var defColor = '#FFFFFF';
  $("#html5colorpicker").val(defColor);
	
$('.uploadForm').submit(false);

  $('.uploadIcon img').click(function(){
    $('.uploadInput').click();
  });

  $('#uploadButton').click(function () {
    $('.uploadInput').click();
  });


  $('.uploadInput').change(function(e) {

    loadImg(e, function() {
       $('.uploadScreen > img').cropper({
        aspectRatio: 1 / 1,
        autoCropArea: 0.65,
        strict: false,
        guides: false,
        highlight: false,
        dragCrop: false,
        cropBoxMovable: false,
        cropBoxResizable: false
      });

      $('#setButton').prop('disabled', false);
    });
   
  })

  $("#updateSubmitBtt").click(function() {

    checkTitle($('#titleInput').val());
    if(titleState) {
      var dataURL = getImgCanvas(700, 700, defColor).toDataURL("image/png");
      var datax = dataURL;
      var title = $("#titleInput").val();

      console.log(datax);
      
      //alert(dataURL);

        $.ajax('/upload', {
          method: "POST",
          data: {image : datax, title : title},
          error: function(){
            obj ={status : 'error', text:'Wystąpił nieznany błąd'};
            showFlashMessages([obj]);
          },
          success: function(data){
            console.log(data);
            if(data.status == 'success') {
              window.location.replace('http://' + location.host+'/');
            } else {
                if(data.redirect) {
                  window.location.replace('http://' + location.host+'/upload');
                } else {
                  showFlashMessages([{status : 'error', text : data.message }])
                }
            }
          },
          timeout: 7000
        });
    }
  });

  $('#setButton').click(function(){
    $('.uploadPanel').slideUp();
    $('#uploadFormDiv').slideDown();
    setPreviewScreen(getImgCanvas(700, 700, defColor));
  });

  $("#html5colorpicker").change(function(){
    defColor = $(this).val();
    setPreviewScreen(getImgCanvas(700, 700, defColor));
  })


  $('#uploadBackBtt').click(function(){
    

    $('.uploadPanel').slideDown();
    $('#uploadFormDiv').slideUp();
  })
});