checkDbObj = { passwordConfirm : true, uri : true};

var showcaseChangeForm = false;

var setState ;
var blocked = false;


var setSetState = function() {
  setState = $('#showcaseDemoTestForm').serialize();
  showcaseChangeForm = true;
  changeWatcher();
}


var showcaseSetSwitch = function() {
  showcaseChangeForm = !showcaseChangeForm;
  $('#showcaseSetBtt').prop('disabled', !showcaseChangeForm);
  if(showcaseChangeForm) {
    $('#showcaseStartBtt').text('Ustaw i start');
  } else {
    $('#showcaseStartBtt').text('Start');
  }
}


var changeWatcher = function() {
  if(setState == $('#showcaseDemoTestForm').serialize() && showcaseChangeForm) {
    showcaseSetSwitch();
  } else if(setState != $('#showcaseDemoTestForm').serialize() && !showcaseChangeForm){
    showcaseSetSwitch();
  }
}

var toMinutes = function(val) {
  var retVal = parseInt(val/60000);
  if(!isNaN(retVal)) {
    return retVal
  } else {
    return 0;
  }
}



selctChange = function (subname) {
    var node = $('#showcasePix'+subname)
    var subname2;
    if(subname == 'Adding') {
      subname2 = 'Pixs';
    } else {
      subname2 = 'Plus';
    }
    if(node.find("option:selected" ).first().attr("class") == 'falseSelect') {
      $('#showcasePix'+subname+'Style').prop('disabled', true);
      $('#showcasePix'+subname+'Speed').prop('disabled', true);
      $('#showcasePix'+subname+'NumberOf'+subname2).prop('disabled',true); 
      $(".pix"+subname+"IsNumber").prop('disabled', true);   
      changeWatcher();
    } else {
      $('#showcasePix'+subname+'Style').prop('disabled', false);
      $('#showcasePix'+subname+'Speed').prop('disabled', false);
      $('#showcasePix'+subname+'NumberOf'+subname2).prop('disabled',false); 
      $(".pix"+subname+"IsNumber").prop('disabled', false);
      isNumberFun(subname, $(".pix"+subname+"IsNumber:checked"));
    }
}


valueSelect = function(node, val) {
  node.find('option').removeAttr( "selected") ;
  node.find("."+val).attr('selected', true);

}

var setSelect = function(node, val) {
  //console.log('xxx');
  var positiveOption = node.find(".trueSelect");
  //console.log(positiveOption.val());
  var negativeOption = node.find(".falseSelect");
  if(val == true) {
    positiveOption.attr('selected', true);
    negativeOption.removeAttr( "selected") ;
  } else {
    negativeOption.attr('selected', true);
    positiveOption.removeAttr( "selected") ;
  }
}

var setToShowcaseTestConfForm = function(obj) {

  $("#showcaseDbUser").val(obj.db.user);
  $("#showcaseDbPassword").val(obj.db.password);
  $("#showcaseDbPasswordConfirm").val(obj.db.password);
  $("#showcaseDbUri").val(obj.db.uri);

  $("#showcaseTestPeriod").val(toMinutes(obj.testPeriod));
  $("#showcaseExpiration").val(toMinutes(obj.expiration));
  $("#showcaseNumberOfUsers").val(obj.numberOfUsers);
  setSelect($("#showcasePixAdding"), obj.pixAdding);
  setSelect($("#showcasePixPlus"), obj.pixPlus);
  setSelect($("#showcaseClearCollections"), obj.clearCollections);
  if(obj.pixAdding) {
    $("#showcasePixAddingStyle").prop('disabled', false);
    $("#showcasePixAddingSpeed").prop('disabled', false);
    $("#showcasePixAddingNumberOfPixs").prop('disabled', false);
    valueSelect($("#showcasePixAddingStyle"), obj.pixAddingConf.style);
    $(".pixAddingIsNumber").prop('disabled', false);
    if(obj.pixAddingConf) {
      if(obj.pixAddingConf.isNumber) {
        $("#showcasePixAddingNumberOfPixs").val(obj.pixAddingConf.numberOfPixs)
      } else  {
        valueSelect($("#showcasePixAddingSpeed"), obj.pixAddingConf.speed);
      }
      $('#showcasePixAddingIsNumberTrue').prop('checked', obj.pixAddingConf.isNumber);
      $('#showcasePixAddingIsNumberFalse').prop('checked', !obj.pixAddingConf.isNumber);
      isNumberFun('Adding', $(".pixAddingIsNumber:checked"));
    }

  } else {
    $("#showcasePixAddingStyle").prop('disabled', true);
    $("#showcasePixAddingSpeed").prop('disabled', true);
    $("#showcasePixAddingNumberOfPixs").prop('disabled', true);
    $(".pixAddingIsNumber").prop('disabled', true);
  }



  if(obj.pixPlus) {
    $("#showcasePixPlusStyle").prop('disabled', false);
    $("#showcasePixPlusSpeed").prop('disabled', false);
    $("#showcasePixPlusNumberOfPlus").prop('disabled', false);
    $(".pixPlusIsNumber").prop('disabled', false);
    valueSelect($("#showcasePixPlusStyle"), obj.pixPlusConf.style);
    if(obj.pixPlusConf) {
      if(obj.pixPlusConf.isNumber) {
        $("#showcasePixPlusNumberOfPlus").val(obj.pixPlusConf.numberOfPlus)
      } else  {
        valueSelect($("#showcasePixPlusSpeed"), obj.pixPlusConf.speed);
      }
      $('#showcasePixPlusIsNumberTrue').prop('checked', obj.pixPlusConf.isNumber);
      $('#showcasePixPlusIsNumberFalse').prop('checked', !obj.pixPlusConf.isNumber);
      isNumberFun('Plus', $(".pixPlusIsNumber:checked"));
    }
  } else {
    $("#showcasePixPlusStyle").prop('disabled', true);
    $("#showcasePixPlusSpeed").prop('disabled', true);
    $("#showcasePixPlusNumberOfPlus").prop('disabled', true);
    $(".pixPlusIsNumber").prop('disabled', true);
  }
  setSetState();
};


var showcaseDemoTestConfRefresh = function() {
	$.ajax({
		
          method: "GET",
          url: "/admin/showcaseConf",
          error: function(){
          	//obj ={status : 'err', text:'Wystąpił nieznany błąd', field : 'global', form: type};
            //setFormMssg(obj)
            console.log('err byl')
          },
          success: function(data){
          	console.log(data);
              if(data.status == 'success') {
              	console.log('ok');
               	setToShowcaseTestConfForm(data.obj);
              } else {
              	//setFormMssg({status: 'err', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global', form : type});
              	//for(i=0;i< data.formMessages.length;i++) {
              	//	console.log('aa');
              	//	setFormMssg(data.formMessages[i]);
              	//}
              	console.log('nieudalo sie');
              }
          },
          timeout: 3000 // sets timeout to 3 seconds
      });
}


var showcaseDemoTestStart = function() {
	$.ajax({
		
          method: "GET",
          url: "/admin/showcase",
          error: function(){
          	//obj ={status : 'err', text:'Wystąpił nieznany błąd', field : 'global', form: type};
            //setFormMssg(obj)
            console.log('err byl')
          },
          success: function(data){
          	console.log(data);
              if(data.status == 'success') {
              	console.log('ok');
                
               	
              } else {
              	//setFormMssg({status: 'err', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global', form : type});
              	//for(i=0;i< data.formMessages.length;i++) {
              	//	console.log('aa');
              	//	setFormMssg(data.formMessages[i]);
              	//}
              	console.log('nieudalo sie');
              }
          },
          timeout: 3000 // sets timeout to 3 seconds
      });
}

var sendOptionForm = function(type) {
	var data =  $("#optionForm"+pixKettle.tools.firstLetterUp(type)).serialize();
	$.ajax({
		
          method: "POST",
          url: "/admin/"+type,
          data: data,
          error: function(){
          	obj ={status : 'err', text:'Wystąpił nieznany błąd', field : 'global', form: type};
            setFormMssg(obj)
          },
          success: function(data){
          	console.log(data);
              if(data.status == 'success') {
              	console.log('ok');
               	
              } else {
              	setFormMssg({status: 'err', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global', form : type});
              	for(i=0;i< data.formMessages.length;i++) {
              		console.log('aa');
              		setFormMssg(data.formMessages[i]);
              	}
              }
          },
          timeout: 3000 // sets timeout to 3 seconds
      });
	
}


var sendShowcaseDemoTestConf = function(cb) {
	var data =  $('#showcaseDemoTestForm').serialize();
	$.ajax({
          method: "POST",
          url: "/admin/showcase",
          data: data,
          error: function(){
          	obj ={status : 'err', text:'Wystąpił nieznany błąd', field : 'global', form: 'img'};
            setFormMssg(obj)
          },
          success: function(data){
          	console.log(data);
              if(data.status == 'success') {
              	console.log('ok');
                setSetState();
               	if(cb)cb();
              } else {
              	setFormMssg({status: 'err', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global', form : 'img'});
              	for(i=0;i< data.formMessages.length;i++) {
              		console.log('aa');
              		//setFormMssg(data.formMessages[i]);
              	}
              }
          },
          timeout: 3000 // sets timeout to 3 seconds
      });
}



var isNumberFun = function(subName, xthis){
    var subName2;
          if(subName == 'Adding') {
            subName2 = 'Pixs';
          } else {
            subName2 = 'Plus';
          }
          
          if(xthis.val() == 'true') {

            $('#showcasePix'+subName+'NumberOf'+subName2).prop('disabled',false); 
            $("#showcasePix"+subName+"Speed").prop('disabled', true);   
          } else {
           
            $('#showcasePix'+subName+'NumberOf'+subName2).prop('disabled',true); 
            $("#showcasePix"+subName+"Speed").prop('disabled', false); 
          }
          changeWatcher();
      }


var setFormMssg = function(objekt) {
	var subName =  pixKettle.tools.firstLetterUp(objekt.field);
	var subName2 = pixKettle.tools.firstLetterUp(objekt.form);
	var mssgString = '';
	if(objekt.status == 'success') {
		window['check'+subName2+'Obj'][objekt.field] = true;
		mssgString +='☑';
	} else {
		window['check'+subName2+'Obj'][objekt.field] = false;
		mssgString +='☒';
	}
	mssgString += objekt.text;
	$('#form'+subName2+'Message'+ subName).text(mssgString);
}



var checkDbPasswordConfirm = function(pass) {
	if(pass != $('#dbPassword').val()) {
		var obj ={status : 'err', text:'', field : 'passwordConfirm', form: 'db'};
		setFormMssg(obj);
	} else {
		setFormMssg({status:'success',text:'',field:'passwordConfirm', form : 'db'});
	}
}

var checkDbURI = function(uri) {
	var patt = /^mongodb:\/\/[a-z0-9\=\/\@\.\:\?\<\>]+$/i;
	if(patt.test(uri)) {
		if($("#dbUser").val().length > 0  && uri.indexOf("<dbuser>:<dbpassword>@") == -1) {
			setFormMssg({status:'err',text:'niewłaściwy format',field:'uri', form: 'db'});
		} else {
			setFormMssg({status:'success',text:'',field:'uri', form: 'db'});
		}
	}
}


$(document).ready(function(){
	$('#optionFormDb').submit(false);
	$('#optionFormImg').submit(false); 
	$('#showcaseDemoTestForm').submit(false);
  $('#showcaseSetBtt').prop('disabled',true);
	$('.imgRadio').change(
    	function(){
        	$("#imgClient-ID").prop('disabled',function( i, val ) { return !val;});  
    	}
	);

  setSetState();
  $('#showcaseDemoTestForm input').change(function() {
    console.log('zmiana');
    changeWatcher();
  })

  $('#showcasePixAddingStyle').change(function() {
    if($(this).val() === 'sameTime') {
      $('.pixAddingIsNumber').prop('disabled',true);

    } else {
      $('.pixAddingIsNumber').prop('disabled',false);
      $('#showcasePixAddingIsNumberTrue').prop('checked', true);
    }
  })

  $('#showcaseDemoTestForm select').change(function() {
    console.log('zmiana');
    changeWatcher();
  })

  $('.pixAddingIsNumber').change(
      function() {isNumberFun('Adding', $(this));}
  );

   $('.pixPlusIsNumber').change(
      function() {isNumberFun('Plus', $(this));}
  );

  $( "#showcasePixAdding" ).change(function(){selctChange('Adding')});
  $( "#showcasePixPlus" ).change(function(){selctChange('Plus')});
	$('#dbPasswordConfirm').focusout(function() {
		checkDbPasswordConfirm($(this).val());
	});
	$('#dbUri').focusout(function() {
		checkDbURI($(this).val());
	});

	$('#optionFormDbSubmit').click(function() {
		checkDbPasswordConfirm($("#dbPasswordConfirm").val());
		checkDbURI($("#dbUri").val());
		if(checkDbObj.passwordConfirm && checkDbObj.uri) {
			sendOptionForm('db');
		} else {
			setFormMssg({status: 'err', text:'Nie można było utworzyć konta. Popraw błędy w formularzu', field : 'global' , form: 'db'});
		}
	})

	$('#optionFormImgSubmit').click(function() {
		sendOptionForm('img');
	})


	$('#showcaseSetBtt').click(function() {
		sendShowcaseDemoTestConf();
	})

	$('#showcaseStartBtt').click(function() {
    if($(this).text() == 'Ustaw i start') {
      sendShowcaseDemoTestConf(function() {
        showcaseDemoTestStart();
      });
    } else {
      showcaseDemoTestStart();
    }
		
	});

	$('#showcaseRefreshBtt').click(function() {
		showcaseDemoTestConfRefresh();
	})

});