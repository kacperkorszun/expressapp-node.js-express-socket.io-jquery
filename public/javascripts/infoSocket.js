var infoSocket

window.pixKettle.infoSocketModule = {};

window.pixKettle.infoSocketModule.plusImgObj = {
	ignoreSuccess : true,
	responseTimeout : 10000,
	waiting : [],

}



$(document).ready(function(){
 	



	if (!infoSocket || !infoSocket.connected) {
			infoSocket = io('http://' + location.host + '/info',{ 'connect timeout': 3000});
		}
		infoSocket.on('connect', function () {       
		            console.log('Nawiązano połączenie przez Socket.io(/info)');
	            	if(myAppObj.uploaded) {
				  		infoSocket.emit('uploaded');
				  		
				  	}
		            

		});

		infoSocket.on('connect_failed', function(){
			alert('info socket connection failed');
		});

		infoSocket.on('connect_error', function() {
			alert('info socket connection error');
					    	
		});

		infoSocket.on('disconnect', function() {
			alert('info socket disconnect');
		});


		infoSocket.on('message', function(data) {
			
			showFlashMessages([data]);
				    	
		});


		
		if(myAppObj.page == 'singleUser') {
	  		window.pixKettle.initFun();
	  	}



	  	    infoSocket.on('screenSizes' , function(data) {
	  	    	
		    	screenSizes = data;
		    	pixKettle.tools.checkSize(function(screenSize) {
		    		bufor = window.pixKettle.currentScreen;
		    		window.pixKettle.currentScreen = screenSize;
		    		if($.isEmptyObject(bufor)) {
		    			//changePoczekalniaScreen(false, window.pixKettle.home.currentScreen);
		    			window.pixKettle.tools.screenTools.changePixScreen(screenSize, myAppObj.page, false, socket);
						$( window ).resize(window.pixKettle.tools.screenTools.screenWatcher(function(xscreen){
							if(myAppObj.page == 'home') {
								window.pixKettle.home.socketTools.resizeFun(socket)(xscreen);
							} else {
								var undo = window.pixKettle.currentScreen;
								window.pixKettle.currentScreen = xscreen;
								changePoczekalniaScreen(undo, pixKettle.currentScreen);
								pixKettle.tools.screenTools.changePixScreen(xscreen, 'anteroom', undo, socket);
								
							}
														
						}));
					}
		    		if(screenSize) {
		    			pixKettle.initFun();
		    		} else {
		    			//jakis error
		    		}
		    		
		    	});
		    })

	
		//socket.on("updateScreen", function (data) {
			
		if(myAppObj.page == 'singleUser' || myAppObj.page == 'home' || myAppObj.page == 'anteroom' ) {
			infoSocket.emit('initial');
		}


})