var express = require('express');
var router = express.Router();
var pixController = require(appRoot+'/controllers/pixController');


router.get('/:id', function(req, res, next){
  
  pixController.getPix(req.params.id, function(status, pix) {
    
    if(pix) {
        req.toSend.title = "'"+pix.title+"'";
        req.toSend.page = 'singlePicture';
        req.toSend.pic = pix;
        res.render('templ',req.toSend);
    } else {
      next();
    }
  });
  
});

module.exports = router;