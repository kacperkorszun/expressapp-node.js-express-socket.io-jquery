var express = require('express');
var router = express.Router();

//tu adres
//var userDbService = require('./../dbServices/userDbService');
var userController = require(appRoot+'/controllers/userController');

router.get('/checkLogin/:login',function(req, res){
  var obj = {};
  userController.checkLoginAvailability(req.params.login, function(err, status, message) {
    if(err) {
      obj = {status : 'err', result : false,  text: err.message};
    } else if(!status) {
      obj= {status : 'ok', result : false, text : message};
    } else {

      obj = {status: 'ok',  result : true, text :"Login is available"};
    }
    res.json(obj);
  });
});



router.get('/checkPassword/:password',function(req, res){
  var obj = {};
  
  userController.validatePassword(req.params.password, function(status, message) {
    if(!status) {
      obj= {status : 'error',  text : message};
    } else {
      obj = {status: 'success', text :""};
    }
    res.json(obj);
  });
});


module.exports = router;