var express = require('express');
path = require('path');
var app = express.Router();
var passport = require('passport');




  app.get('/', function(req, res){
    if(req.user) {
      res.redirect('/');
    } else {
       if(req.session.uploadFailur == true) {
        
        var mess = {status : 'info',text : 'Musisz być zalogowany aby dodawać obrazki'};
        req.toSend.fMessages = [mess];
        req.session.uploadFailur = null;
      }
      req.toSend.title =  "Logowanie";
      req.toSend.page = 'login';
      res.render('templ', req.toSend);
    }
  });

  app.get('/form', function(req,res){
    res.sendFile(path.resolve(__dirname+'/../views/loginCloudForm.html'));
  })

  app.post('/', function(req, res, next) {
    if(req.user) {
      res.json({status :'error', messages : "Jesteś zalogowany "});
    } else {
      passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.json({status :'error', messages : [{ status : 'error',  text : info.message }]}); }
        req.login(user, function(err) {
          if (err) return next(err);
          if(user.login == 'admin') {

            req.session.superuser = true;
            req.session.save(function(err) {
              if(err) console.log(err);
            })
          }
          req.session.loginSuccess = true;
          req.session.userId = user._id;
          req.session.save(function(err) {
            if(err) console.log(err);
          })

          return res.json({status : 'ok'});
        });
        
      })(req, res, next);
    }
    
  });


module.exports = app;




