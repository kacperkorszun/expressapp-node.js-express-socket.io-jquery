var express = require('express');
var app = express.Router();
var passport = require('passport');


  app.get('/', function(req, res){ 
  if(req.user) {
    res.redirect('/');
  } else {
    req.toSend.title = "Rejestracja";
    req.toSend.page = 'register';
    res.render('templ', req.toSend);
  }
});

app.post('/', function(req, res, next) {
  if(req.user) {
    res.json({status :'error', messages : "Jesteś zalogowany "});
  } else {
    passport.authenticate('rejestracja', function(err, user, info) {
      if (err) { 
        var data = {};
        data.status = 'error';
        data.formMessages = [{status : 'error', text : 'Nieudało się dodać użytkownika' , field : 'global'}];
        return res.json(data); 
      } 
      else if (!user) {

        var data = {};
        data.status = 'error';
        var formMessage = {};
        formMessage.status = 'error';
        formMessage.text = info.message;
        if(info.target) {
          formMessage.field = info.target;
        } else {
          formMessage.field = 'global';
        } 
        data.formMessages = [formMessage];
        return res.json(data); 
      } else {
       
        req.session.registerSuccess = true;
        req.session.save(function(err) {
          if(err) console.log(err);
        })
        return res.json({status : 'success'});
      }
    })(req, res, next);
  }  
});



module.exports = app;