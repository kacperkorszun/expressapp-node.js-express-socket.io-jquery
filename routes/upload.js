var express = require('express');
var app = express.Router();
var fs = require('fs');
var pixController = require(appRoot+'/controllers/pixController');

var uploadMulter = (require('multer'))(require("./../uploadMulter.js"));

app.get('/', function(req, res){
  if(req.user) {
    req.toSend.title = "Dodaj obrazek";
    req.toSend.page = 'upload';
    res.render('templ', req.toSend);
  } else  {
    req.session.uploadFailur = true;
    res.redirect('../login');
  }
  
});


app.post('/', function(req, res) {
  var path = require('path');
  var appDir = path.dirname(require.main.filename);




    if(req.user) {
      if(!req.user || !req.body.image || pixController.checkPixFileType(req.body.image)) {
        res.json({status : 'error', message : 'Wysłane dane nie są poprawne'});
      } else {

        pixController.uploadNewPix(req.body.image,req.body.title, req.user.id, function(err) {
          if(err) {
            res.json({status : 'error', message : 'Zapisanie obrazu nie było możliwe'});
          } else {
            req.session.uploadSuccess = true;
            req.session.save(function(err) {
              if(err) console.log(err);
            })
            res.json({status : 'success'});
          }
        });
      }
    } else {
      res.json({status : 'error', redirect: true});
    }
    
});




module.exports = app;