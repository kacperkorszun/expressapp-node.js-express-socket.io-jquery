var express = require('express');
var app = express.Router();
var fs = require('fs');

var showcaseTestConfModule = require('./../testModules/showcaseTestConfModule.js');
var showcaseTestModule = require('./../testModules/showcaseTestModule.js');
var showcaseTestConf = showcaseTestConfModule.getConf();




var checkDbFormSec = function(reqBody) {
  if(reqBody.dbUri) {
    return true;
  } else {
    return false;
  }
}

var checkOtherFormSec = function(reqBody) {
  if(reqBody.testPeriod && reqBody.expiration && reqBody.pixAdding &&  
        reqBody.pixPlus && reqBody.clearCollections &&  reqBody.numberOfUsers) {
    return true;
  } else  {
    return false;
  }

}

app.get('/', function(req, res){
  if(req.session.superuser) {
    var myConfig = JSON.parse(fs.readFileSync('./conf/myConf.json', 'UTF-8'));

    res.render('admin', {title : "Admin", db : myConfig.dbConfig, img: myConfig.imgServer, showcaseTestConf : showcaseTestConfModule.getConf()});
  } else {
    res.redirect('/');
  }
  
});


app.get('/showcase', function(req, res){
  if(req.session.superuser) {
    showcaseTestModule.startTest(function(testId) {
      res.json({status : 'success', testId : testId});
    });
  } else {
    res.json({status : 'err'});
  }
  
});


app.get('/showcaseStop', function(req, res){
  if(req.session.superuser) {
    showcaseTestModule.cancelTest(function() {
      res.json({status : 'success'});
    });
  } else {
    res.json({status : 'err'});
  }
  
});

app.get('/showcaseConf', function(req, res){

  if(req.session.superuser) {
    res.json({status : 'success', obj : showcaseTestConfModule.getRefresh() });
  } else {
    res.json({status : 'err'});
  }
  
});

app.post('/:id', function(req, res){

  if(req.session.superuser) {
    if(req.params.id == 'db') {
      if(req.body && req.body.dbUser && req.body.dbPassword && req.body.dbPasswordConfirm) {
        if(req.body.dbPassword === req.body.dbPasswordConfirm) {
          var myConfig = JSON.parse(fs.readFileSync('./conf/myConf.json', 'UTF-8'));
          myConfig.dbConfig.user = req.body.dbUser;
          myConfig.dbConfig.password = req.body.dbPassword;
          myConfig.dbConfig.uri = req.body.dbUri;
          fs.writeFileSync('./conf/myConf.json', JSON.stringify(myConfig),'UTF-8');
          res.json({status : 'success'});
        } else {
          res.json({status : 'err'})
        }        
      } else {
        res.json({status : 'err'});
      }
    } else if(req.params.id == 'img') {
      if(req.body && req.body.imgRadio) {
        if(req.body.imgRadio === 'local') {
          var myConfig = JSON.parse(fs.readFileSync('./conf/myConf.json', 'UTF-8'));
          myConfig.imgServer.local = true;
          fs.writeFileSync('./conf/myConf.json', JSON.stringify(myConfig),'UTF-8');
          res.json({status : 'success'});
        } else if (req.body.imgRadio === 'imgur' && req.body.imgClientID) {
          var myConfig = JSON.parse(fs.readFileSync('./conf/myConf.json', 'UTF-8'));
          myConfig.imgServer.local = false;
          myConfig.imgServer["client-ID"] = String(req.body.imgClientID);
          fs.writeFileSync('./conf/myConf.json', JSON.stringify(myConfig),'UTF-8');
          res.json({status : 'success'});
        } else {
          res.json({status : 'err'})
        }        
      } else {
        res.json({status : 'err'});
      }
    } else if(req.params.id == 'showcase') {
      console.log(checkDbFormSec(req.body));
      if(req.body && checkDbFormSec(req.body) && checkOtherFormSec(req.body) ) {
          var objx = req.body; 
          var formStatus = showcaseTestConfModule.setAll(objx);
          
          if(formStatus.type == 'success') {
            res.json({status : 'success'});
          } else {
             res.json({status : 'err'});
          }
      } else {
        res.json({status : 'err'});
      }
    } else {
      res.json({staus : 'err'});
    }
  } else {
    res.json({staus : 'err'});
  }
})


module.exports = app;
