var express = require('express');
var app = express.Router();

app.get('*',function(req,res,next){
  req.toSend = {}
  req.toSend.user = null;
  req.toSend.clientObj ={};

  if(!req.user && req.session.superuser) {
    loginAdmin(req,res, next);
  } else {
    
    

    if(req.user && req.user.login) {
      req.toSend.clientObj.logged = true;
      req.toSend.clientObj.user = {login : req.user.login};
      if(req.user.avatar)req.toSend.clientObj.user.avatar = req.user.avatar; 
    } else {
      req.toSend.clientObj.logged = false;
    }


    if(req.session.loginSuccess == true && req.user) {
      var mess = {status : 'success',text : 'Zalogowałeś sie!'};
      req.toSend.fMessages = [mess];
      req.session.loginSuccess = null;
    }
    if(req.session.registerSuccess == true ) {
      var mess = {status : 'success',text : 'Utworzyłeś konto'};
      req.toSend.fMessages= [mess];
      req.session.registerSuccess= null;
    }
    if(req.session.uploadSuccess == true ) {
      var mess = {status : 'success',text : 'Obrazek poprawnie wysłany na serwer'};
      req.toSend.fMessages= [mess];
      req.toSend.uploaded = true;
      req.session.uploadSuccess= null;
    }
    next();
  }
})

module.exports = app;