var express = require('express');
var router = express.Router();
var userController = require(appRoot+'/controllers/userController');

router.get('/:name', function(req, res,next){
  userController.getUserByName (req.params.name, function(err, user) {
    if(user) {
        req.toSend.title = user.login;
        req.toSend.page = 'singleUser';
        req.toSend.userPage = user;
        res.render('templ',req.toSend);
    } else {
      next();
    }
  });
});

module.exports = router;
