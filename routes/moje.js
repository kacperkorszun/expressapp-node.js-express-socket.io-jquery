var express = require('express');
var app = express.Router();

/* GET home page. */
app.get('/', function(req, res) {
	 if(!req.user) {
	 	res.redirect('/');
	 } else {
	 	res.redirect('/u/'+req.user.login);
	 }
})



app.get('/option', function(req, res) {
	if(!req.user) {
	 	res.redirect('/');
	 } else {
	 	req.toSend.title = 'Opcje | '+req.user.login;
  		req.toSend.page = 'userOption';
  		res.render('templ',req.toSend);
	}
})


module.exports = app;
