var express = require('express');
var router = express.Router();

router.get('/', function(req,res){
  if(req.user) {
    if(req.session.superuser) {
      req.session.superuser = false;
    }
    req.logout();
    req.session.userId = null;
    req.session.save(function(err) {
      if(err) console.log(err);
    })
  } 
  res.redirect('/');
});

module.exports = router;