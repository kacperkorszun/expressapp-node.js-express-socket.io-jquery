var shortid = require('shortid');

Array.prototype.delteById= function(id,callback) {
  var j =0;
  while(j < this.length  && this[j].id != id)j++;
  if(j<this.length) {
    if(callback)callback(this[j]);
    this.splice(j, 1);
  }
}

var cleanUsed = function(arr) {
	for (var i = 0; i < arr.length; i++) {
		messagesArr.delteById(arr[i]);
	};
}

var checkUserEvents = function(userId) {
	var toDelete = [];
	for (var i = 0; i < messagesArr.length; i++) {
    	if(messagesArr[i].toUserId === userId) {
      		socket.emit('message', messagesArr[i].message);
      		toDelete.push(messagesArr[i].id);
    	}
  	}
  	cleanUsed(toDelete);
}

var checkEventUsers = function(userId, message) {
	var done = false;
	for (var i = 0; i < usersArr.length; i++) {
    	if(usersArr[i].userId === userId) {
    		done = true;
      		usersArr[i].socket.emit('message', message);
    	}
	}

	return done;
}

var usersArr = [];
var messagesArr = [];

infoModule ={};

infoModule.setUserSocket = function(usId, socket) {
	usersArr.push({userId : usId, socket : socket});
	checkUserEvents(usId);
};

infoModule.addMessage = function(usId, message) {
	if(!checkEventUsers(usId,message)) {
		messagesArr.push({id :  shortid.generate(), toUserId : usId, message : message});
	}
}

module.exports = infoModule
