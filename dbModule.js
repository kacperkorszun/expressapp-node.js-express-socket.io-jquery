var mongoose = require('mongoose');
var fs = require('fs');

var dbUndo

// -- LOCAL MODULES
var timeConfObject = require('./localModels/timeConfObject.js');

var dbController = require(appRoot+'/controllers/dbController');

//  -- VARS 
var dbConfig

// -- MODULE OBJECT
var dbObj ={};
dbObj.db = {};

var getDbConf = function() { 
  return JSON.parse(fs.readFileSync('./conf/dbConf.json', 'UTF-8')) || {};
}

dbConfig = getDbConf();

// -- PRIVATE METHODS
var initProcedure = function(expiration, cleaningDelay, cleaningPeriod, cb, cleaningDb) {
  var xfun = function() {
   console.log('INFO : Poprawnie połączono z bazą danych');
    dbController.initFun(expiration, cleaningDelay, cleaningPeriod, cb);
  };
  if(cleaningDb) {
    dbController.clearCollections(xfun);
  }  else {
    xfun();
  }   
}

dbObj.start = function(initx) {
  var connUri = dbConfig.uri.replace('<dbuser>', dbConfig.user).replace('<dbpassword>', dbConfig.password);
	dbObj.db = mongoose.connect(connUri);

	mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
	mongoose.connection.once('open', function (callback) {
    initProcedure(timeConfObject.getExpiration(), timeConfObject.getDelay(), timeConfObject.getPeriod(), initx);
	});
}

dbObj.testStart = function(testConfig, cb) {
  
  var startData ={};
  dbController.turnOffCurrentState();



  mongoose.connection.close(function() {
  

    var connUri = testConfig.db.uri.replace('<dbuser>', testConfig.db.user).replace('<dbpassword>', testConfig.db.password);
    dbUndo = dbObj.db;
    dbObj.db = mongoose.connect(connUri);


    mongoose.connection.on('error', function(){});
    mongoose.connection.once('open', function (callback) {
      
      

      startData.testBegin = Date.now();
      startData.testEnd =  startData.testBegin + testConfig.testPeriod;
      
      initProcedure(testConfig.expiration, timeConfObject.getDelay(), timeConfObject.getPeriod(), function() {
       
        cb(startData);
      }, testConfig.clearCollections );
    });
  });
  dbObj.db.disconnect();

  
};


dbObj.testEnd = function(cb) {

  dbController.turnOffCurrentState();
  mongoose.connection.close(function() {
   

      dbObj.start(cb);

    
    
    dbObj.db = mongoose.connection;
    
  });
 
};

module.exports = dbObj;

