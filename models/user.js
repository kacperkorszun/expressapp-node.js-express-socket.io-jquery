var mongoose = require('mongoose');
var crypto = require('crypto');
var ObjectId = mongoose.Types.ObjectId


var oneWord = function(word) {
	var newWord = word.replace(/(\n|\r|\r\n)/g,' ').trim();
	var wordArr = newWord.split(' ');
	if(wordArr.length == 1) {
		return newWord
	} else {
		return null;
	}
}

var user = mongoose.Schema({
    "login": { type :String, required: 'Login is required' },
    "password": { type: String, required: 'Password is required'},
    "avatar": String,
    "timeStamp": Date,
    "superUser" : Boolean,
});

user.pre('save', function(next) {

	var that = this;
	if (that.isNew) {
		that.timeStamp = Date.now();
	}
	if(!that.superUser) that.superUser = false;
	next();
});

user.pre('save', function(next,done) {

	var that = this;
	if (this.isNew) {
		mongoose.models["users"].validateLogin(that.login, function(status, message) {
			
			if(status) {
				mongoose.models["users"].checkLoginAvailability(that.login, function(err, status, message){
					if(err) {
						done(err);

					} else if(status) {

			           //done();
			        } else {
			            that.invalidate("login",message);
			            done(new Error(message));
			        }
				})
			} else {
				that.invalidate("login",message);
		   		done(new Error(message));
			}
		});

		next();
	} else {
		done();
		next();
	}	
});

user.pre('save', function(next) {

	var that = this;
	if (this.isNew) {
		mongoose.models["users"].validatePassword(that.password, function(status, message) {
			if(status) {
				mongoose.models["users"].hashPassword(that.password, that.timeStamp, function(err, newPassword){
					if(err) {
						next(err);
					} else {
						
						that.password = newPassword;
						next();
					}
					
				});
				
			} else {
				that.invalidate("password",message);
	        	next(new Error(message));
			}
		});
	}
});





user.static('checkLoginAvailability', function(login, callback) {
	mongoose.models["users"].findOne({"login" : { $regex : new RegExp(login, "i") }}, function(err, user) {

		if(err) {
			callback(err);
		} else if(user) {
			callback(null, false, "Login must be unique", user);
        } else {
            callback(null, true);
        }
    });
});


user.static('findById', function (id, callback) {
  return this.findOne({ _id: new ObjectId(id) }, callback);
});


user.static('hashPassword', function(password, timeStamp, xcallback) {


	crypto.pbkdf2(password, timeStamp.getTime().toString(), 4096, 512, function(err, key){

		xcallback(err, key.toString('hex'));

	});

});


user.static('validatePassword', function(password, callback) {

	//TO DO : to do after commit
	if(password.length >= 8) {
		callback(true)
	} else {
		callback(false,'Password is too short');
	}
});

user.static("validateLogin", function(login,callback) {

	//TO DO : to do after commit
	var newWord = oneWord(login)
	if(newWord) {
		if(newWord.length > 3) {
			callback(true,null, newWord);
		} else {
			callback(false, 'Login is too short');
		}
	} else {
		callback(false, 'Login must be one word');
	}
	
});


user.static("checkPasswordConfirmation", function(password, confirmPassword) {
	if(password ===  confirmPassword) return true; else return false;
});


user.static('verifyPassword', function(toCheck , password, timeStamp) {

	var pass = crypto.pbkdf2Sync(toCheck, timeStamp.getTime().toString(), 4096, 512).toString('hex');
	if(pass == password) {
		return  true
	} else {
		return false;
	}
});


user.methods.getPix = function(callback, limit, page) {
	var Pixs =require('./pix');
	Pixs.find({ author: this._id }).sort( { "timeStamp": -1 } ).limit(limit).skip(limit * (page || 0)).exec(function(err, items){
		if(err) {
			callback(err);
		} else {
			callback(undefined, items)
		}
	});
    	
};

user.methods.countPix = function(callback) {
	var Pixs =require('./pix');
	Pixs.count({author : this._id}, function( err, count){
      callback(err,count)
    });
} 








module.exports = mongoose.model('users', user);