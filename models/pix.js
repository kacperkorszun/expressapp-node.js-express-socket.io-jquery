var mongoose = require('mongoose');

var Schema = mongoose.Schema;
//var Votes = require('./vote.js');

var Votes = require('./vote.js');
var Users = require('./user.js');

var pix = mongoose.Schema({
    "title": { type :String, required: 'Title is required'},
    "author": { type : Schema.Types.ObjectId, required: 'Author is required'},
    "src": { type :String, required: 'Image address is required'},
    "points": Number,
    "timeStamp": Date
});


pix.pre('save', function(next) {
  var that = this;
  if (that.isNew) {
    that.timeStamp = Date.now();
    that.points = 0;
  }
  next();
});





pix.statics.validAuthor = function(usId ,callback) {
  if(mongoose.Types.ObjectId.isValid(usId)) {
     Users.findById(id,function(err, user){
        if(err) {
          callback(err)
        } else if(user) {
          callback(null,user);
        } else {
          callback(null, false);
        }
      });
  } else {
    callback(null,false);
  }
}


pix.statics.validTitle = function(title) {
  var newTitle = title.replace(/(\n|\r|\r\n)/g,' ').trim();
  if(newTitle.length > 3  && newTitle.length  < 40) {
    return true;
  } else {
    return false;
  }
}

pix.statics.plusPix = function (userId, pixId, callback, unleashed) {
  var that = this;
  this.findById(pixId, function(err, pixOne) {

    if(pixOne) {
      if(unleashed) {
        pixOne.points ++;
        pixOne.save(function(es,err) {
                    //updateObj= {pixId: pixOne.id, action:'plus', points :pixOne.points };
                    //publicStuff.emit('update', JSON.stringify(updateObj));
                    callback('success');
                  });
      } else {
      Votes.findOne({ user : userId },function(err, vote){
        if(vote) {
          if(vote.plusedPixs.indexOf(pixId) != -1) {
            callback("plused");
          } else {
            
            vote.plusedPixs.push(pixId);
            vote.save(function(err,es){if(es){
              that.findById(pixId, function(err,pixTwo) {
                if(pixTwo) {
                  pixTwo.points ++;
                  pixTwo.save(function(err) {
                    //updateObj= {pixId: pixOne.id, action:'plus', points :pixOne.points };
                   
                    //publicStuff.emit('update', JSON.stringify(updateObj));
                    if(err){
                      console.log(err);
                    } else {
                      
                      callback('success');
                    }
                  })
                  
                }
                
              })
            }});
          }
        } else {
          var vote = new Votes({'user' : userId});
          vote.plusedPixs =[];
          vote.plusedPixs.push(pixId);
          vote.save(function(err,es){
            if(es){
             that.findById(pixId, function(err,pixTwo) {
                if(pixTwo) {
                  pixTwo.points ++;
                  pixTwo.save(function(err) {
                    //updateObj= {pixId: pixOne.id, action:'plus', points :pixOne.points };
                    
                    //publicStuff.emit('update', JSON.stringify(updateObj));
                    if(err){
                      console.log(err);
                    } else {
                      callback('success');
                    }
                  })
                  
                }
                
              })
            }
          });
        }
      });
    }
    } else {
      callback('error');
    }
  }
  )
}


pix.methods.getAuthor = function(cb) {
  Users.findById(this.author, function(err, user) {
    cb(err,user);
  })
}

pix.methods.deletePix = function(cb) {
  this.remove(function(err,removed){ 
    
    if(err) {
      cb('error');
      console.log(err);
    } else {
      //console.log(removed);     
      cb('success')
      Votes.update({ plusedPixs: removed._id },{ $pull: {plusedPixs : removed._id } }, { multi: true }, function(err,xremoved) {
        if(err) {
          console.log(err);
        } else {
          //console.log(xremoved);
          
        }
      });
    }
  });
};


pix.methods.getSentObject = function(author) {
  return {
    _id : ''+this._id,
    title : this.title,
    src : this.src,
    points : this.points,
    timeStamp : this.timeStamp,
    author : author.login,
    authorAvatar : author.avatar,
  };
}


module.exports = mongoose.model('pixs', pix);