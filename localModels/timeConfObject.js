var fs = require('fs');
var timingConf = JSON.parse(fs.readFileSync('./conf/timingConf.json', 'UTF-8'));

var obj = {};

obj.getPeriod = function() {
	return timingConf.period || 300000;
}

obj.getDelay = function() {
	return  timingConf.delay || 30000;
}

obj.getExpiration = function(){
	return  timingConf.expiration || 86400000;
}

var saveTimingConf = function() {
  fs.writeFileSync('./conf/timingConf.json', JSON.stringify(timingConfig),'UTF-8');
}

obj.setPeriod = function(period) {
  //
  timingConf.period = period;
  saveTimingConf();
}


obj.setDelay = function(delay) {
  timingConf.delay = delay;
  saveTimingConf();
}

obj.setExpiration = function(expiration) {
  timingConf.expiration = expiration;
  saveTimingConf();
}

obj.refreshConf = function() {
  var timingConf = JSON.parse(fs.readFileSync('./conf/timingConf.json', 'UTF-8'));
}

module.exports = obj;