var obj = {};

obj.UpdateScreenObject = function (type, screen, toScreen, id, dim, top, left, points, newPix, leftBrother) {
    this.type = type ||  null; // resizeAll, deletePix, addPix, updatePix, changePosition, screenStarted
    this.screen = screen || null;
    this.toScreen = toScreen || null;
    this.id = id || null;
    this.changeObj = {
    	dim : dim,
    	points : points,
    	top : top,
    	left : left
    } || null;
    this.newPix = newPix || null;
    this.leftBrother = leftBrother || null;
}

obj.newUpdatePix = function(xscreen, id, points, dim, top, left) {
	return new this.UpdateScreenObject('updatePix', xscreen, null, id, dim, top, left, points); 
}

obj.deletePix = function(xscreen, id) {
    return new this.UpdateScreenObject('removePix', xscreen, null, id); 
}

obj.removeAll = function() {
    return new this.UpdateScreenObject('removeAll',null, null, null);
}

obj.addPix = function(xscreen, newPix, leftBrother) {
    var obj = new this.UpdateScreenObject('newPix', xscreen); 
    obj.newPix = newPix;
    obj.leftBrother = leftBrother;
    return obj;
}


module.exports = obj;
