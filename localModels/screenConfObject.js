
var fs = require('fs');
var screenConfig = JSON.parse(fs.readFileSync('./conf/screenConf.json', 'UTF-8'));

var obj = {};


// -- PRIVATE METHODS
var findByName = function(name) {
  var xScreensSizes = obj.getScreenSizes();
  var i=0;
  while(i<xScreensSizes.length && xScreensSizes[i].name != name ) i++
  if(i == xScreensSizes.length) return -1; else return i;
}

// -- PUBLIC METHODS
obj.getHomeElements = function() {
	return screenConfig.numberOfHomeElements || 16;
}

obj.getPageLength = function() {
	return  screenConfig.poczekalniaPageLength || 30;
}

obj.getScreenSizes = function(){
	return screenConfig.screenSizes || [{
      "name": "big",
      "minSize" : 1200, 
      "width": 1000,
      "tileSize" : 168,
      "minTile" : 100,
      'maxTile' : 700
    }] ;
}





obj.checkName = function(name){
  if(findByName(name) == -1) return false; else return true;
}


obj.chooseScreenByName = function(name) {
  var xScreensSizes = this.getScreenSizes();
  return xScreensSizes[findByName(name)] || null;
}


obj.chooseScreen= function(width){
  var i = 0;
  var screenSizes = this.getScreenSizes();
  while(screenSizes[i].minSize && screenSizes[i].minSize > width)i++;
  return screenSizes[i];
};

obj.findByWidth = function(width) {
  var i = 0;
  var screenSizes = this.getScreenSizes();
  while(screenSizes[i] && screenSizes[i].width != width)i++;
  return screenSizes[i];
};

obj.chooseCurrentScreen= function(width, callback){
  var i = 0;
  var screenSizes = this.getScreenSizes();
  while(screenSizes[i].minSize && screenSizes[i].minSize > width)i++;
  obj = {};
  obj.name = screenSizes[i].name;
  obj.minSize = screenSizes[i].minSize;
  obj.tileSize = screenSizes[i].tileSize
  obj.width = screenSizes[i].width;
  if(i == 0) {
    obj.maxSize = false;
  } else {
    obj.maxSize = screenSizes[i-1].minSize;
  }
  callback(obj);
};

obj.chooseTileSize= function(name) { 
  var tileScreen = this.chooseScreenByName(name);
  if(tileScreen) {
    return tileScreen.tileSize || "100%" ;
  } else {
    return '100%';
  }
}
  

obj.getName = function(width) { return this.chooseScreen(width).name || "xsmall" } ;

module.exports = obj;