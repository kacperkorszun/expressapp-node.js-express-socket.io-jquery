# Node JS/Express Application  #

## Table of Contents ##
* [Short description](#markdown-header-short-description)
* [Overview](#markdown-header-overview)
* [Technologies](#markdown-header-technologies)
## Short description ##
Simple **WebSocket** application based on **Node.JS** platform. Front-end part is just
WebSocket and basic web technologies(HTML/CSS/JS). No plugins.
## Overview ##
###  Idea ([PL](http://en.wikipedia.org/wiki/Polish_language)) ###
Aplikacja składa się ze strony głównej, poczekalni i stron użytkowników.Zachowanie elementów na stronie głównej oraz dwukierunkowa komunikacja z użytkownikiem odbywająca się na niej w 'czasie rzeczywistym'  stanowią główną część aplikacji.

 Strona główna  to 'ściana' obrazków/[memów](https://www.google.pl/search?q=mem&hl=pl&site=imghp&source=lnms&tbm=isch) ułożonych w 'sensowny' sposób. Obrazki posiadają dynamicznie zmieniające się liczniki głosów, zmieniają  rozmiar, pojawiają się i znikają.

Zalogowany użytkownik może głosować na wybrany obrazek/mem. Strona główna zawiera n najpopularniejszych w danym okresie obrazków/memów.  Użytkownik może dodać nowy obrazek,  który widoczny będzie w poczekalni do czasu spełnienia warunków strony głównej. Obrazek, znika po ustalonym czasie po dodaniu( w poczekalni ) lub gdy nie spełnia kryteriów(strona główna). Rozmiar obrazków oraz ilość głosów jest zmieniana na bieżąco w interfejsie użytkownika.


###  Look  ([PL](http://en.wikipedia.org/wiki/Polish_language))###
Obrazki znajdują
 się na kafelkach o proporcjach 1x1. Powierzchnia odpowiada % 		zdobytych głosów wśród najpopularniejszych(1% ≈ 1% pow.).  Po najechaniu na 		obrazek moĹźna na niego zagłosować ( lub skorzystać z innych opcji). O sposobie ułożenia obrazków i ich rozmiarze decyduje serwer. Lokalny skrypt układa,zmienia rozmiar lub usuwa/dodaje  elementy. Wszystkie zmiany elementów wykonywane są  'płynnie'.  Obrazki ułożone są jako mozaika (w 'sprytny' sposób zostawiając możliwie mało wolnego miejsca) 
		Wielkość obrazka nie może być mniejsza niż wartość minimalna . Obrazki w 			poczekalni wyświetlane są jako lista.
## Technologies ##
### Front-end ###
* HTML  - structure of document
* CSS/ less - style of elements 
* JS -  animations, 'real-time' changes in structure of document and style of elements  based on information from server,  events handling

### Back-end ###
* Node.js - application  server
* Express - application  framework
* Java Script with libraries - business logic
* Passport - authentication, js/node libary
* ImageMagick -  software suite for displaying, converting, and editing raster image
* Redis - data structure server
* Socket.IO - WebSocket's js libary 

### Database ###
* MongoDB - database
* mongoose - object modeling for node.js

### Other ###
* JS Hint
* Grunt
* Bower

## Usage ##
### Requirements ###
* Node.js installed
* Npm installed
* Redis installed
* ImageMagick installed
* Internet connection


### Important information ###
This version of project uses external db([mlab](https://mlab.com)) and image hosting ([imgur](imgur.com)) 

### Installation ###
Commandline in project directory:

```
npm install
```

### Run ###
Commandline in project directory:

```
npm start
```