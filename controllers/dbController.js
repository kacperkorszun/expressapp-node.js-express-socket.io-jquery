// -- MONGOOSE MODELS 
var Pixs = require(appRoot+'/models/pix.js');

var Votes = require(appRoot+'/models/vote.js');
var updateModule = require(appRoot+'/updateModule.js');
var timeoutController = require(appRoot+'/controllers/timeoutController');
var dbObj = {};


dbObj.dbOldCleanStart = function (expiration, cb) {
  var xend = Date.now() - expiration;
  Pixs.remove({ "timeStamp": {$lte: xend} },function(err, removed){
    if(!err) {
      console.log("INFO : przeterminowane przed startem :" + JSON.parse(removed).n); 
      cb();
    } else {
      console.log(err);
    }
  })
}

//1
dbObj.dbOldClean = function(expiration, delay, period, cb){
  console.log("INFO : Start Procedury usuwania obrazków");
  var start = Date.now() + delay - expiration;
  var end = start + period; 
  //pixsRemoved = 0;
  Pixs.find({ "timeStamp": {$lte: end} },function(err, items){ 
    if(items) {
      console.log("* Elementów do usunięcia : " + items.length)
      items.forEach(function(entry) {
        var entryEnd = entry.timeStamp.getTime() + expiration;
        if( entryEnd < Date.now()) {
          
         updateModule.queuingUpdates( function(cbb) {

            entry.deletePix(function(status) {
              if(status == 'success') {
                cbb(status,'remove',entry._id);
              } else {
                cbb(status);
              }
              
            });
          });

        } else if(entryEnd < start) {
          if(toKill.indexOf(entry._id) == -1) {
            timeoutController.timeoutRemove(entry,entryEnd);
          }
        } else if(entryEnd > start) {
          timeoutController.timeoutRemove(entry, entryEnd)
        }
    })
  }
    if(cb)cb();
  })
}

//1
dbObj.clearCollections = function(callback)  {
  Pixs.remove({}, function(err) {
    if (err) {
        console.log(err)
    } else {
        Votes.remove({}, function(err) {
          if(err) {
            console.log(err)
          } else {
            callback();
          }
        });
    }
  });
}


dbObj.dbStartClean = function(callback) {
  Pixs.find({ "timeStamp": {$gte: Date.now()} }).remove(function(err,removed) {
          console.log("INFO : Usunięto "+JSON.parse(removed).n+" niepoprawnych obrazków");
          callback();
        }).exec();
}



dbObj.initFun = function(expiration, cleaningDelay, cleaningPeriod, cb) {
 
  dbObj.dbStartClean(function() {
      dbObj.dbOldCleanStart(expiration, function() {
          dbObj.dbOldClean(expiration, cleaningDelay, cleaningPeriod,cb);
          timeoutController.cleaningInterval =  setInterval(function () { dbObj.dbOldClean(expiration, cleaningDelay, cleaningPeriod);}, cleaningPeriod); 
      });
    });
}

dbObj.turnOffCurrentState = function() {
  timeoutController.turnOffCleaningMethods();
  updateModule.emitRemoveAll();
}



module.exports = dbObj;