var homeObj = {};
var screenConfObject = require(appRoot+'/localModels/screenConfObject.js');
var pixController = require(appRoot+'/controllers/pixController');
var updateModule = require(appRoot+'/updateModule.js');

var content ={pixs : [], screens : {} };

var socket;

homeObj.setContent = function(newContent){
  content = newContent
}

homeObj.getContent = function() {
  return content;
}

homeObj.getScreen = function(name, callback) {
    if(screenConfObject.checkName(name)) {
      var i=0;
      var toSendObj = {}
      var toSend = JSON.parse(JSON.stringify(content.pixs));
      if(content.screens[name] == null) {
        toSendObj["puzzle"] = false;
        toSendObj["dimString"] = screenConfObject.chooseTileSize(name);
      } else {
        toSendObj["puzzle"] = true;
        for(i=0;i<toSend.length;i++) {
          toSend[i].top = content.screens[name].images[i].top;
          toSend[i].left = content.screens[name].images[i].left;
          toSend[i].dim = content.screens[name].images[i].dim;
        }
        toSendObj.height = content.screens[name].height; 
      }
      toSendObj["images"] = toSend;
      callback(toSendObj);
    } else {
      return null;
    }
    //
}



var calculateDim = function(percent, width, minTile, maxTile){
  var sqrtt  = Math.sqrt(percent);
  var dim = Math.floor(sqrtt*width); 
  if(dim > maxTile) dim = maxTile;
  if(dim < minTile) dim = minTile;
  return dim;
}

var setOrder = function(pixs, unordredScreen) {
  var j =0;
  var order = [];
  for (var i = 0 ; i< pixs.length; i++) {
    j=0;
    while(j< pixs.length &&  pixs[i]._id != unordredScreen[j].id) j++;
    order.push(j);
  };
  return order;
}

var getOrderedScreen = function(pixs, unordredScreen) {
  var orderedScreen = [];
  var orderOfOrder = setOrder(pixs, unordredScreen);
  var obj ={};
  for(var i = 0; i< orderOfOrder.length; i++) {
    obj = {};
    obj.dim = unordredScreen[orderOfOrder[i]].dim;
    obj.top = unordredScreen[orderOfOrder[i]].top;
    obj.left = unordredScreen[orderOfOrder[i]].left;
    orderedScreen.push(obj);
  }
  return orderedScreen;
}


var addScreen = function(mainScreen, subScreen, startPoint) {
  for(var i = 0 ; i<subScreen.length; i++) {
    subScreen[i].top += startPoint.top;
    subScreen[i].left += startPoint.left;
    mainScreen.push(subScreen[i]);
  }
}

var checkDone = function(set) {
  var j = 0; 
  while(set[j] && set[j].done == true) j++;
  if(j < set.length) return false; else return true;
}

var firstUndone = function(set) {
  var j = 0; 
  while(j < set.length && set[j].done == true) j++;
  if(j == set.length) return null; else return set[j];
}


var findSmallerThan = function(xdim, workSet, pixSizes) {
  var j= 0;
  while(j<workSet.length && ( workSet[j].done || pixSizes[j]  + 30 > xdim )) {
  
    j++;
  }
  if(j==workSet.length) return -1; else return j;
}

var fillBox = function(box, workSet, pixSizes) {
  var z;
  var boxScreen = [];
  var subBoxScreen = [];
  if(box.width > box.height) {
    z = findSmallerThan(box.height, workSet, pixSizes);
  } else {
    z = findSmallerThan(box.width,workSet, pixSizes);
  }
  if(z != -1) {
 
    workSet[z].done = true;

    boxScreen.push({id:  workSet[z]._id, top : 10 , left : 10, dim : pixSizes[z] })
    var boxWidthVertical = box.width;
    var boxHeightVertical = box.height - pixSizes[z] -30;
    if(boxWidthVertical > 30 && boxHeightVertical > 30) {
      subBoxScreen = fillBox({height: boxHeightVertical, width : boxWidthVertical}, workSet, pixSizes);
      addScreen(boxScreen, subBoxScreen, {top : pixSizes[z] + 30, left: 0});
    }

    var boxWidthHorizontal = box.width - 30 - pixSizes[z];
    var boxHeightHorizontal = pixSizes[z] + 30;
    if(boxWidthHorizontal > 30 && boxHeightHorizontal > 30) {
      subBoxScreen = fillBox({height: boxHeightHorizontal, width : boxWidthHorizontal}, workSet, pixSizes);
      addScreen(boxScreen, subBoxScreen, {top : 0, left: pixSizes[z] + 30});
    }

    

  }
  return boxScreen;
}


var isOneLine = function(lineScreen) {
  var j =0;
  if(lineScreen.length < 3) {
    return true;
  } else {
    while(j<lineScreen.length && lineScreen[j].top == 10) {
      j++
    }

    if(j == lineScreen.length) return true; else return false;
  }
}

var composeLine = function(lineScreen, screenWidth) {
  var bufLine = JSON.parse(JSON.stringify(lineScreen));
  var gapsWidth,xgapWidth,oldLeft;
  if(isOneLine(lineScreen)) {
      var dimSum =0;
      for(var k =0; k<lineScreen.length; k++) {
        dimSum += lineScreen[k].dim;
        dimSum +=20;
      }
      gapsWidth = screenWidth - dimSum - (lineScreen.length -1)*10;
      xgapWidth = gapsWidth/2;
      for(var k=0; k<lineScreen.length; k++) {
        bufLine[k].left += xgapWidth - 10; 
      }
  } else {
      var maximum=0,bufmaximum=0;
     for(var k =0; k<lineScreen.length; k++) {
        bufmaximum = bufLine[k].left + bufLine[k].dim + 20;
        if(bufmaximum > maximum) {
          maximum = bufmaximum;
        }
      }
      gapsWidth = screenWidth - maximum +10;
      xgapWidth = gapsWidth/2;
      for(var k=0; k<lineScreen.length; k++) {
        bufLine[k].left += xgapWidth - 10; 
      }
  }

  return bufLine;
}

var nextLine = function(workSet, screenWidth, pixSizes) {
  lineScreen = [];
  firstEl = firstUndone(workSet); 
  firstElIndex = workSet.indexOf(firstEl);
  firstEl.done = true;
  lineScreen.push({id:  firstEl._id, top : 10 , left : 10, dim : pixSizes[firstElIndex]});
  var boxWidth = screenWidth - 40 - pixSizes[firstElIndex];
  var boxHeight = pixSizes[firstElIndex] + 30;
  if(boxWidth > 30 && boxHeight > 30) {
    var boxScreen = fillBox({height: boxHeight, width : boxWidth}, workSet, pixSizes);
    addScreen(lineScreen, boxScreen, {top : 0, left: pixSizes[firstElIndex] + 30});
  } 
  if(lineScreen.length > 0) {
    lineScreen = composeLine(lineScreen, screenWidth);
  }
 
  return lineScreen;
}

var designTheSet = function(pixs, pixSizes, screenWidth) {
  var workSet = JSON.parse(JSON.stringify(pixs));
  for(var ind =0 ; ind < workSet.length; ind++)workSet[ind].index = ind;
  var mainScreen = [];
  var subScreen = [];
  var line = 0; 
  var workSetIndex = 0;

  for(var i=0; i< workSet.length; i++) workSet[i].done = false;

  while(!checkDone(workSet)) {
    workSetIndex = workSet.indexOf(firstUndone(workSet))

    subScreen = nextLine(workSet, screenWidth, pixSizes);
    addScreen(mainScreen, subScreen, {top: line, left: 0});
    line = line + 30 + pixSizes[workSetIndex] ;
  }

  mainScreen.images = getOrderedScreen(pixs, mainScreen);
  mainScreen.height = line;
  return mainScreen;
}

var placePixOnTheScreen = function(pixs,screenWidth, minTile, maxTile) {
  screen = [];  
  pixSizes = []
   for(i=0; i<pixs.length;i++) {
      pixSizes[i] = calculateDim(pixs[i].dim, screenWidth, minTile, maxTile);
    }

    screen = designTheSet(pixs, pixSizes, screenWidth);

    return screen;
}

homeObj.setScreens = function(pixs) {

  var screens = {};
  var sizes = screenConfObject.getScreenSizes();
  var i;
  for(i=0;i<sizes.length;i++) {
    if(sizes[i].width != false) {
      screens[sizes[i].name] = placePixOnTheScreen(pixs, sizes[i].width, sizes[i].minTile, sizes[i].maxTile);
    } else {
      screens[sizes[i].name] = null
    }
  }
  return screens;

}






//screenConfObject.getHomeElements()
homeObj.getHome = function(hpc, callback) {
  var that = this
  pixController.getTopImages(function(status, items){

    if(status) {
      pixController.getSentArr(items, function(newItems,sum){
        var bufSum = sum + items.length; 
        hpc.pixs = items.slice();
        for (var i = 0; i < hpc.pixs.length; i++) {
          hpc.pixs[i].dim = (hpc.pixs[i].points +1) / bufSum ;

        };
        hpc.screens = homeObj.setScreens(hpc.pixs);
        if(callback)callback();
      }) 
      
     
    } else {
     that.initx(function(){
        callback();
     });
    }
    
	}, screenConfObject.getHomeElements());
};





homeObj.getPixList = function(hpc, callback) {
  var that = this
  

   pixController.getTopImages(function(status, items){
    
    if(status == true) {
      pixController.getSentArr(items, function(newItems, sum){
         
          var bufSum = sum + newItems.length;
          hpc.pixs = newItems.slice();
          for (var i = 0; i < hpc.pixs.length; i++) {
            hpc.pixs[i].dim = (hpc.pixs[i].points + 1) / bufSum ;
          }
          callback();
      }) 
      
    } else {
   
     that.initx(function() {
          callback();
     });
    }
    
  }, screenConfObject.getHomeElements());
}


homeObj.initx = function(callback, homeSocket) {

	homeObj.getHome(content,function() {
    console.log("INFO : Załadowano nowy układ strony głównej");

    if(homeSocket){
      updateModule.setHomeSocket(homeSocket);
      socket = homeSocket;
    }
    updateModule.emitStart();
    
    callback();
  });
}

module.exports = homeObj;


