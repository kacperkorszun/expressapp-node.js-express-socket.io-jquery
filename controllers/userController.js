var mongoose = require('mongoose');
var Users = require(appRoot + '/models/user.js');
//var updateModule = require('./../updateModule.js');

var userController = {};


userController.getUserByName = function(login, callback) {
  Users.findOne({ login: login }, function(err, user) {
      callback(err, user)
  });
}

userController.checkLoginAvailability = function(login,callback) {
	Users.checkLoginAvailability(login, callback);
}

userController.validatePassword = function(password,callback) {
	Users.validatePassword(password, callback);
}


userController.getUserPixs = function(id, callback, limit, page) {
  if(mongoose.Types.ObjectId.isValid(id)) {
        Users.findById(id, function(err,user) {
        	if(err) {
        		callback(err)
        	} else if(user) {
        		user.getPix(function(err, items) {
        			if(err) {
        				callback(err)
        			} else{
        				if(limit) {

        					var minima  =  limit * ((page || 0)+1);
        					user.countPix(function(err, count){
                                
        						if(err) {
        							callback(err);
        						} else if(count <= minima) {
						            callback(err, user, items,true);
						        } else {
						        	callback(err, user, items,false);
						        }
        					})
        				} else {
        					callback(err, user, items, true);
        				}
        				
        			}
        		}, limit, page)
        	} else{
        		callback(undefined,undefined);
        	}
        })
  } else {
    callback(undefined, undefined);
  }
}


module.exports = userController;