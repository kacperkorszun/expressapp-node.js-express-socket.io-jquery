var Pixs = require(appRoot + '/models/pix.js');
var Users = require(appRoot + '/models/pix.js');
var infoModule = require(appRoot + '/infoModule.js');
var updateModule = require(appRoot + '/updateModule.js');
var fs = require('fs');
var mongoose = require('mongoose');
var gm = require('gm').subClass({ imageMagick: true });
var shortid = require('shortid');
var request = require('request');
var pixController = {};
var extend = require('util')._extend;


var imgTypeString = 'data:image/png;base64,';


Array.prototype.findByName = function(name,callback) {
  
  var j =0;
  while(j < this.length  && this[j].name != name)j++;
  if(j<this.length) {
    callback(this[j]);
  
  }
}

Array.prototype.delteByName = function(name,callback) {
  
  var j =0;
  while(j < this.length  && this[j].name != name)j++;
  if(j<this.length) {
    if(callback)callback(this[j]);
    this.splice(j, 1);
  }
}

var uploadedArr = [];


var getSentObject = function(obj,author) {
  return {
    _id : ''+obj._id,
    title : obj.title,
    src : obj.src,
    points : obj.points,
    timeStamp : obj.timeStamp,
    author : author.login,
    authorAvatar : author.avatar,
  };
}


pixController.checkPixFileType = function(pixFile) {

    return pixFile.indexOf(imgTypeString) != 0;
 }



var findSinglePix = function(id, callback) {

  if(mongoose.Types.ObjectId.isValid(id)) {

    Pixs.findOne({ _id: id},function(err, pix){
      if(err) {
        throw err
      } else  if(pix){
          callback(pix);
          
      } else {
        callback(undefined);
      }
    });
  } else {
    callback(undefined);
  }
}

pixController.addPix =function(link, title, usId,callback) {

  updateModule.queuingUpdates( function(cb) {

    var newPix = new Pixs({src : link, title : title, author :usId});


    newPix.save(function(err){

      if(err){
        console.log(err);
        callback('error');
        cb('error');
      }else{

        callback('success',newPix);
        cb('success','new', newPix._id);
      }
    });
  });
};


pixController.getPix = function(id, callback) {
  findSinglePix(id, function(pix){
    if(pix) {
      pix.getAuthor(function(err, user) {
        if(err) {
          throw err;
        } else if(user) {
          callback(true, pix.getSentObject(user));
        } else {
           pix.remove(function() {
            callback(false);
          })
        }
      })
    } else {
      callback(undefined, undefined);
    }
  })
}


pixController.uploadNewPix = function( pixFile, pixTitle, pixAuthor, callback ) {
  var that = this;

  var genName = function() {
    var ximgName = shortid.generate();
    if (fs.existsSync(appRoot+'/uploads/'+ximgName +'.png')) {
      return genName();
    } else {
      return ximgName;
    }
  }


  var validImage = function(ximgPath, callback) {
    gm(ximgPath).format(function (err, format) {
      if(err) {
        console.log(err);
        callback(err);
      } else if(format == 'PNG') {
        gm(ximgPath).size(function (err, size) {
          if (!err) {

            if(size.height == 700 && size.width == 700) {
                callback(null);
            } else {
               callback(new Error("Wrong image size"));
            }
          } else {

            callback(err);
          }
        });
      } else {
        callback(new Error("Wrong format"));
      }
    });
  } 


  var imgurImageUpload = function(ximgPath, callback) {
    var formData = { 
      image: fs.createReadStream(ximgPath)
    };

    request.post({url:'https://api.imgur.com/3/upload', headers: {
        Authorization: 'Client-ID ' + 'efdfddc7565d090',
        Accept: 'application/json'
      }, formData: formData}, function optionalCallback(err, httpResponse, body) {
        if (err) {
          callback(err);
        } else {
           var obj = JSON.parse(body);
          console.log('Obraz popprawnie wysłany na serwer IMGUR!');
          fs.unlink(ximgPath) ;
        
          callback(null, obj);
        }
      });
  };

  var pixName = genName();
  var pixPath = appRoot+'/uploads/'+pixName+'.png';
  var buff = new Buffer(pixFile.replace('data:image/png;base64,',''), 'base64');

  fs.writeFile( pixPath, buff, function (err) { 
    if(err) {
      throw err;
    } else {
        validImage(pixPath, function(err){
          if(err) {
            fs.unlinkSync(pixPath);
            callback(err)
            
          } else {
            callback(undefined);
            uploadedArr.push({name : pixName, status :'wait'});
            imgurImageUpload(pixPath, function(er, objr) {
              if(err) {
                infoModule.addMessage(pixAuthor, {status : 'error', text : 'Wystapił błąd podczas wysyłanie na serwer IMGUR'});
              
                fs.unlinkSync(pixPath );
              } else {
               
                 that.addPix(objr.data.link, pixTitle, pixAuthor,function(status){
           
                    var objm = {};
                    objm.status = status;
                    if(status != 'success') objm.text = 'Zapisanie obrazu nie było możliwe'; else {
                      objm.text = 'Obraz zapisany poprawnie';
                    } 
                    infoModule.addMessage(pixAuthor,objm);
                  });
              }
              
            });
          }
        })
      
    }

    });


  }


pixController.addVote = function(userId, pixId, callback,unleashed) {
 
 updateModule.queuingUpdates( function(cb) {
    Pixs.plusPix(userId, pixId, function(status) {
      if(status == 'success') {
        
        callback('success');
        cb('success','plus', pixId);
      } else if(status == 'plused'){
        callback('plused');
        cb('undone');
      } else {
        console.log('err');
        callback('error', 'Obrazek nie istnieje');
        cb('error');
      }
    },unleashed)
  });
  
}

pixController.getSentArr = function(arr,callback){
  var usArr = [];
  var newArr = [];
  var pointsSum = 0;
  
  Array.prototype.findById = function(id) {
   
    var j =0;
    while(j < this.length  && this[j]._id != id)j++;
    if(j<this.length) {
      return this[j]
      //this.splice(j, 1);
    } else {
      return false;
    }
  }

  if(newArr.length == arr) {
    callback(newArr, pointsSum);
  } else {
     arr.forEach(function(entry) {
     
      var buffIndex =  usArr.findById(entry.author);
      if(buffIndex !== false) {
        newArr[arr.indexOf(entry)] = entry.getSentObject(buffIndex);
        pointsSum += entry.points;
        if(newArr.length == arr.length) {
          callback(newArr, pointsSum);
        }
      } else {
       entry.getAuthor(function(err,author) {
          if(err) {
            throw err;
          } else {
            usArr.push(author);
            newArr[arr.indexOf(entry)] = entry.getSentObject(author);
            pointsSum += entry.points;
            if(newArr.length == arr.length) {
              callback(newArr, pointsSum);
            }
          }
       })
      }
    });
  }
  
}


var isEndFun = function(limit, skip, callback) {
  var minima  =  limit + skip;
  Pixs.count({}, function( err, count){
      if(err) {
        callback(err);
      } else if(count <= minima) {
        callback(err, true);
      } else {
        callback(err,false);
      }
  });
}

pixController.getTopImages = function(callback, limit, toSkip, isEnd){

  var pointsSum =0;
  var sendPixs =[];
   Pixs.find().sort( { "points": -1, "timeStamp": 1 } ).limit(limit).skip(toSkip || 0).exec(function(err, items) {
    
    if(err) {
      console.log(err);
      throw err;
    } else if(items) {
      
      if(isEnd) {
          
        isEndFun(limit, toSkip, function(err, result) {
          callback(true, items, result)
        });
      } else {
        callback(true,items);
      }
    } else {
      
      callback(false,[],0,true);

    }
     
   });
}

module.exports = pixController;



