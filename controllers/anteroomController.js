var screenConfObject = require(appRoot+'/localModels/screenConfObject.js');
var pixController = require(appRoot+'/controllers/pixController');

var toSend =[];
anteroomController = {};

anteroomController.getPoczekalnia = function(callback,page) {
    pixController.getTopImages(function(status, items, isEnd){
        pixController.getSentArr(items, function(newItems){
            callback(newItems, isEnd);
        }) 
        
    }, screenConfObject.getPageLength(), screenConfObject.getHomeElements() + (screenConfObject.getPageLength() * (page || 0)), true);
}
    


anteroomController.getRefresh = function(callback,page) {
     pixController.getTopImages(function(status, items, isEnd){
        if(items) {
            
            for(var j =0;j<items.length;j++){
                toSend.push(items[j]._id);
            } 
            
            callback(toSend)
        } else {
            callback([]);
        }
        
    }, screenConfObject.getPageLength(), screenConfObject.getHomeElements() + (screenConfObject.getPageLength() * (page || 0)));
}



module.exports = anteroomController;


            