var userController = require(appRoot+'/controllers/userController');
var screenConfObject = require('./../localModels/screenConfObject.js');

var userPageController = {};
//

var getSentPixs = function(arr,user) {
    var sentArr = [];
    for (var i = 0; i < arr.length; i++) {
        sentArr[i] = arr[i].getSentObject(user);
    };
    return sentArr;
}



userPageController.getUserPixs = function(id, callback,page) {
  userController.getUserPixs(id, function(err,user,items,isEnd){
    if(err){
        throw err;
    } else if(items){
        callback(getSentPixs(items,user), isEnd);
    } else{
        callback([],true)
    }
  }, screenConfObject.getPageLength(),page)
}



userPageController.getRefresh = function(id,callback,page) {
   userController.getUserPixs(id, function(err,user,items,isEnd){
    if(err){
        throw err;
    } else if(items){
        callback(getSentPixs(items,user), isEnd);
    } else{
        callback([],true)
    }
  }, screenConfObject.getPageLength())
};



module.exports = userPageController;