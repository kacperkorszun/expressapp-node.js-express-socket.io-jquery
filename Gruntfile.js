var screenModule = require("./localModels/screenConfObject.js");

var firstLetterUp = function(string) {
  return string[0].toUpperCase() + string.slice(1);
}

var setScreenSizesToFile = function(fileString) {
  var screens = screenModule.getScreenSizes();
  var screenSizesString = "";
  var mediaString = "";
  var i = 0;
  while(i < screens.length && screens[i].width != false ) {

    screenSizesString  += "@ScreenWidth"+firstLetterUp(screens[i].name);
    screenSizesString  += " : "+screens[i].minSize+"px;\n"
    screenSizesString  += "@PicScreenWidth"+firstLetterUp(screens[i].name);
    screenSizesString  += " : "+screens[i].width+"px;\n";
    i++;

  }
  fileString = fileString.replace("<screenWidths>", screenSizesString);
  i =  screens.length-1;
  while(screens[i].width == false  && i != 0) i--;
  for(i;i>=0;i--) {
    mediaString += "@media screen and ( min-width: "+"@ScreenWidth"+firstLetterUp(screens[i].name)+" ){\n";
    mediaString += "\t\twidth: "+"@PicScreenWidth"+firstLetterUp(screens[i].name)+";\n";
    mediaString+="\t}\n"
  }
  
  fileString = fileString.replace("<mediaSec>", mediaString);

  mediaString ="";
  i =  0;
  while(i < screens.length && screens[i].width != false ) i++;
  for(i;i < screens.length ;i++) {
    if(screens[i-1]) {
      mediaString += "div#picScreen"+firstLetterUp(screens[i].name)+"{\n";
      mediaString += "\t\t"+".pView("+screens[i].tileSize.slice(0,-1)+")"+";\n";
      mediaString+="\t}\n"
    }
   
  }

  fileString = fileString.replace("<mediaSmall>", mediaString);
  return fileString;
} 


/*
@media screen and  (max-width: 799px) {
  .picScreen{
    .pView(50);
  }
  
}

@media screen and (max-width: 499px) {
  .picScreen {
    .pView(100);
  }
}

*/






/*@media screen and ( min-width: @ScreenWidthSmall ) {
      width: @PicScreenWidthSmall;
      
    }
  @media screen and ( min-width: @ScreenWidthBig ) {
      width: @PicScreenWidthBig;
      
    }*/



    /*@media screen and ( min-width: @ScreenWidthSmall ) {
      width: @PicScreenWidthSmall;
    }
    @media screen and ( min-width: @ScreenWidthBig ) {
      width: @PicScreenWidthBig;
    }*/




//@ScreenWidthBig : 1100px;
//@PicScreenWidthBig : @ScreenWidthBig - 100px;
//@ScreenWidthSmall : 900px;
//@PicScreenWidthSmall : @ScreenWidthSmall - 100px;


 module.exports = function(grunt) {
  grunt.file.defaultEncoding = 'utf8';
  grunt.file.preserveBOM = false;

  var contents = grunt.file.read("less/mainWork.less");
  contents = setScreenSizesToFile(contents);
  grunt.file.write("less/main.less", contents);

  var contents = grunt.file.read("less/mainWork.less");
  contents = setScreenSizesToFile(contents);
  grunt.file.write("less/main.less", contents);

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.initConfig({less: {
  development: {
    options: {
      paths: ["public/stylesheets"]
    },
    files: {
      "public/stylesheets/main.css": "less/main.less",
      "public/stylesheets/home.css": "less/home.less",
      "public/stylesheets/poczekalnia.css": "less/poczekalnia.less",
      "public/stylesheets/login.css": "less/login.less",
    }
  },
  production: {
    options: {
      paths: ["public/stylesheets"]
    },
    files: {
      "public/stylesheets/main.css": "public/stylesheets/main.less"
    }
  }
}});

  };
