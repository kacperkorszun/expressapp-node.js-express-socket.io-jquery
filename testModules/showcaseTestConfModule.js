var fs = require('fs');
var showcaseTestConf = JSON.parse(fs.readFileSync('./conf/showcaseTestConf.json', 'UTF-8')) || {};

var obj = {};
var dbx = {
    "uri":"mongodb://<dbuser>:<dbpassword>@ds029630.mongolab.com:29630/tswpicsapp",
    "user" : "admin",
    "password":"t3Stw00Rd44"
  };


var checkNumber = function(numberString, min, max) {
  var xnumber = parseInt(numberString);
  if(!isNaN(xnumber) && xnumber >= min && xnumber <= max){
    return true
  }  else {
    return false;
  }
}

var checkExpiration = function(exp) {
   if(exp && checkNumber(exp, 1,100)) {
    return true;
  } else {
    return false;
  }
}

var checkPeriod = function(objx) {
  if(objx.testPeriod && checkNumber(objx.testPeriod, 1,100)) {
    return true;
  } else {
    return false;
  }
}

var checkPixAddingIsNumber = function(objx, buf) {
  console.log('smalone');
  if(objx.pixAddingIsNumber) {
    console.log('smalltwo');
    if(objx.pixAddingIsNumber === 'true') {
      console.log('smalltree');
      buf.pixAddingConf.isNumber = true;
      return true;
    } else if(objx.pixAddingIsNumber === 'false') {
      console.log('smalfor');
      buf.pixAddingConf.isNumber = false;
      return true;
    } else {
      console.log('smalfajv');
      return false
    }
  } else {
    console.log('smallsixx');
    return false;
  }

}

var checkPixPlusIsNumber = function(objx, buf) {
  if(objx.pixPlusIsNumber) {
    if(objx.pixPlusIsNumber === 'true') {
      buf.pixPlusConf.isNumber = true;
      return true;
    } else if(objx.pixPlusIsNumber === 'false') {
      buf.pixPlusConf.isNumber = false;
      return true;
    } else {
      return false
    }
  } else {
    return false;
  }

}

var checkPixAddingNumberOfPixs = function(objx,buf) {
   console.log('tutaj8');
    if(objx.pixAddingNumberOfPixs && checkNumber(objx.pixAddingNumberOfPixs, 1, 100)) {
      buf.pixAddingConf.numberOfPixs  = parseInt(objx.pixAddingNumberOfPixs);
      return true;
    } else {
      return false;
    }
}

var checkPixPlusNumberOfPlus= function(objx, buf) {
   console.log('tutaj8');
    if(objx.pixPlusNumberOfPlus && checkNumber(objx.pixPlusNumberOfPlus, 1, 500)) {
      buf.pixPlusConf.numberOfPlus  = parseInt(objx.pixPlusNumberOfPlus);
      return true;
    } else {
      return false;
    }
}

//STYLES : 'sameTime', 'constant', 'random'


var pixAddingConfx = {
  style : 'sameTime',
  isNumber : false,
  speed : 5000,
  numberOfPixs : 5000,
};

var pixPlusConfx = {
  style : 'constant',
  isNumber : false,
  speed : 5000,
  numberOfPlus : 5000
};

checkPixAddingStyle= function(objx, buf) {
  console.log('tutaj3');
  if(objx.pixAddingStyle) {
    buf.pixAddingConf = {};
    if(objx.pixAddingStyle == 'constant' ||  objx.pixAddingStyle =='random' ||  objx.pixAddingStyle =='sameTime') {
      buf.pixAddingConf.style  = objx.pixAddingStyle;
      console.log('bitgit1');
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
};

checkPixAddingSpeed = function(objx, buf) {
  console.log('tutaj4');
  if(objx.pixAddingSpeed && checkNumber(objx.pixAddingSpeed, 15000, 240000)) {
    buf.pixAddingConf.speed  = parseInt(objx.pixAddingSpeed);
    return true;
  } else {
    return false;
  }
};

checkPixPlusStyle = function(objx, buf){
  console.log('tutaj5');
  if(objx.pixPlusStyle) {
    buf.pixPlusConf = {};
    if(objx.pixPlusStyle == 'constant' ||  objx.pixPlusStyle =='random') {
      buf.pixPlusConf.style  = objx.pixPlusStyle;
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}


checkPixPlusSpeed = function(objx, buf) {
   console.log('tutaj6');
  if(objx.pixPlusSpeed && checkNumber(objx.pixPlusSpeed, 15000, 240000)) {
    buf.pixPlusConf.speed  = parseInt(objx.pixPlusSpeed);
    return true;
  } else {
    return false;
  }
}

obj.getConf = function() {
    var bufConf = {
      'db':  this.getDb(),
      "testPeriod" : this.getTestPeriod(),
      "expiration" : this.getExpiration(),
      "pixAdding" : this.getPixAdding(),
      "pixPlus" : this.getPixPlus(),
      "clearCollections" : this.getClearCollections(),
      "numberOfUsers"  : this.getNumberOfUsers()

    };


    if(bufConf.pixAdding) {
      bufConf.pixAddingConf = this.getPixAddingConf();
    }

    if(bufConf.pixPlus) {
      bufConf.pixPlusConf = this.getPixPlusConf();
     
    }

    return bufConf;
}

obj.getPixPlusConf = function() {
    var bufConf ={};
    bufConf.style = this.getPixPlusStyle();
    bufConf.isNumber = this.getPixPlusIsNUmber();
    if(bufConf.isNumber)  {
      bufConf.numberOfPlus = this.getPixPlusNumberOfPlus();
    } else {
      bufConf.speed = this.getPixPlusSpeed();  
    }
    return bufConf;
}

obj.getPixAddingConf = function() {
  var bufConf ={};
  bufConf.style = this.getPixAddingStyle();
  if(bufConf.style != 'sameTime') {
    bufConf.isNumber = this.getPixAddingIsNumber();
    if(bufConf.isNumber)  {
      bufConf.numberOfPixs = this.getPixAddingNumberOfPixs();
    } else {
      bufConf.speed = this.getPixAddingSpeed();  
    }
  } else {
    bufConf.numberOfPixs = this.getPixAddingNumberOfPixs();
  }
  
  return bufConf;
}


obj.getNumberOfUsers = function() {
  return showcaseTestConf.numberOfUsers || 10;
}





obj.getPixPlusIsNUmber = function() {
  if(showcaseTestConf.pixPlusConf) {
    return showcaseTestConf.pixPlusConf.isNumber || pixPlusConfx.isNumber;
  } else {
    return pixPlusConfx.isNumber;
  }
}


obj.getPixAddingIsNumber = function() {
  if(showcaseTestConf.pixAddingConf) {
    return showcaseTestConf.pixAddingConf.isNumber || pixAddingConfx.isNumber;
  } else {
    return pixAddingConfx.isNumber;
  }
}

obj.getPixAddingNumberOfPixs = function() {
  if(showcaseTestConf.pixAddingConf) {
    return showcaseTestConf.pixAddingConf.numberOfPixs || pixAddingConfx.numberOfPixs;
  } else {
    return pixAddingConfx.numberOfPixs;
  }
}

obj.getPixPlusNumberOfPlus = function() {
  if(showcaseTestConf.pixPlusConf) {
    return showcaseTestConf.pixPlusConf.numberOfPlus || pixPlusConfx.numberOfPlus;
  } else {
    return pixPlusConfx.numberOfPlus;
  }
}

obj.getPixAddingStyle  = function() {
  if(showcaseTestConf.pixAddingConf) {
    return showcaseTestConf.pixAddingConf.style || pixAddingConfx.style;
  } else {
    return pixAddingConfx.style;
  }
}

obj.getPixAddingSpeed  = function() {
  if(showcaseTestConf.pixAddingConf) {
    return showcaseTestConf.pixAddingConf.speed || pixAddingConfx.speed;
  } else {
    return pixAddingConfx.speed;
  }
}

obj.getPixPlusStyle  = function() {
  if(showcaseTestConf.pixPlusConf) {
    return showcaseTestConf.pixPlusConf.style || pixPlusConfx.style;
  } else {
    return pixPlusConfx.style;
  }
}

obj.getPixPlusSpeed  = function() {
  if(showcaseTestConf.pixPlusConf) {
    return showcaseTestConf.pixPlusConf.speed || pixPlusConfx.speed;
  } else {
    return pixPlusConfx.speed;
  }
}

obj.getDb = function() {
  if(showcaseTestConf.db && showcaseTestConf.db.uri) {
    return  showcaseTestConf.db ;
  } else {
    return dbx;
  }
}

obj.getDbUri = function() {
  if(showcaseTestConf.db) {
    return showcaseTestConf.db.uri || dbx.uri;
  } else {
    return dbx.uri
  }
}


obj.getClearCollections = function() {
  return  showcaseTestConf.clearCollections || false;
}



obj.getTestPeriod = function() {
	return showcaseTestConf.testPeriod || 300000;
}


obj.getExpiration = function(){
	return  showcaseTestConf.expiration || 86400000;
}

obj.getPixAdding = function(){
  return  showcaseTestConf.pixAdding || false;
}

obj.getPixPlus = function(){
  return  showcaseTestConf.pixPlus || false;
}

var saveShowcaseTestConf = function() {
  fs.writeFileSync('./conf/showcaseTestConf.json', JSON.stringify(showcaseTestConf),'UTF-8');
}

obj.setDb= function(db) {
  //
  showcaseTestConf.db = db;
  saveShowcaseTestConf();
}

obj.setDbUri= function(dbUri) {
 if(!showcaseTestConf.db) {
    showcaseTestConf.db={};
  }
  showcaseTestConf.db.uri = dbUri;
  saveShowcaseTestConf();
}

obj.setDbUser = function(dbuser) {
  if(!showcaseTestConf.db) {
    showcaseTestConf.db={};
  }
  showcaseTestConf.db.user = dbuser;
  saveShowcaseTestConf();
}

obj.setDbPassword = function(dbPassword) {
  if(!showcaseTestConf.db) {
    showcaseTestConf.db={};
  }
  showcaseTestConf.db.password = dbPassword;
  saveShowcaseTestConf();
}

obj.setTestPeriod = function(testPeriod) {
  showcaseTestConf.testPeriod = testPeriod;
  saveShowcaseTestConf();
}


obj.setExpiration = function(expiration) {
  showcaseTestConf.expiration = expiration;
  saveShowcaseTestConf();
}

obj.setPixAdding = function(pixAdding) {
  showcaseTestConf.pixAdding = pixAdding;
  saveShowcaseTestConf();
}

obj.setPixPlus = function(pixPlus) {
  showcaseTestConf.pixPlus = pixPlus;
  saveShowcaseTestConf();
}


obj.setAll = function(objx) {
  console.log('setAllfun');
  console.log(objx)
  var confBuf  = {db: {}};
  if(objx.dbUser) {
     console.log('1');
    confBuf.db.user = objx.dbUser;
  }

  if(objx.dbPassword && objx.dbPasswordConfirm) {
    if(objx.dbPassword == objx.dbPasswordConfirm) {
      console.log('2');
      confBuf.db.password = objx.dbPassword;
    } else {
      return {type :'error', message : 'Hasła nie są takie same'};
    }
  } else {
    confBuf.db.password = '';
  }
  //plus sprawd uri
  if(objx.dbUri ) {
    console.log('3');
    confBuf.db.uri = objx.dbUri;
  } else {
    return  {type : 'error', message : 'Brak Uri'};
  }

  if(objx.testPeriod && checkPeriod(objx)) {
    confBuf.testPeriod = parseInt(objx.testPeriod) * 60000;
    console.log('4');
  } else {
    return  {type : 'error', message : 'Brak Uri'};
  }

  if(objx.expiration && checkExpiration(objx.expiration)) {
    confBuf.expiration = parseInt(objx.expiration) * 60000;
  } else {
    return  {type :'error', message : 'Brak Uri'};
  }

  if(objx.pixAdding) {
    if(objx.pixAdding === 'true') {
      console.log('siemax');
      confBuf.pixAdding = true;
    } else {
      confBuf.pixAdding = false;
    }
    
  } else {
    return  {type : 'error', message : 'Brak Uri'};
  }

  if(objx.pixPlus) {
    console.log(objx.pixPlus);
    if(objx.pixPlus === 'true') {
      confBuf.pixPlus = true;
    } else {
      confBuf.pixPlus = false;
    }
  } else {
    return  {type : 'error', message : 'Brak Uri'};
  }




  if(objx.clearCollections) {
      console.log('cc');
    if(objx.clearCollections === 'true') {
      confBuf.clearCollections = true;
    } else {
      confBuf.clearCollections = false;
    }
  } else {
    return  {type : 'error', message : 'Brak Uri'};
  }



  if(objx.numberOfUsers) {
    console.log('nou');
    if(checkNumber(objx.numberOfUsers, 1 ,100)) {
      confBuf.numberOfUsers = parseInt(objx.numberOfUsers);
    } else {
      return  {type : 'error', message : 'Brak Uri'};
    }
  } else {
    return  {type : 'error', message : 'Brak Uri'};
  }


  if(confBuf.pixAdding) {
    console.log('siema');
    if(objx.pixAddingStyle) {
      console.log('siema2');
      if(!checkPixAddingStyle(objx, confBuf) ){
        return  {type : 'error', message : ''};
      } else {
        if(confBuf.pixAddingConf.style == 'sameTime') {
          console.log('git');
          if(!checkPixAddingNumberOfPixs(objx,confBuf)) {
            return  {type : 'error', message : ''};
          }
        } else {
          console.log('dwojeczka');
            if(!( objx.pixAddingIsNumber && checkPixAddingIsNumber(objx, confBuf))) {
              console.log('trojeczka');
              return  {type : 'error', message : ''};
            } else {
              console.log('czworeczka');
              if(confBuf.pixAddingConf.isNumber) {
                if(!checkPixAddingNumberOfPixs(objx,confBuf)) {
                  console.log('piateczka');
                  return  {type : 'error', message : ''};
                }
              } else {
                console.log('szosteczka')
                if(!checkPixAddingSpeed(objx,confBuf)) {
                  console.log('siodemka')
                  return  {type : 'error', message : ''};
                } 
              }
            }
        }
      }
    } else {
      console.log('osemka');
      return  {type : 'error', message : ''};
    }
  }

  if(confBuf.pixPlus) {
    console.log('siemaPlus1');
    if(objx.pixPlusStyle && objx.pixPlusIsNumber) {
      console.log('siemaPlus2');
      if(!(checkPixPlusStyle(objx, confBuf) && checkPixPlusIsNumber(objx, confBuf))){
        return  {type : 'error', message : ''};
      } else {
        console.log('siemaPlus3');
        if(confBuf.pixPlusConf.isNumber) {
          if(!checkPixPlusNumberOfPlus(objx,confBuf)) {
            return  {type : 'error', message : ''};
          }
        } else {
          if(!checkPixPlusSpeed(objx,confBuf)) {
            return  {type : 'error', message : ''};
          } 
        }
      }
    } else {
      return  {type : 'error', message : ''};
    }
  }
  console.log(confBuf);
  console.log('success');
  showcaseTestConf = confBuf;
  saveShowcaseTestConf();
  return  {type : 'success', message : 'sukces'};

}

obj.refreshConf = function() {
  showcaseTestConf = JSON.parse(fs.readFileSync('./conf/showcaseTestConf.json', 'UTF-8'));
}

obj.getRefresh  = function() {
  this.refreshConf();
  return this.getConf();
}


module.exports = obj;