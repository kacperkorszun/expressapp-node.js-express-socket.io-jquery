var showcaseTestConfModule = require('./showcaseTestConfModule.js');
var dbModule = require(appRoot+'/dbModule.js');
var pixController = require(appRoot + '/controllers/pixController.js');
var Pixs = require('./../models/pix.js');
var Users = require('./../models/user.js');
var showcaseTestSocket;
var homeController = require(appRoot+'/controllers/homeController');

var testStarted = false
var shortid = require('shortid');



/// to form
var testConf;
var passport = require('passport');


var pixsArr = [];
var usersArr = [];
var stats = {};

var obj = {};
var showcaseTo;


var closeTest = function() {
	console.log("Start testu : "+stats.testBegin);
	console.log("Planowany koniec  : "+stats.testEnd);
	console.log("Koniec: " + Date.now());
	console.log("Użytkownicy dodani"+usersArr.length+"/"+testConf.numberOfUsers);
	if(testConf.pixPlus)console.log('splusowane obrazy ....'+stats.pixsPlused)
	showcaseTestSocket.emit('testEnd', testStarted);
	dbModule.testEnd(function() {
		testStarted = false;
		homeController.initx(function() {});
	});
};


obj.cancelTest = function(cb) {
	clearTimeout(showcaseTo);
	closeTest();
}

obj.setShowcaseTestSocket = function(socket) {
	showcaseTestSocket = socket;
}

var randInt = function(val) {
  return Math.floor((Math.random() * val));
}

var getRandTime = function(med) {
	var min = 0;
	var max = 2*med;
	return Math.floor((Math.random() * (max - min))  + min);
}

var genTitle = function(){
	var xcounter = -1;
	return  function() {
		xcounter++;
		return 'test pix '+xcounter;
	}
}(); 


var genTestPix = function(){
	var srcArr = ['http://i.imgur.com/yozuihp.png',
'http://i.imgur.com/d3tjNBk.png',
'http://i.imgur.com/lGZoOQX.png',
'http://i.imgur.com/8fTocOc.png',
'http://i.imgur.com/i8Lmyu0.png',
'http://i.imgur.com/lUFHCp1.png',
'http://i.imgur.com/KP1XqD5.png',
'http://i.imgur.com/Im2fzfK.png',
'http://i.imgur.com/TkwSX18.png',
'http://i.imgur.com/bp3lRjD.png']; 
	return function() {
		console.log('hello genTestPix');
		if(usersArr.length > 0) {
			console.log('hello genTestPix x');
			var pix ={};
			pix.usId = usersArr[randInt(usersArr.length)];
			pix.src = srcArr[randInt(srcArr.length)];
			pix.title = genTitle();
			return  pix;

		} else {
			connsole.log('blad');
			return null;
		}
	};
}();


var genSuperUserPassword = function() {
	return 'adminadmin';
}

var genSuperUser = function(callback){
	console.log("Tworzenie admina")
	Users.checkLoginAvailability('admin' , function(err, result, message, user) {
      if (err) { 
      	throw err; 
      } else if(!result) {
      	user.remove(function() {
      		genSuperUser(callback);
      	});
      } else  {
      	var newUser = new Users({});
	    newUser.login = 'admin';
	    newUser.avatar = "http://i.imgur.com/QTZFzOR.png";
	    newUser.timeStamp = Date.now();
	    newUser.password = genSuperUserPassword();
	    newUser.superUser = true;
	    newUser.save(function(err, user){
	    	callback(err, user);
	    });
      }
    });
}



var randomModeNumberAddPix = function(numberOfPixs, speed, testStartedx) {
	console.log(speed);
	if(pixsArr.length < numberOfPixs && testStarted == testStartedx) {
		var pix = genTestPix();
		var xtime = getRandTime(speed);
		if(pix)setTimeout(function() {
			if(testStarted == testStartedx)pixController.addPix(pix.src, pix.title, pix.usId,function(funRes, es){
				if(funRes == "success") {
					console.log("Dodano nowy obrazek");
					pixsArr.push(es);
					randomModeNumberAddPix(numberOfPixs, speed, testStartedx);
				} else {
					connsole.log("Wystapil blad.")
				}
			});
		}, xtime);
	}
};



var randomModeTimeoutAddPix = function(speed, testStartedx) {
	if(testStarted == testStartedx) {
		var pix = genTestPix();
		var xtime = getRandTime(speed);
		if(pix)setTimeout(function() {
			if(testStarted == testStartedx)pixController.addPix(pix.src, pix.title, pix.usId,function(funRes, es){
				if(funRes == "success") {
					console.log("Dodano nowy obrazek");
					pixsArr.push(es);
					randomModeTimeoutAddPix(speed, testStartedx);
				} else {
					connsole.log("Wystapil blad.")
				}
			});
		}, xtime);
	}
};



var constantModeTimeoutAddPix = function(speed, testStartedx) {
	if(testStarted == testStartedx) {
		var pix = genTestPix();
		if(pix)setTimeout(function() {
			if(testStarted == testStartedx)pixController.addPix(pix.src, pix.title, pix.usId,function(funRes, es){
				if(funRes == "success") {
					console.log("Dodano nowy obrazek");
					pixsArr.push(es);
					constantModeTimeoutAddPix(speed, testStartedx);
				} else {
					console.log("Wystapil blad.")
				}
			});
		}, speed);
	}
};


var constantModeNumberAddPix = function(numberOfPixs, speed, testStartedx) {
	if(pixsArr.length < numberOfPixs && testStarted == testStartedx) {
		var pix = genTestPix();
		if(pix)setTimeout(function() {
			if(testStarted == testStartedx)pixController.addPix(pix.src, pix.title, pix.usId,function(funRes, es){
				if(funRes == "success") {
					console.log("Dodano nowy obrazek");
					pixsArr.push(es);
					constantModeNumberAddPix(numberOfPixs, speed, testStartedx);
				} else {
					connsole.log("Wystapil blad.")
				}
			});
		}, speed);
	}
};




var sameTimeModeAddPix = function(numberOfPixs, testStartedx) {

	if(pixsArr.length < numberOfPixs && testStarted == testStartedx) {
		console.log("-wszystkie naraz");
		var pix = genTestPix();
		console.log(pix);
		console.log(testStarted == testStartedx);
		if(pix)if(testStarted == testStartedx)pixController.addPix(pix.src, pix.title, pix.usId,function(funRes, es){
			if(funRes == "success") {
				console.log("Dodano nowy obrazek");
				pixsArr.push(es);
				sameTimeModeAddPix(numberOfPixs, testStartedx);
			} else {
				connsole.log("Wystapil blad.")
			}
		}) 
	}	
} 

var genSpeed = function(numberOfPixs) {
	return Math.floor(stats.testPeriod / (numberOfPixs + 1)); 
}


//'sameTime', 'constant', 'random'

var genAllPixs = function(mode, testStartedx, pixAddingSpeed, numberOfPixs) {
	console.log("Generowanie obrazków.");
	if(mode == 'sameTime') {
		
		sameTimeModeAddPix(numberOfPixs, testStartedx);
	} else if (mode == 'constant'){
		if(numberOfPixs) {
			console.log("-");
			var xspeed = genSpeed(numberOfPixs);
			constantModeNumberAddPix(numberOfPixs, xspeed, testStartedx);
		} else {
			constantModeTimeoutAddPix(pixAddingSpeed, testStartedx)
		}
	} else if(mode == 'random') {
		if(numberOfPixs) {
			console.log('random number')
			var xspeed = genSpeed(numberOfPixs);
			randomModeNumberAddPix(numberOfPixs, xspeed, testStartedx);
		} else {
			randomModeTimeoutAddPix(pixAddingSpeed, testStartedx)
		}
	}
}



var getRandPix = function(callback, testStartedx) {
	if(testStarted == testStartedx) {
		Pixs.find().sort( { "points": -1, "timeStamp": 1 } ).exec(function(err, items) {
			console.log('hello');
			if(err) {
				console.log('blad');
				console.log(err);
			} else {
				if(items.length > 0) {
					console.log(items.length)
					var randIndex = randInt(items.length);
					callback(items[randIndex]);
				} else {
					setTimeout(function() { getRandPix(callback,testStartedx) }, 100);
				}
			}
	   });
	}
}

var plusPixs = function(callback, testStartedx) {
	console.log('funkcja plusujaca')
	var randUser =  usersArr[randInt(usersArr.length)];
	if(randUser) {
		console.log('randuser: '+ randUser);
		getRandPix(function(randPix){
			console.log('rand pix: '+ randPix);
			if(testStarted == testStartedx)pixController.addVote(randUser, randPix._id, function(status, message){
				console.log('vote was added');
				if(status == 'success' || status == 'plused') {
					if(status == 'success' ) {
						if(stats.pixsPlused) stats.pixsPlused++; else stats.pixsPlused = 1;
						callback();
					} else {
						console.log("plus zostal powtórzony.");
						plusPixs(callback,testStartedx);
					}
					
				}
			},false);
		},testStartedx);
	} else {
		console.log('err')
	}
}

var nameArr = ['adam','ola','ewa', 'zenek', 'kacper', 'janusz', 'jonasz', 'mariusz', 'max', 'krysia'];

var genRandName = function(callback) {
	var firstPart = nameArr[Math.floor(Math.random() * nameArr.length)];
	var secondPart = Math.floor(Math.random() * 100);
	console.log(firstPart+secondPart);
	Users.checkLoginAvailability(firstPart+secondPart,function(err, result, message, login){
		if(!err) {
			if(result) {
				callback(firstPart+secondPart)
			} else {
				genRandName(callback)
			} 
		} else {
			throw err;
		}
	}) 
};





var randomModeTimeoutPlusPix = function(speed, testStartedx) {
	if(testStarted == testStartedx) {
		var xtime = getRandTime(speed);
		setTimeout(function() {
			plusPixs(function(){
				randomModeTimeoutPlusPix( speed, testStartedx);
			}, testStartedx);
		}, xtime);
	}
};

var constantModeNumberPlusPix= function(numberOfPlus, speed, testStartedx) {
	debugger;
	console.log('fun con num plus');
	if(stats.pixsPlused < numberOfPlus && testStarted == testStartedx) {
		setTimeout(function() {
			plusPixs(function(){
				constantModeNumberPlusPix(numberOfPlus, speed, testStartedx);
			}, testStartedx);
		}, speed);
	}
};


var randomModeNumberPlusPix = function(numberOfPlus, speed, testStartedx) {

	if(stats.pixsPlused < numberOfPlus  && testStarted == testStartedx) {
		var xtime = getRandTime(speed);
		setTimeout(function() {
			plusPixs(function(){
				randomModeNumberPlusPix(numberOfPlus, speed, testStartedx);
			}, testStartedx);
		}, xtime);
	}
};


var constantModeTimeoutPlusPix = function(speed, testStartedx) {
	if(testStarted == testStartedx) {

		setTimeout(function() {
			plusPixs(function(){
				constantModeTimeoutPlusPix(speed, testStartedx);
			}, testStartedx);
		}, speed);
	}
};


var genAllPlus = function(mode, testStartedx, pixPlusSpeed, numberOfPlus) {
	if (mode == 'constant'){
		if(numberOfPlus) {
			stats.pixsPlused = 0;
			var xspeed = genSpeed(numberOfPlus);
			console.log('xspeed :'+xspeed);
			debugger;
			constantModeNumberPlusPix(numberOfPlus, xspeed, testStartedx);
		} else {
			constantModeTimeoutPlusPix(pixPlusSpeed, testStartedx)
		}
	} else if(mode == 'random') {
		if(numberOfPlus) {
			stats.pixsPlused = 0;
			var xspeed = genSpeed(numberOfPlus);
			randomModeNumberPlusPix(numberOfPlus, xspeed, testStartedx);
		} else {
			randomModeTimeoutPlusPix(pixPlusSpeed, testStartedx)
		}
	}
}

var addUser = function(login, password, avatar, callback) {
	Users.checkLoginAvailability(login,function(err, result, message, user) {
		console.log('**');
		if(err) {
			throw err;
		} else if(!result) {
			callback(null, false);
		} else {
			var newUser = new Users({});
		    newUser.login = login;
		    newUser.timeStamp = Date.now();
		    newUser.avatar = avatar;
		    newUser.password = password;
		    newUser.superUser = false;
		    newUser.save(function(err,user){
		    	if(err) {
		    		callback(err, false);
		    	} else {
		    		callback(err,true, user);
		    	}
		    	
		    });
		}
	})
}

var genPassword = function() {
	return 'c77N2SvT';
}


var genAvatar = function() {
	avArr = [
	"http://i.imgur.com/okQV9cn.png",
	"http://i.imgur.com/u4GXMmn.png",
	"http://i.imgur.com/hCUHzLR.png",
	"http://i.imgur.com/IL7mQmI.png",
	"http://i.imgur.com/WoTokTB.png",
	"http://i.imgur.com/IkazPRV.png"
	];

	return avArr[randInt(avArr.length)];
}



var nextUser = function(callback) {
	if(usersArr.length < testConf.numberOfUsers) {
		console.log('*');
		genRandName(function(login) {
			console.log('***');
			xpass = genPassword();
			var avatar = genAvatar();
			addUser(login, xpass, avatar, function(err, saved, newUser) {
				if(err) {
					throw err;
				} else {
					if(saved) {
						console.log('zapisany')
						usersArr.push(newUser._id);
						nextUser(callback);
					}  else {
						console.log('istnieje taki uzytkownik. Jakis bug');
						nextUser(callback);
					}
				}
			})
		});
	} else {
		callback();
	}
}


var genUsers = function() {
	nextUser();
}




obj.getTestInfo = function(tId) {
	//current end testPeriod  testConf
	if(tId === testStarted) {
		return {
			status : 'success',
			tId : testStarted,
			name : 'showcaseTest',
			current : Date.now(),
			testPeriod : testConf.testPeriod,
			testEnd : stats.testEnd,
			testConf: testConf
		};
	} else {
		return { status : 'error'};
	}
}

obj.getTestId = function() {
	return testStarted;
}

obj.startTest = function(cb) {
	var that = this;
if(!testStarted) {
  testStarted = shortid.generate();
  console.log('--Start Showcase Test ---');
  testConf = showcaseTestConfModule.getConf();
  console.log(testConf);
  console.log("Czy czyscic baze ..................... "+testConf.clearCollections);
  dbModule.testStart(testConf, function(startData) {
  
  		var xfun = function() {
  			cb();
  			var xback = function() {}
  			
  			
	  		stats = startData;
			pixsArr =[];
			usersArr =[];
			showcaseTestSocket.emit('testInfo', that.getTestInfo(testStarted));
			console.log("Generowanie użytkowników ");
			console.log("Liczba użytkowników do wygenerowania ..................... "+testConf.numberOfUsers);
			usersArr = [];
		  	nextUser(function() {
		  		console.log("Generowanie użytkowników zakończone. Wygenerowano "+usersArr.length);
		  		console.log("Czy dodac obrazy ..................... "+testConf.pixAdding);
		  		if(testConf.pixAdding)genAllPixs(testConf.pixAddingConf.style, testStarted, testConf.pixAddingConf.speed, testConf.pixAddingConf.numberOfPixs);
		  		console.log("Czy plusowac obrazy ..................... "+testConf.pixPlus);
		  		if(testConf.pixPlus)genAllPlus(testConf.pixPlusConf.style, testStarted, testConf.pixPlusConf.speed, testConf.pixPlusConf.numberOfPlus);

		  	});
		  	stats.testPeriod = showcaseTestConfModule.getTestPeriod();
		  	showcaseTo = setTimeout(function() {
		  		closeTest();
		  	}, startData.testEnd - Date.now());
		};
		homeController.initx(function() {
			genSuperUser(function(err, adUser){
	  			if(err) {
	  				throw err;
	  			} else if(addUser) {
	  				console.log(adUser);
	  				xfun()
	  			}	
	  		});
		});
  		
  });
	} else {
		console.log('mozna odpalic tylko jeden test na raz');
	}
}

module.exports = obj;