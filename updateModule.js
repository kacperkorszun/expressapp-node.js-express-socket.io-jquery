
var homeController;
var updateScreenObject = require('./localModels/updateScreenObject.js');
var screenConfObject = require('./localModels/screenConfObject.js');

var obj  = {};
var homeSocket = {};
var anteroomSocket = {};
var extend = require('util')._extend;

obj.emitStart = function() {
	emitHomeUpdate(new updateScreenObject.UpdateScreenObject('screenStarted'));
}

var emitHomeUpdate = function(updateObj) {
	homeSocket.emit('updateScreen', updateObj);
} 

var singlePixUpdate = function(xscreen,id,points, dim, top, left) {
	
	var obj =updateScreenObject.newUpdatePix(xscreen, id, points, dim, top, left);
	emitHomeUpdate(obj);
}

var deletePix  = function(newOne, id, changeVectors) {
	var buforObj = {};
	var singleChange ={};
	for (var xscreen in newOne.screens) {
		if(newOne.screens[xscreen] != null) {
			singleChange ={};
			var obj =updateScreenObject.deletePix(xscreen, id);
			singleChange.obj = obj;
			singleChange.fun = function() { emitHomeUpdate(this.obj);}
			changeVectors[xscreen].push(singleChange);
			buforObj = {};
		}
	}
	
	var obj =updateScreenObject.deletePix('other', id);
	emitHomeUpdate(obj);
	buforObj = {};
	return changeVectors;
}


obj.emitRemoveAll = function() {
	var buforObj = updateScreenObject.removeAll();
	emitHomeUpdate(buforObj);
	emitUpdateStreamRemoveAll();
}

var addPix = function(newOne, i, id, changeVectors) {
	var buforObj = {};
	var singleChange ={};
	for (var xscreen in newOne.screens) {
		if(newOne.screens[xscreen] != null) {
			buforObj  = JSON.parse(JSON.stringify(newOne.pixs[i]));;
			buforObj.dim  = newOne.screens[xscreen]['images'][i].dim;
			buforObj.top = newOne.screens[xscreen]['images'][i].top;
			buforObj.left = newOne.screens[xscreen]['images'][i].left;
			var obj =updateScreenObject.addPix(xscreen, buforObj, id);
			singleChange.obj = obj;
			singleChange.fun = function() { emitHomeUpdate(this.obj);}
			changeVectors[xscreen].push(singleChange);
			buforObj = {};
			singleChange ={};
		}
	}
	buforObj  = JSON.parse(JSON.stringify(newOne.pixs[i]));
	var obj =updateScreenObject.addPix('other', buforObj, id);
	emitHomeUpdate(obj);
	buforObj = {};
	return changeVectors;
}


var emitNewHeight = function(screens) {
	for (var xscreen in screens) {
		if(screens[xscreen] != null) { 
			homeSocket.emit("newPicScreenHeight",{ toScreen : xscreen, height : screens[xscreen].height})
		}
	}
}


var reduceDiff = function(newOne, old, add, changeVectors) {
	var i = 0;
	var j =0;
	var selected = [];
	var rdObj = {};
	var first,second;
	if(add) {
		first = newOne.pixs;
		second = old.pixs;
	} else {
		first = old.pixs;
		second = newOne.pixs;
	}
	for(i=0;i<first.length;i++) {
		j= getFromTableById(second, first[i]._id);
		if(j == -1){
			var brother  = null;
			if(newOne.pixs[i-1])brother = newOne.pixs[i-1]._id;
			if(add)changeVectors = addPix(newOne, i,  brother, changeVectors);
			if(!add)changeVectors =  deletePix(newOne,first[i]._id,changeVectors);
			selected.push(i)	
		}
	}
	rdObj.selected = selected;
	rdObj.changeVectors = changeVectors;
	return rdObj;
}

var changeUnremovedPixs = function(old, newOne, deleted, changeVectors) {
	var i = 0;
	var j = 0;
	var xid, points, dim, top, left;
	var singleChange ={};
	
	for (i = 0; i < old.pixs.length; i++) {

		if(deleted.indexOf(i) == -1) {

			xid  = old.pixs[i]._id;

		
			j = getFromTableById(newOne.pixs,xid)
			points = newOne.pixs[j].points;
			 
			for (var xscreen in old.screens) {
				if(old.screens[xscreen] != null) {
					
					singleChange ={};
					dim = null;
					top = null;
					left = null;
					
					if(old.screens[xscreen]['images'][i].dim != newOne.screens[xscreen]['images'][j].dim)dim =  newOne.screens[xscreen]['images'][j].dim;
					if(old.screens[xscreen]['images'][i].top != newOne.screens[xscreen]['images'][j].top)top=  newOne.screens[xscreen]['images'][j].top;
					if(old.screens[xscreen]['images'][i].left != newOne.screens[xscreen]['images'][j].left)left =  newOne.screens[xscreen]['images'][j].left;
					singleChange = {dim : dim, top: top, left : left, id : xid, screen : xscreen};
					singleChange.fun = function() { singlePixUpdate(this.screen, this.id, this.points, this.dim, this.top, this.left); };

					changeVectors[xscreen].push(singleChange);
				}
			}
		} 
	
	};
	return changeVectors;
}

var getFromTableById = function(table, id) {
	var  i =0;
	while(i < table.length && id != table[i]._id) {

		i++;
	}
	
	if(i == table.length) return -1; 
	else return i;
}

var emitUpdateSmall = function(id, points, changePosition, leftBrother) {
	changeObj = {points: points, changePosition : false};
	if(changePosition) {
		changeObj.changePosition = true;
		changeObj.leftBrother = leftBrother;
	} 
	homeSocket.emit('updateScreen',{ type : 'updatePixSmall', id : id, changeObj : changeObj});
}


var changeArrPosition = function(id, leftBrother, arr) {
	var i=0, bfx={};
	var xar = arr;

	while(i <xar.length && id != xar[i]._id ) i++;
	if(i ==  xar.length) {
		// error
	}  else {

		bfx = JSON.parse(JSON.stringify(xar[i]));

		xar.splice(i, 1);
	} 

	i=0;
	if(leftBrother != false) {
		
		while(i <xar.length && leftBrother != xar[i]._id ) i++;
	} else {
		i = -1;
	}
	
	if(i ==  xar.length) {
		// error
	}  else {
		xar.splice(i+1, 0, bfx);
	} 
	var j=0;
	return xar;
}


var sameOrderArr = function(arr1, arr2) {
	var i =0;
	while(i < arr1.length && arr1[i]._id == arr2[i]._id ) i++;
	
	if(i == arr1.length) return true; else false;

}


var samePixsOnHome = function(old, newOne, sameOrder, changeVectors) {
	
	var i = 0;
	var j = 0;
	var singleChange = {};
	var xid, points, dim, top, left;
	for(i=0;i< old.pixs.length; i++) {
		xid  = old.pixs[i]._id;
		j=i;
		if(sameOrder) {
			if(old.pixs[i].points != newOne.pixs[i].points) {
				emitUpdateSmall(xid, newOne.pixs[i].points, false);
			}
		}
		if(!sameOrder)j = getFromTableById(newOne.pixs,xid)
		points = newOne.pixs[j].points;
		 
		for (var xscreen in old.screens) {
			
			if(old.screens[xscreen] != null) {
				singleChange = {};
				dim = null;
				top = null;
				left = null;
				if(old.screens[xscreen]['images'][i].dim != newOne.screens[xscreen]['images'][j].dim)dim =  newOne.screens[xscreen]['images'][j].dim;
				if(old.screens[xscreen]['images'][i].top != newOne.screens[xscreen]['images'][j].top)top=  newOne.screens[xscreen]['images'][j].top;
				if(old.screens[xscreen]['images'][i].left != newOne.screens[xscreen]['images'][j].left)left =  newOne.screens[xscreen]['images'][j].left;
				singleChange = { id : xid, screen : xscreen, points : points, dim : dim, top : top, left : left};
				singleChange.fun = function() { singlePixUpdate(this.screen, this.id, this.points, this.dim, this.top, this.left); };
				changeVectors[xscreen].push(singleChange);
				
			}
		}

	}
	if(!sameOrder) {

		var buforTab =  JSON.parse(JSON.stringify(old.pixs));
		
		var startPoint = 0;
		while(buforTab[startPoint]._id == newOne.pixs[startPoint]._id) {
		
			if(buforTab[startPoint].points != newOne.pixs[startPoint].points) {
				emitUpdateSmall(buforTab[startPoint]._id, newOne.pixs[startPoint].points, false);
			}
			startPoint++;
		}
		var leftBrother = false;
		var i=startPoint;
		while(!sameOrderArr(buforTab, newOne.pixs)) {
			
			if(newOne.pixs[i-1]) leftBrother = newOne.pixs[i-1]._id; else leftBrother = false; 
			emitUpdateSmall(newOne.pixs[i]._id, newOne.pixs[i].points, true, leftBrother);
			buforTab = changeArrPosition(newOne.pixs[i]._id, leftBrother, buforTab);
			
			var j=0;
			
			i++;
		}
		while(i < newOne.pixs.length) {
			if(buforTab[i].points != newOne.pixs[i].points) {
				emitUpdateSmall(newOne.pixs[i]._id, newOne.pixs[i].points, false);
			}
			i++;
		}
	}
	return changeVectors;
}

var compareTables = function(a,b) {
	var  i =0;
	while(i < a.length && a[i]._id == b[i]._id ) {
		i++
	}
	if(i == a.length){
		return true; 
	} 
	else return false;
}

var busy = false;
var queue = [];


obj.queuingUpdates = function(action) {
	var that = this;
	
	if(busy || queue.length >0) {
		
		queue.push(action);
	} else {
		
		busy = true;
		action(function(status,title,id){
			if(status == 'success') {
				that.update({title : title, id : id});
			} else {
				busy = false;
				that.nextInQueue();
			}
		});
	}
}


obj.nextInQueue = function() {
	var that = this;
	if (queue.length > 0 && !busy) {
		
		busy = true;
		var current = queue[0];
		queue.shift();
		current(function(status,title,id){
			if(status == 'success') {
				that.update({title : title, id : id});
			} else {
				busy = false;
				that.nextInQueue();
			}
			
		});
	}
}


obj.setAnteroomSocket = function(socket) {
	anteroomSocket = socket;
}

obj.setHomeSocket = function(socket) {
	homeSocket = socket;
}

obj.setUpdateStreamSocket = function(socket){
	updateStreamSocket = socket;
} ;

var checkContains = function(id, table) {
	if(getFromTableById(table,id) == -1 ) return false; else return true;
}

var emitUpdateStreamPlus = function(type) {
	var Pixs = require('./models/pix.js');
	var mongoose = require('mongoose');
	var sendobj = {};
	if(mongoose.Types.ObjectId.isValid(type.id)) {

    Pixs.findById(type.id,function(err, pix){
    	if(err) {
    		throw err
    	} else  if(pix){
    		sendobj.type = 'plus';
    		sendobj.id = pix._id;
    		sendobj.points = pix.points;
    		updateStreamSocket.emit('action', sendobj);
    	} else {
    		//err
    	}
    });
  } else {

  }

}



var emitUpdateStreamDelete = function(type) {
	updateStreamSocket.emit('action', {type : 'delete', id : type.id});
}

var emitUpdateStreamNew = function(type) {
	updateStreamSocket.emit('action', {type : 'new', id : type.id});
}

var emitUpdateStreamRemoveAll = function() {
	updateStreamSocket.emit('action', { type : 'removeAll' });
}


var setChangeVectors = function(screens) {
	var vector ={}
	for (var xscreen in screens) {
		vector[xscreen] = [];
	}
	return vector;
}


var emitVectors = function(arr) {
	for(var k =0 ; k < arr.length ; k++ ) {
		arr[k].fun();
	}
}

var emitInOrder = function(changeVectors, screens, oldScreens) {
	for (var xscreen in screens) {
		if(screens[xscreen] != null)  {
			if(oldScreens[xscreen].height < screens[xscreen].height) {
				homeSocket.emit("newPicScreenHeight",{ toScreen : xscreen, height : screens[xscreen].height});
				emitVectors(changeVectors[xscreen]);
			} else {
				emitVectors(changeVectors[xscreen]);
				homeSocket.emit("newPicScreenHeight",{ toScreen : xscreen, height : screens[xscreen].height});			
			}
		}
	}
}

obj.update = function(type) {

	var homeController = require(appRoot+'/controllers/homeController');
	var newOne = {};
	var current = homeController.getContent();
	var that = this;

	var changeVectors;



	if(type.title == 'plus') {
		emitUpdateStreamPlus(type);
		homeController.getPixList(newOne, function() {
			if(!(checkContains(type.id, current.pixs) || checkContains(type.id, newOne.pixs))) {
				
			} else {
				//console.log("Plus from * to home");

					if(compareTables(newOne.pixs, current.pixs)) {
						
						newOne.screens = homeController.setScreens(newOne.pixs);
						changeVectors = setChangeVectors(newOne.screens);
						changeVectors = samePixsOnHome(current, newOne, true, changeVectors);
						emitInOrder(changeVectors, newOne.screens, current.screens);
						
					} else {
						
						newOne.screens = homeController.setScreens(newOne.pixs);
						changeVectors = setChangeVectors(newOne.screens);
						if(checkContains(type.id, current.pixs) && checkContains(type.id, newOne.pixs)) {
							
							changeVectors = samePixsOnHome(current, newOne, false, changeVectors);
							emitInOrder(changeVectors, newOne.screens, current.screens);
						} else {
							
							var rdObj = reduceDiff(newOne, current, false,changeVectors);
							var deleted = rdObj.selected;
							changeVectors = rdObj.changeVectors;
							changeVectors = changeUnremovedPixs(current, newOne, deleted, changeVectors);
							changeVectors = reduceDiff(newOne, current, true, changeVectors).changeVectors;
							emitInOrder(changeVectors, newOne.screens, current.screens);
						}
						
					}
					homeController.setContent(newOne);
			}
			busy = false;
			that.nextInQueue();
			
		})
	}  else if (type.title == 'remove') {
		//console.log("Start past remove update");
		emitUpdateStreamDelete(type);
		if(!checkContains(type.id, current.pixs))  {
			//console.log("remove from anteroom");
			busy = false;
			that.nextInQueue();
			//pocz
		} else {
			//console.log("remove from homeController");

			homeController.getPixList(newOne, function() {
				newOne.screens = homeController.setScreens(newOne.pixs);
				changeVectors = setChangeVectors(newOne.screens);
				
				var rdObj = reduceDiff(newOne, current,false, changeVectors);
				var deleted = rdObj.selected;
				changeVectors = rdObj.changeVectors
				changeVectors =  changeUnremovedPixs(current, newOne, deleted, changeVectors);
				changeVectors = reduceDiff(newOne, current, true, changeVectors).changeVectors;
				emitInOrder(changeVectors, newOne.screens, current.screens);
				homeController.setContent(newOne);
				busy = false;
				that.nextInQueue();			
			})
		}

	} else if(type.title == 'new') {
		//console.log('new pix update screen option');
		
		emitUpdateStreamNew(type);
		if(current.pixs.length == screenConfObject.getHomeElements()) {
			//console.log("new Pix in anteroom");			

			busy = false;
			that.nextInQueue();
		} else {
			//console.log("new Pix on homeController");
			
			homeController.getPixList(newOne, function() {
				
				newOne.screens = homeController.setScreens(newOne.pixs);
				changeVectors = setChangeVectors(newOne.screens);
				changeVectors = changeUnremovedPixs(current, newOne, [], changeVectors);
				changeVectors = reduceDiff(newOne, current, true, changeVectors).changeVectors;
				emitInOrder(changeVectors, newOne.screens, current.screens);
				homeController.setContent(newOne);	
				busy = false;
				that.nextInQueue();
			});
		}
	}  
	
}

module.exports = obj;
