var screenConfObject = require('./../localModels/screenConfObject.js');
//var dbModule = require('./../dbModule.js');
//var updateModule = require('./../updateModule.js');
var UpdateScreenObject = require('./../localModels/updateScreenObject.js');
var homeController = require(appRoot+'/controllers/homeController');

var resizeEmit = function(socket,dim) {
  var toSend = new UpdateScreenObject('resizeAll',null, dim);
  socket.emit('updateScreen', toSend);
}


var deleteEmit = function(socket, id) {
  var toSend = new UpdateScreenObject('deletePix', id);
  socket.emit('updateScreen', toSend);
}

var checkWidth = function(width) {
  if(!isNaN(width) && isFinite(width) && !(width == false)) return true; else return false;
}


var resizeHome = function(oldScreen, newScreen, callback, callback2) {
  
  var i = 0;
  var j = 0;
  var content = homeController.getContent();
  var obj;
  for(i=0; i<content.pixs.length ; i++) {
    obj = {};
    obj.xscreen = newScreen.name;
    obj.id = content.pixs[i]._id;
    obj.points = content.pixs[i].points;
    if(content.screens[oldScreen.name][i].dim != content.screens[newScreen.name][i].dim)obj.dim =  content.screens[newScreen.name][i].dim;
    if(content.screens[oldScreen.name][i].top != content.screens[newScreen.name][i].top)obj.top=  content.screens[newScreen.name][i].top;
    if(content.screens[oldScreen.name][i].left != content.screens[newScreen.name][i].left)obj.left =  content.screens[newScreen.name][i].left;
    callback(obj);
  }
  callback2(content.screens[newScreen.name].height);
}


module.exports = function(io) {
 var rValue =  io
    .of('/home')
    .on('connection', function (socket) {
      console.log("homePageSocket connected");

        socket.on('getScreen', function (data) {
          homeController.getScreen(data, function(toSend) {
            if(toSend) {
              socket.emit('images', toSend);
            } else {
              //fmess ?
              console.log('error here');
            }
          });
        });

        socket.on('resize', function (data) {
          if(data.current && data.undo) {
            var currentScreen = screenConfObject.chooseScreenByName(data.current), undoScreen = screenConfObject.chooseScreenByName(data.undo);          
            if(currentScreen && undoScreen) {
              if(currentScreen.width && undoScreen.width) {
                //resize screen elements
                socket.emit('screenChange' , false);
                resizeHome( undoScreen, currentScreen, function(obj) {
                      var objx = UpdateScreenObject.newUpdatePix(obj.xscreen, obj.id, obj.points, obj.dim, obj.top, obj.left);
                      socket.emit('updateScreen', objx);

                },function(height) {
                  
                  socket.emit('newPicScreenHeight',{toScreen : data.current, height : height});
                });
              } else if(currentScreen.width == false && undoScreen.width == false) {
                //nic 
                socket.emit('screenChange' , false);
              } else {
                socket.emit('screenChange' , true);
                homeController.getScreen(data.current, function(toSend) { socket.emit('images', toSend); });
              }
            } else {
              //errror
            }
          } else {
            //error
          }          
        });
    });

    
         
    return rValue;
}