var updateModule = require('./../updateModule.js');
var userPageController = require(appRoot+'/controllers/userPageController');


var checkWidth = function(width) {
  if(!isNaN(width) && isFinite(width) && !(width == false)) return true; else return false;
}

module.exports = function(io) {
  var rValue =  io
        .of('/usersPixs')
        .on('connection', function (socket) {

          

          socket.on('getUsersPixs', function (usId) {
            userPageController.getUserPixs(usId, function(toSend, isEnd){
                    socket.emit('images', { 'images' : toSend, 'isEnd' : isEnd});
            });
          });


          socket.on('refresh', function (usId) {
            userPageController.getRefresh(usId, function(toSend){
                    socket.emit('refreshBack', { 'ids' : toSend});
                  });
          });



          socket.on('getPage', function (data) {
             userPageController.getUserPixs(data.usId, function(toSend, isEnd){
                    socket.emit('nextPage', { 'images' : toSend, pageNumber : data.page, 'isEnd' : isEnd});
                  }, data.page);
          });



        });

        return rValue;

}