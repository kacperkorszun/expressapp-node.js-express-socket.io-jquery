var pixController = require(appRoot + '/controllers/pixController.js');

module.exports = function(io) {
 var rValue =  io
    .of('/img')
    .on('connection', function (socket) {
      
      
        socket.on('plus', function (data) {

          var id = data.imgId;
           if(socket.request.session.userId) {
            
              var unleashed = false;
              if(socket.request.session.superuser) {
                unleashed = true;
              }
              pixController.addVote(socket.request.session.userId, id, function(status, retMessage){
                
                var retObj = {};
                retObj.reqId = data.reqId;
                retObj.status = status;
                if(status == 'success') {
                  retObj.messages = [{ status : 'success' , text: 'twoj glos zostal dodany' }];
                } 
                if(status == 'plused'){
                  retObj.messages = [{ status : 'error' , text: 'mozesz zaglosowac tylko raz na obraz' }];
                }
                if(status == 'error'){
                  if(retMessage) {
                     retObj.messages = [{ status : 'error' , text: retMessage }];
                  } else {
                    retObj.messages = [{ status : 'error' , text: 'nieznany blad' }];
                  }
                  
                }
                socket.emit('response', retObj);
              },unleashed);
              
            } else { 
              socket.emit('response', { reqId : data.reqId, status : "error", messages : [{ status : 'error' , text: 'Musisz być zalogowany aby głosować' }]});
            }
          
        });


       

        
    });

    //(rValue);
         
    return rValue;
}