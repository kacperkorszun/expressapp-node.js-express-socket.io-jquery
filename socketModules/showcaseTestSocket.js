var screenConfObject = require('./../localModels/screenConfObject.js');
var updateModule = require('./../updateModule.js');
var UpdateScreenObject = require('./../localModels/updateScreenObject.js');
var showcaseTestModule = require('./../testModules/showcaseTestModule.js');


module.exports = function(io) {
 var rValue =  io
    .of('/showcaseTest')
    .on('connection', function (socket) {
      //console.log("konekted");

        socket.on('checkTest', function () {
          var tId = showcaseTestModule.getTestId();
          if(tId) {
            socket.emit('testId', {started : true, tId : tId});
          } else {
            socket.emit('testId', {started : false});
          }
        });

        socket.on('getTestInfo', function (testId) {
         socket.emit('testInfo', showcaseTestModule.getTestInfo(testId));
        });

       

    });
    showcaseTestModule.setShowcaseTestSocket(rValue);
    return rValue;
}