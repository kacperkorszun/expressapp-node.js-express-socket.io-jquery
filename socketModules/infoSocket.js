var infoModule = require(appRoot + '/infoModule.js');
var screenConfObject = require(appRoot+'/localModels/screenConfObject.js');

module.exports = function(io) {
 var rValue =  io
    .of('/info')
    .on('connection', function (socket) {
        if(socket.request.session.userId) {
          infoModule.setUserSocket(socket.request.session.userId, socket);
        }
      
        socket.on('initial', function () {
            socket.emit('screenSizes',screenConfObject.getScreenSizes());
          });
        
    });

    //(rValue);
         
    return rValue;
}