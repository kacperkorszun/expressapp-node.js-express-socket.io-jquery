//var screenConfObject = require('./../localModels/screenConfObject.js');
//var dbModule = require('./../dbModule.js');
var socket

var anteroomController = require(appRoot+'/controllers/anteroomController');

var resizeEmit = function(socket,dim) {
  var toSend = new UpdateScreenObj('resizeAll',null, dim);
  socket.emit('updateScreen', toSend);
}

var deleteEmit = function(socket, id) {
  var toSend = new UpdateScreenObj('deletePix', id);
  socket.emit('updateScreen', toSend);
}

module.exports = function(io) {
  var rValue =  io
        .of('/poczekalnia')
        .on('connection', function (socket) {

          socket.on('getPoczekalnia', function () {
            anteroomController.getPoczekalnia(function(toSend, isEnd){
                    socket.emit('images', { 'images' : toSend, 'isEnd' : isEnd});
                  });
          });


          socket.on('refresh', function () {
            anteroomController.getRefresh(function(toSend){
                    socket.emit('refreshBack', { 'ids' : toSend});
                  });
          });



          socket.on('getPage', function (xnumber) {
             anteroomController.getPoczekalnia(function(toSend, isEnd){
                    socket.emit('nextPage', { 'images' : toSend, pageNumber : xnumber, 'isEnd' : isEnd});
                  }, xnumber);
          });


        });

   

        return rValue;

}
    